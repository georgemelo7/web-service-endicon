<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerRegional
 *
 * @author George Tassiano
 */
use App\Modulos\BD;
use App\Modulos\Regional;

class ControllerRegional {

  public function getRegional($idRegional) {
    if ($idRegional == NULL) {
      $reg = new Regional();
      return $reg->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_Regional WHERE idTB_Regional = :idRegional";
      $bd->query($sql);
      $bd->bind(':idRegional', $idRegional);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $reg = new Regional($row["idTB_Regional"], $row["Nome"], $row["Estado"]);
        
        //recuperar processos
        $controlProcesso = new ControllerProcesso;
        $bd1 = new BD();
        $sql = "SELECT * FROM TB_Regional_has_TB_Processo WHERE TB_Regional_idTB_Regional = :idRegional";
        $bd1->query($sql);
        $bd1->bind(':idRegional', $reg->getId());
        if ($bd1->execute()) {
          $processos = array();
          while ($row = $bd1->single()) {
            $processos[] = $controlProcesso->getProcesso($row["TB_Processo_idTB_Processo"]);
          }
        } else {
          $processos = null;
        }
        $bd1->close();
        $reg->setProcessos($processos);
        
        //recuperar polos
        $controlPolo = new ControllerPolo;
        $bd1 = new BD();
        $sql = "SELECT * FROM TB_Polo WHERE TB_Regional_idTB_Regional = :idRegional";
        $bd1->query($sql);
        $bd1->bind(':idRegional', $reg->getId());
        if ($bd1->execute()) {
          $polos = array();
          while ($row = $bd1->single()) {
            $polos[] = $controlPolo->getPolo($row["idTB_Polo"]);
          }
        } else {
          $polos = null;
        }
        $bd1->close();
        $reg->setPolos($polos);
        
        $regional = $reg->toArray();
      } else {
        $regional = null;
      }
      $bd->close();
      return $regional;
    }
  }
  
  public function getRegionalLite($idRegional) {
    if ($idRegional == NULL) {
      $reg = new Regional();
      return $reg->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_Regional WHERE idTB_Regional = :idRegional";
      $bd->query($sql);
      $bd->bind(':idRegional', $idRegional);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $reg = new Regional($row["idTB_Regional"], $row["Nome"], $row["Estado"]);
        $regional = $reg->toArray();
      } else {
        $regional = null;
      }
      $bd->close();
      return $regional;
    }
  }
  

  public function getRegionais() {
    $bd1 = new BD();
    $sql = "SELECT * FROM TB_Regional";
    $bd1->query($sql);
    if ($bd1->execute()) {
      $regionais = array();
      while ($row = $bd1->single()) {
        $reg = new Regional($row["idTB_Regional"], $row["Nome"], $row["Estado"]);
        //recuperar processos
        $controlProcesso = new ControllerProcesso;
        $bd = new BD();
        $sql = "SELECT * FROM TB_Regional_has_TB_Processo WHERE TB_Regional_idTB_Regional = :idRegional";
        $bd->query($sql);
        $bd->bind(':idRegional', $reg->getId());
        if ($bd->execute()) {
          $processos = array();
          while ($row = $bd->single()) {
            $processos[] = $controlProcesso->getProcesso($row["TB_Processo_idTB_Processo"]);
          }
        } else {
          $processos = null;
        }
        $bd->close();
        $reg->setProcessos($processos);
        
        //recuperar polos
        $controlPolo = new ControllerPolo;
        $bd = new BD();
        $sql = "SELECT * FROM TB_Polo WHERE TB_Regional_idTB_Regional = :idRegional";
        $bd->query($sql);
        $bd->bind(':idRegional', $reg->getId());
        if ($bd->execute()) {
          $polos = array();
          while ($row = $bd->single()) {
            $polos[] = $controlPolo->getPolo($row["idTB_Polo"]);
          }
        } else {
          $polos = null;
        }
        $bd->close();
        $reg->setPolos($polos);
        
        $regionais[] = $reg->toArray();
      }
    } else {
      $regionais = null;
    }
    $bd1->close();
    return $regionais;
  }

  public function deleteRegional($idRegional) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Regional WHERE idTB_Regional = :idRegional";
    $bd->query($sql);
    $bd->bind(':idRegional', $idRegional);
    $bd->execute();
    $bd->close();
  }

  public function postRegional($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Regional (Nome, Estado) VALUES (:nome, :estado)";
    $bd->query($sql);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->bind(':estado', $dados["Estado"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putRegional($idRegional, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_Regional SET Nome=:nome, Estado=:estado WHERE idTB_Regional = :idRegional";
    $bd->query($sql);
    $bd->bind(':idRegional', $idRegional);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->bind(':estado', $dados["Estado"]);
    $bd->execute();
    $bd->close();
  }

}
