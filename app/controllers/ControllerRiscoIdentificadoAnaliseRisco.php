<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerRiscoIdentificadoAnaliseRisco
 *
 * @author ADM
 */
use App\Modulos\BD;

class ControllerRiscoIdentificadoAnaliseRisco {

  public function deleteRiscosIdentificadosAnaliseRisco($referencia, $riscoIdentificado) {
    $bd = new BD();
    $sql = "DELETE FROM TB_AnaliseRisco_has_TB_RiscoIdentificado WHERE TB_AnaliseRisco_idTB_AnaliseRisco = :idAnaliseRisco";
    $bd->query($sql);
    $bd->bind(':idAnaliseRisco', $referencia);
    $bd->execute();
    $bd->close();
  }

  public function postRiscoIdentificadoAnaliseRisco($referencia, $riscoIdentificado) {
    $bd = new BD();
    $sql = "INSERT INTO TB_AnaliseRisco_has_TB_RiscoIdentificado (TB_AnaliseRisco_idTB_AnaliseRisco, TB_RiscoIdentificado_idTB_RiscoIdentificado) VALUES (:idAnaliseRisco, :idRiscoIdentificado)";
    $bd->query($sql);
    $bd->bind(':idAnaliseRisco', $referencia);
    $bd->bind(':idRiscoIdentificado', $riscoIdentificado);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

}
