<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use App\Modulos\Desvio;
use App\Modulos\BD;

class ControllerDesvio {

  public function getDesvio($idDesvio) {
    if ($idDesvio == NULL) {
      $dev = new Desvio();
      return $dev->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_Desvio WHERE idTB_Desvio = :idDesvio";
      $bd->query($sql);
      $bd->bind(':idDesvio', $idDesvio);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $dev = new Desvio($row['idTB_Desvio'], $row['DataCriacao'], $row['HoraCriacao'], $row['Tipo'], $row['DataOcorrido'], $row['HoraOcorrido'], $row['Registro'], $row['TB_Funcionario_Matricula'], $row['TB_Local_idTB_Local'], $row['TB_LocalidadePolo_idTB_LocalidadePolo']);
        //recupera funcionario
        $controlFuncionario = new ControllerFuncionario;
        $dev->setFuncionario($controlFuncionario->getFuncionario($dev->getFuncionario()));
        //recupera local
        $controlLocal = new ControllerLocal;
        $dev->setLocal($controlLocal->getLocal($dev->getLocal()));
        //recupera localidadePolo
        $controlLocalidadePolo = new ControllerLocalidadePolo();
        $dev->setLocalidadePolo($controlLocalidadePolo->getLocalidadePolo($dev->getLocalidadePolo()));

        $desvio = $dev->toArray();
      } else {
        $desvio = null;
      }
      $bd->close();
      return $desvio;
    }
  }

  public function getDesvios() {
    $bd = new BD();
    $sql = "SELECT * FROM TB_Desvio";
    $bd->query($sql);
    if ($bd->execute()) {
      $desvios = array();
      while ($row = $bd->single()) {
        $dev = new Desvio($row['idTB_Desvio'], $row['DataCriacao'], $row['HoraCriacao'], $row['Tipo'], $row['DataOcorrido'], $row['HoraOcorrido'], $row['Registro'], $row['TB_Funcionario_Matricula'], $row['TB_Local_idTB_Local'], $row['TB_LocalidadePolo_idTB_LocalidadePolo']);
        //recupera funcionario
        $controlFuncionario = new ControllerFuncionario;
        $dev->setFuncionario($controlFuncionario->getFuncionario($dev->getFuncionario()));
        //recupera local
        $controlLocal = new ControllerLocal;
        $dev->setLocal($controlLocal->getLocal($dev->getLocal()));
        //recupera localidadePolo
        $controlLocalidadePolo = new ControllerLocalidadePolo();
        $dev->setLocalidadePolo($controlLocalidadePolo->getLocalidadePolo($dev->getLocalidadePolo()));

        $desvios[] = $dev->toArray();
      }
    } else {
      $desvios = null;
    }
    $bd->close();
    return $desvios;
  }

  public function deleteDesvio($idDesvio) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Desvio WHERE idTB_Desvio = :idDesvio";
    $bd->query($sql);
    $bd->bind(':idDesvio', $idDesvio);
    $bd->execute();
    $bd->close();
  }

  public function postDesvio($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Desvio (DataCriacao, HoraCriacao, DataOcorrido, HoraOcorrido, Tipo, Registro, TB_Funcionario_Matricula, TB_Local_idTB_Local, TB_LocalidadePolo_idTB_LocalidadePolo) VALUES (:dataCriacao, :horaCriacao, :dataOcorrido, :horaOcorrido, :tipo, :registro, :funcionario, :local, :localidadePolo)";
    $bd->query($sql);
    $bd->bind(':dataCriacao', $dados["DataCriacao"]);
    $bd->bind(':horaCriacao', $dados["HoraCriacao"]);
    $bd->bind(':dataOcorrido', $dados["DataOcorrido"]);
    $bd->bind(':horaOcorrido', $dados["HoraOcorrido"]);
    $bd->bind(':tipo', $dados["Tipo"]);
    $bd->bind(':registro', $dados["Registro"]);
    $bd->bind(':funcionario', $dados["TB_Funcionario_Matricula"]);
    $bd->bind(':local', $dados["TB_Local_idTB_Local"]);
    $bd->bind(':localidadePolo', $dados["TB_LocalidadePolo_idTB_LocalidadePolo"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putDesvio($idDesvio, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_Desvio SET DataCriacao=:dataCriacao, HoraCriacao=:horaCriacao, DataOcorrido=:dataOcorrido, HoraOcorrido=:horaOcorrido, Tipo=:tipo, Registro=:registro, TB_Funcionario_Matricula=:funcionario, TB_Local_idTB_Local=:local, TB_LocalidadePolo_idTB_LocalidadePolo=:localidadePolo  WHERE idTB_Desvio = :idDesvio";
    $bd->query($sql);
    $bd->bind(':idDesvio', $idDesvio);
    $bd->bind(':dataCriacao', $dados["DataCriacao"]);
    $bd->bind(':horaCriacao', $dados["HoraCriacao"]);
    $bd->bind(':dataOcorrido', $dados["DataOcorrido"]);
    $bd->bind(':horaOcorrido', $dados["HoraOcorrido"]);
    $bd->bind(':tipo', $dados["Tipo"]);
    $bd->bind(':registro', $dados["Registro"]);
    $bd->bind(':funcionario', $dados["TB_Funcionario_Matricula"]);
    $bd->bind(':local', $dados["TB_Local_idTB_Local"]);
    $bd->bind(':localidadePolo', $dados["TB_LocalidadePolo_idTB_LocalidadePolo"]);
    $bd->execute();
    $bd->close();
  }

}
