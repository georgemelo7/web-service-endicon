<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerCategoriaCausa
 *
 * @author ADM
 */
use App\Modulos\CategoriaCausa;
use App\Modulos\BD;

class ControllerCategoriaCausa {

  public function getCategoriaCausa($idCategoriaCausa) {
    if ($idCategoriaCausa == NULL) {
      $cat = new CategoriaCausa();
      return $cat->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_CategoriaCausa WHERE idTB_CategoriaCausa = :idCategoriaCausa";
      $bd->query($sql);
      $bd->bind(':idCategoriaCausa', $idcategoriaCausa);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $cat = new CategoriaCausa($row['idTB_CategoriaCausa'], $row['Nome']);
        $categoriaCausa = $cat->toArray();
      } else {
        $categoriaCausa = null;
      }
      $bd->close();
      return $categoriaCausa;
    }
  }

  public function getCategoriasCausa() {
    $bd = new BD();
    $sql = "SELECT * FROM TB_CategoriaCausa";
    $bd->query($sql);
    if ($bd->execute()) {
      $categoriasCausa = array();
      while ($row = $bd->single()) {
        $cat = new CategoriaCausa($row['idTB_CategoriaCausa'], $row['Nome']);
        $categoriasCausa[] = $cat->toArray();
      }
    } else {
      $categoriasCausa = null;
    }
    $bd->close();
    return $categoriasCausa;
  }

  public function deletecategoriaCausa($idCategoriaCausa) {
    $bd = new BD();
    $sql = "DELETE FROM TB_CategoriaCausa WHERE idTB_CategoriaCausa = :idCategoriaCausa";
    $bd->query($sql);
    $bd->bind(':idcategoriaCausa', $idCategoriacausa);
    $bd->execute();
    $bd->close();
  }

  public function postCategoriaCausa($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_CategoriaCausa (Nome) VALUES (:nome)";
    $bd->query($sql);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putCategoriaCausa($idCategoriaCausa, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_CategoriaCausa SET Nome=:nome WHERE idTB_CategoriaCausa = :idCategoriaCausa";
    $bd->query($sql);
    $bd->bind(':idCategoriaCausa', $idCategoriaCausa);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->execute();
    $bd->close();
  }

}
