<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use App\Modulos\BD;
use App\Modulos\Funcionario;

class ControllerFuncionario {

  public function getFuncionario($Matricula) {
    if ($Matricula == NULL) {
      $mat = new Funcionario();
      return $mat->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_Funcionario WHERE Matricula = :matricula";
      $bd->query($sql);
      $bd->bind(':matricula', $Matricula);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {

        $fun = new Funcionario($row["Matricula"], $row["Nome"], $row["DataNascimento"], $row["Sexo"], $row["EstadoCivil"], $row["InicioFuncao"], $row["GrauInstrucao"], $row["EstadoInstrucao"], $row["InicioRefeicao"], $row["FinalRefeicao"], $row["TurnoTrabalho"], $row["Nacionalidade"], $row["InicioTrabalho"], $row["HoraExtra"], $row["TB_Funcao_idTB_Funcao"], $row["TB_Local_idTB_Local"], $row["TB_LocalidadePolo_idTB_LocalidadePolo"]);
        //recupera funcao
        $controlFuncao = new ControllerFuncao();
        $fun->setFuncao($controlFuncao->getFuncao($fun->getFuncao()));
        //recupera local
        $controlLocal = new ControllerLocal();
        $fun->setLocal($controlLocal->getLocal($fun->getLocal()));
        //recupera localidadePolo
        $controlLocalidadePolo = new ControllerLocalidadePolo();
        $fun->setLocalidadePolo($controlLocalidadePolo->getLocalidadePolo($fun->getLocalidadePolo()));
        //recupera treinamentosRealizados
        $controlTreinamentoRealizado = new ControllerTreinamentoRealizado();
        $bd = new BD();
        $sql = "SELECT * FROM TB_Funcionario_has_TB_Treinamento WHERE TB_Funcionario_Matricula=:matricula";
        $bd->query($sql);
        $bd->bind(':matricula', $fun->getMatricula());
        if ($bd->execute()) {
          $treinamentosRealizados = array();
          while ($row = $bd->single()) {
            $treinamentosRealizados[] = $controlTreinamentoRealizado->getTreinamentoRealizado($fun->getMatricula(), $row["TB_Treinamento_idTB_Treinamento"]);
          }
        } else {
          $treinamentosRealizados = null;
        }
        $fun->setTreinamentosRealizados($treinamentosRealizados);
        $bd->close();
        $funcionario = $fun->toArray();
      } else {
        $funcionario = null;
      }
      $bd->close();

      return $funcionario;
    }
  }

  public function getFuncionarioLite($Matricula) {

    $bd = new BD();
    $sql = "SELECT * FROM TB_Funcionario WHERE Matricula = :matricula";
    $bd->query($sql);
    $bd->bind(':matricula', $Matricula);
    $bd->execute();
    $row = $bd->single();
    if (!empty($row)) {

      $fun = new Funcionario($row["Matricula"], $row["Nome"], $row["DataNascimento"], $row["Sexo"], $row["EstadoCivil"], $row["InicioFuncao"], $row["GrauInstrucao"], $row["EstadoInstrucao"], $row["InicioRefeicao"], $row["FinalRefeicao"], $row["TurnoTrabalho"], $row["Nacionalidade"], $row["InicioTrabalho"], $row["HoraExtra"], $row["TB_Funcao_idTB_Funcao"], $row["TB_Local_idTB_Local"], $row["TB_LocalidadePolo_idTB_LocalidadePolo"]);

      $funcionario = $fun->toArray();
    } else {
      $funcionario = null;
    }
    $bd->close();

    return $funcionario;
  }

  public function getFuncionarios() {
    $bd1 = new BD();
    $sql = "SELECT * FROM TB_Funcionario";
    $bd1->query($sql);
    if ($bd1->execute()) {
      $funcionarios = array();
      while ($row = $bd1->single()) {
        $fun = new Funcionario($row["Matricula"], $row["Nome"], $row["DataNascimento"], $row["Sexo"], $row["EstadoCivil"], $row["InicioFuncao"], $row["GrauInstrucao"], $row["EstadoInstrucao"], $row["InicioRefeicao"], $row["FinalRefeicao"], $row["TurnoTrabalho"], $row["Nacionalidade"], $row["InicioTrabalho"], $row["HoraExtra"], $row["TB_Funcao_idTB_Funcao"], $row["TB_Local_idTB_Local"], $row["TB_LocalidadePolo_idTB_LocalidadePolo"]);
        //recupera funcao
        $controlFuncao = new ControllerFuncao();
        $fun->setFuncao($controlFuncao->getFuncao($fun->getFuncao()));
        //recupera local
        $controlLocal = new ControllerLocal();
        $fun->setLocal($controlLocal->getLocal($fun->getLocal()));
        //recupera localidadePolo
        $controlLocalidadePolo = new ControllerLocalidadePolo();
        $fun->setLocalidadePolo($controlLocalidadePolo->getLocalidadePolo($fun->getLocalidadePolo()));
        //recupera treinamentosRealizados
        $controlTreinamentoRealizado = new ControllerTreinamentoRealizado();
        $bd = new BD();
        $sql = "SELECT * FROM TB_Funcionario_has_TB_Treinamento WHERE TB_Funcionario_Matricula=:matricula";
        $bd->query($sql);
        $bd->bind(':matricula', $fun->getMatricula());
        if ($bd->execute()) {
          $treinamentosRealizados = array();
          while ($row = $bd->single()) {
            $treinamentosRealizados[] = $controlTreinamentoRealizado->getTreinamentoRealizado($fun->getMatricula(), $row["TB_Treinamento_idTB_Treinamento"]);
          }
        } else {
          $treinamentosRealizados = null;
        }
        $fun->setTreinamentosRealizados($treinamentosRealizados);
        $bd->close();

        $funcionarios[] = $fun->toArray();
      }
    } else {
      $funcionarios = null;
    }
    $bd1->close();
    return $funcionarios;
  }

  public function deleteFuncionario($Matricula) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Funcionario WHERE Matricula = :matricula";
    $bd->query($sql);
    $bd->bind(':matricula', $Matricula);
    $bd->execute();
    $bd->close();
  }

  public function postFuncionario($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Funcionario (Matricula, Nome, DataNascimento, Sexo, EstadoCivil, InicioFuncao, GrauInstrucao, EstadoInstrucao, InicioRefeicao, FinalRefeicao, TurnoTrabalho, Nacionalidade, InicioTrabalho, HoraExtra, TB_Local_idTB_Local, TB_Funcao_idTB_Funcao, TB_LocalidadePolo_idTB_LocalidadePolo) VALUES (:matricula, :nome, :dataNascimento, :sexo, :estadoCivil, :inicioFuncao, :grauInstrucao, :estadoInstrucao, :inicioRefeicao, :finalRefeicao, :turnoTrabalho, :nacionalidade, :inicioTrabalho, :horaExtra, :local, :funcao, :localidadePolo)";
    $bd->query($sql);
    $bd->bind(':matricula', $dados["Matricula"]);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->bind(':dataNascimento', $dados["DataNascimento"]);
    $bd->bind(':sexo', $dados["Sexo"]);
    $bd->bind(':estadoCivil', $dados["EstadoCivil"]);
    $bd->bind(':inicioFuncao', $dados["InicioFuncao"]);
    $bd->bind(':grauInstrucao', $dados["GrauInstrucao"]);
    $bd->bind(':estadoInstrucao', $dados["EstadoInstrucao"]);
    $bd->bind(':inicioRefeicao', $dados["InicioRefeicao"]);
    $bd->bind(':finalRefeicao', $dados["FinalRefeicao"]);
    $bd->bind(':turnoTrabalho', $dados["TurnoTrabalho"]);
    $bd->bind(':nacionalidade', $dados["Nacionalidade"]);
    $bd->bind(':inicioTrabalho', $dados["InicioTrabalho"]);
    $bd->bind(':horaExtra', $dados["HoraExtra"]);
    if ($dados["TB_Local_idTB_Local"] == "NULL") {
      $bd1 = new BD();
      $sql = "INSERT INTO TB_Local (Endereco, CEP, PosicaoGeografica, Estado, Cidade) VALUES (:endereco, :cep, :posicaoGeografica, :estado, :cidade)";
      $bd1->query($sql);
      $bd1->bind(':endereco', "");
      $bd1->bind(':cep', 0);
      $bd1->bind(':posicaoGeografica', '"lng":"0.0","lat":"0.0"');
      $bd1->bind(':estado', "");
      $bd1->bind(':cidade', "");
      $bd1->execute();
      $bd->bind(':local', (int) $bd1->lastInput());
      $bd1->close();
    } else {
      $bd->bind(':local', $dados["TB_Local_idTB_Local"]);
    }
    if ($dados["TB_Funcao_idTB_Funcao"] == "NULL") {
      $bd->bind(':funcao', NULL);
    } else {
      $bd->bind(':funcao', $dados["TB_Funcao_idTB_Funcao"]);
    }
    if ($dados["TB_LocalidadePolo_idTB_LocalidadePolo"] === "NULL") {
      $bd->bind(':localidadePolo', NULL);
    } else {
      $bd->bind(':localidadePolo', $dados["TB_LocalidadePolo_idTB_LocalidadePolo"]);
    }
    $bd->execute();
    $json = array(
        'matricula' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putFuncionario($Matricula, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_Funcionario SET Nome=:nome, DataNascimento=:dataNascimento, Sexo=:sexo, EstadoCivil=:estadoCivil, InicioFuncao=:inicioFuncao, GrauInstrucao=:grauInstrucao, EstadoInstrucao=:estadoInstrucao, InicioRefeicao=:inicioRefeicao, FinalRefeicao=:finalRefeicao, TurnoTrabalho=:turnoTrabalho, Nacionalidade=:nacionalidade, InicioTrabalho=:inicioTrabalho, HoraExtra=:horaExtra, TB_Local_idTB_Local=:local, TB_Funcao_idTB_Funcao=:funcao, TB_LocalidadePolo_idTB_LocalidadePolo=:localidadePolo WHERE Matricula = :matricula";
    $bd->query($sql);
    $bd->bind(':matricula', $Matricula);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->bind(':dataNascimento', $dados["DataNascimento"]);
    $bd->bind(':sexo', $dados["Sexo"]);
    $bd->bind(':estadoCivil', $dados["EstadoCivil"]);
    $bd->bind(':inicioFuncao', $dados["InicioFuncao"]);
    $bd->bind(':grauInstrucao', $dados["GrauInstrucao"]);
    $bd->bind(':estadoInstrucao', $dados["EstadoInstrucao"]);
    $bd->bind(':inicioRefeicao', $dados["InicioRefeicao"]);
    $bd->bind(':finalRefeicao', $dados["FinalRefeicao"]);
    $bd->bind(':turnoTrabalho', $dados["TurnoTrabalho"]);
    $bd->bind(':nacionalidade', $dados["Nacionalidade"]);
    $bd->bind(':inicioTrabalho', $dados["InicioTrabalho"]);
    $bd->bind(':horaExtra', $dados["HoraExtra"]);
    $bd->bind(':local', $dados["TB_Local_idTB_Local"]);
    $bd->bind(':funcao', $dados["TB_Funcao_idTB_Funcao"]);
    $bd->bind(':localidadePolo', $dados["TB_LocalidadePolo_idTB_LocalidadePolo"]);
    $bd->execute();
    $bd->close();
  }

}
