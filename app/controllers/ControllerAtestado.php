<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerDiaAtestado
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\Atestado;

class ControllerAtestado {

  public function getAtestado($idAtestado) {
    if ($idAtestado == NULL) {
      $ate = new Atestado();
      return $ate->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_Atestado WHERE idTB_Atestado = :idAtestado";
      $bd->query($sql);
      $bd->bind(':idAtestado', $idAtestado);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $ate = new Atestado($row["idTB_Atestado"], $row["InicioAtestado"], $row["FimAtestado"]);
        $atestado = $ate->toArray();
      } else {
        $atestado = null;
      }
      $bd->close();
      return $atestado;
    }
  }

  public function getAtestados() {
    $bd = new BD();
    $sql = "SELECT * FROM TB_Atestado";
    $bd->query($sql);
    if ($bd->execute()) {
      $atestados = array();
      while ($row = $bd->single()) {
        $ate = new Atestado($row["idTB_Atestado"], $row["InicioAtestado"], $row["FimAtestado"]);
        $atestados[] = $ate->toArray();
      }
    } else {
      $atestados = null;
    }
    $bd->close();
    return $atestados;
  }

  public function deleteAtestado($idDiaAtestado) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Atestado WHERE idTB_Atestado = :idAtestado";
    $bd->query($sql);
    $bd->bind(':idAtestado', $idAtestado);
    $bd->execute();
    $bd->close();
  }

  public function postAtestado($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Atestado (InicioAtestado, FimAtestado) VALUES (:inicioAtestado, :fimAtestado)";
    $bd->query($sql);
    $bd->bind(':inicioAtestado', $dados["InicioAtestado"]);
    $bd->bind(':fimAtestado', $dados["FimAtestado"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putDiaAtestado($idDiaAtestado, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_Atestado SET InicioAtestado=:inicioAtestado, FimAtestado=:fimAtestado WHERE idTB_Atestado = :idAtestado";
    $bd->query($sql);
    $bd->bind(':idAtestado', $idAtestado);
    $bd->bind(':inicioAtestado', $dados["InicioAtestado"]);
    $bd->bind(':fimAtestado', $dados["FimAtestado"]);
    $bd->execute();
    $bd->close();
  }

}
