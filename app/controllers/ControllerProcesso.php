<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerProcesso
 *
 * @author George Tassiano
 */
use App\Modulos\BD;
use App\Modulos\Processo;

class ControllerProcesso {

  public function getProcesso($idProcesso) {
    if ($idProcesso == NULL) {
      $pro = new Processo();
      return $pro->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_Processo WHERE idTB_Processo = :idProcesso";
      $bd->query($sql);
      $bd->bind(':idProcesso', $idProcesso);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $pro = new Processo($row["idTB_Processo"], $row["Nome"]);
        $processo = $pro->toArray();
      } else {
        $processo = null;
      }
      $bd->close();
      return $processo;
    }
  }

  public function getProcessos() {
    $bd = new BD();
    $sql = "SELECT * FROM TB_Processo";
    $bd->query($sql);
    if ($bd->execute()) {
      $processos = array();
      while ($row = $bd->single()) {
        $pro = new Processo($row["idTB_Processo"], $row["Nome"]);
        $processos[] = $pro->toArray();
      }
    } else {
      $processos = null;
    }
    $bd->close();
    return $processos;
  }

  public function deleteProcesso($idProcesso) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Processo WHERE idTB_Processo = :idProcesso";
    $bd->query($sql);
    $bd->bind(':idProcesso', $idProcesso);
    $bd->execute();
    $bd->close();
  }

  public function postProcesso($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Processo (Nome) VALUES (:nome)";
    $bd->query($sql);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putProcesso($idProcesso, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_Processo SET Nome=:nome WHERE idTB_Processo = :idProcesso";
    $bd->query($sql);
    $bd->bind(':idProcesso', $idProcesso);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->execute();
    $bd->close();
  }

}
