<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerLocalidadePolo
 *
 * @author George Tassiano
 */
use App\Modulos\BD;
use App\Modulos\LocalidadePolo;

class ControllerLocalidadePolo {

  public function getLocalidadePolo($idLocalidadePolo) {
    if ($idLocalidadePolo == NULL) {
      $loc = new LocalidadePolo();
      //recupera o Polo
      $controlPolo = new ControllerPolo();
      $loc->setPolo($controlPolo->getPolo($loc->getPolo()));
      return $loc->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_LocalidadePolo WHERE idTB_LocalidadePolo = :idLocalidadePolo";
      $bd->query($sql);
      $bd->bind(':idLocalidadePolo', $idLocalidadePolo);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $loc = new LocalidadePolo($row["idTB_LocalidadePolo"], $row["Nome"], $row["TB_Polo_idTB_Polo"]);
        //recupera o Polo
        $controlPolo = new ControllerPolo();
        $loc->setPolo($controlPolo->getPoloLite($loc->getPolo()));

        $localidadePolo = $loc->toArray();
      } else {
        $localidadePolo = null;
      }

      $bd->close();
      return $localidadePolo;
    }
  }

  public function getLocalidadesPolo() {
    $bd = new BD();
    $sql = "SELECT * FROM TB_LocalidadePolo";
    $bd->query($sql);
    if ($bd->execute()) {
      $localidadesPolo = array();
      while ($row = $bd->single()) {
        $loc = new LocalidadePolo($row["idTB_LocalidadePolo"], $row["Nome"], $row["TB_Polo_idTB_Polo"]);
        //recupera o Polo
        $controlPolo = new ControllerPolo();
        $loc->setPolo($controlPolo->getPoloLite($loc->getPolo()));
        $localidadesPolo[] = $loc->toArray();
      }
    } else {
      $localidadesPolo = null;
    }
    $bd->close();
    return $localidadesPolo;
  }

  public function deleteLocalidadePolo($idLocalidadePolo) {
    $bd = new BD();
    $sql = "DELETE FROM TB_LocalidadePolo WHERE idTB_LocalidadePolo = :idLocalidadePolo";
    $bd->query($sql);
    $bd->bind(':idLocalidadePolo', $idLocalidadePolo);
    $bd->execute();
    $bd->close();
  }

  public function postLocalidadePolo($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_LocalidadePolo (Nome,TB_Polo_idTB_Polo) VALUES (:nome,:polo)";
    $bd->query($sql);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->bind(':polo', $dados["TB_Polo_idTB_Polo"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putLocalidadePolo($idLocalidadePolo, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_LocalidadePolo SET Nome=:nome, TB_Polo_idTB_Polo=:polo WHERE idTB_LocalidadePolo = :idLocalidadePolo";
    $bd->query($sql);
    $bd->bind(':idLocalidadePolo', $idLocalidadePolo);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->bind(':polo', $dados["TB_Polo_idTB_Polo"]);
    $bd->execute();
    $bd->close();
  }

}
