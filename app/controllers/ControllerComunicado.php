<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of controller_comunicado
 *
 * @author George Tassiano
 */
use App\Modulos\Comunicado;
use App\Modulos\BD;

class ControllerComunicado {

  public function getComunicados() {
    $bd1 = new BD();
    $sql = "SELECT * FROM TB_ComunicadoAcidente";
    $bd1->query($sql);

    if ($bd1->execute()) {
      $comunicados = array();
      while ($row = $bd1->single()) {
        $comu = new Comunicado($row["idTB_ComunicadoAcidente"], $row["Tipo"], $row["DataCriacao"], $row["HoraCriacao"], $row["Tema"], $row["Descricao"], $row["DataOcorrido"], $row["HoraOcorrido"], $row["DanosPessoais"], $row["DanosMateriaisAmbientais"], $row["Codigo"], $row["RepresentanteSesmt"], $row["Gravidade"], $row["NaturezaLesao"], $row["ParteCorpo"], $row["EspecificacaoRisco"], $row["NumeroCat"], $row["Tarefa"], $row["TB_Local_idTB_Local"], $row["TB_Funcionario_Matricula"], $row["TB_Processo_idTB_Processo"], $row["TB_LocalidadePolo_idTB_LocalidadePolo"], $row["TB_AnaliseRisco_idTB_AnaliseRisco"]);
        //recupera os residuos do comunicado
        $controlResiduo = new ControllerResiduo();
        $bd = new BD();
        $sql = "SELECT * FROM TB_Residuo WHERE TB_ComunicadoAcidente_idTB_ComunicadoAcidente=:idComunicado";
        $bd->query($sql);
        $bd->bind(':idComunicado', $comu->getId());
        if ($bd->execute()) {
          $residuos = array();
          while ($row = $bd->single()) {
            $residuos[] = $controlResiduo->getResiduo($row["idTB_Residuo"]);
          }
        } else {
          $residuos = null;
        }
        $comu->setResiduos($residuos);
        $bd->close();
        $controlFuncionario = new ControllerFuncionario();
        //recupera o funcionario que preencheu o comunicado
        $comu->setFuncionario($controlFuncionario->getFuncionarioLite($comu->getFuncionario()));
        //recupera os envolvidos no comunicado
        $controlEnvolvido = new ControllerEnvolvidoComunicado();
        $comu->setEnvolvidos($controlEnvolvido->getEnvolvidosComunicado($comu->getId()));
        //recupera os funcionarios envolvidos no comunicado
        $controlEnvolvidoFuncionario = new ControllerEnvolvidoFuncionario();
        $comu->setEnvolvidosFuncionarios($controlEnvolvidoFuncionario->getEnvolvidos($comu->getId()));
        //recupera o local do ocorrido
        $controlLocal = new ControllerLocal();
        $comu->setLocal($controlLocal->getLocal($comu->getLocal()));
        //recupera o processo
        $controlProcesso = new ControllerProcesso();
        $comu->setProcesso($controlProcesso->getProcesso($comu->getProcesso()));
        //recupera a analiserisco
        $controlAnaliseRisco = new ControllerAnaliseRisco();
        $comu->setAnaliseRisco($controlAnaliseRisco->getAnaliseRisco($comu->getAnaliseRisco()));
        //recupera localidadePolo
        $controlLocalidadePolo = new ControllerLocalidadePolo();
        $comu->setLocalidadePolo($controlLocalidadePolo->getLocalidadePolo($comu->getLocalidadePolo()));
        
        $comunicados[] = $comu->toArray();
      }
    } else {
      $comunicados = null;
    }

    $bd1->close();



    return $comunicados;
  }

  public function getComunicado($idComunicado) {
    if ($idComunicado == NULL) {
      $com = new Comunicado();
      return $com->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_Comunicado WHERE idTB_Comunicado=:idComunicado";
      $bd->query($sql);
      $bd->execute();
      $row = $bd->single();
      $bd->close();
      if (!empty($row)) {
        $comu = new Comunicado($row["idTB_ComunicadoAcidente"], $row["Tipo"], $row["DataCriacao"], $row["HoraCriacao"], $row["Tema"], $row["Descricao"], $row["DataOcorrido"], $row["HoraOcorrido"], $row["DanosPessoais"], $row["DanosMateriaisAmbientais"], $row["Codigo"], $row["RepresentanteSesmt"], $row["Gravidade"], $row["NaturezaLesao"], $row["ParteCorpo"], $row["EspecificacaoRisco"], $row["NumeroCat"], $row["Tarefa"], $row["TB_Local_idTB_Local"], $row["TB_Funcionario_Matricula"], $row["TB_Processo_idTB_Processo"], $row["TB_LocalidadePolo_idTB_LocalidadePolo"], $row["TB_AnaliseRisco_idTB_AnaliseRisco"]);
        //recupera os residuos do comunicado
        $controlResiduo = new ControllerResiduo();
        $bd = new BD();
        $sql = "SELECT * FROM TB_Residuo WHERE TB_ComunicadoAcidente_idTB_ComunicadoAcidente=:idComunicado";
        $bd->query($sql);
        $bd->bind(':idComunicado', $com->getId());
        if ($bd->execute()) {
          $residuos = array();
          while ($row = $bd->single()) {
            $residuos[] = $controlResiduo->getResiduo($row["idTB_Residuo"]);
          }
        } else {
          $residuos = null;
        }
        $com->setResiduos($residuos);
        $bd->close();

        $controlFuncionario = new ControllerFuncionario();
        //recupera o funcionario que preencheu o comunicado
        $com->setFuncionario($controlFuncionario->getFuncionario($com->getFuncionario()));
        //recupera os envolvidos no comunicado
        $controlEnvolvido = new ControllerEnvolvidoComunicado();
        $comu->setEnvolvidos($controlEnvolvido->getEnvolvidosComunicado($comu->getId()));
        //recupera os funcionarios envolvidos no comunicado
        $controlEnvolvidoFuncionario = new ControllerEnvolvidoFuncionario();
        $comu->setEnvolvidosFuncionarios($controlEnvolvidoFuncionario->getEnvolvidos($comu->getId()));
        //recupera o local do ocorrido
        $controlLocal = new ControllerLocal();
        $com->setLocal($controlLocal->getLocal($com->getLocal()));
        //recupera o processo
        $controlProcesso = new ControllerProcesso();
        $com->setProcesso($controlProcesso->getProcesso($com->getProcesso()));
        //recupera a analiserisco
        $controlAnaliseRisco = new ControllerAnaliseRisco();
        $com->setAnaliseRisco($controlAnaliseRisco->getAnaliseRisco($com->getAnaliseRisco()));
        //recupera localidadePolo
        $controlLocalidadePolo = new ControllerLocalidadePolo();
        $comu->setLocalidadePolo($controlLocalidadePolo->getLocalidadePolo($comu->getLocalidadePolo()));
        
        $bd->close();
        $comunicado = $com->toArray();
      } else {
        $comunicado = null;
      }
      return $comunicado;
    }
  }

  public function deleteComunicado($idComunicado) {
    $bd = new BD();
    $sql = "DELETE FROM TB_ComunicadoAcidente WHERE idTB_ComunicadoAcidente = :idComunicado";
    $bd->query($sql);
    $bd->bind(':idComunicado', $idComunicado);
    $bd->execute();
    $bd->close();
  }

  public function postComunicado($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_ComunicadoAcidente (HoraCriacao, DataCriacao, Tema, Descricao, DataOcorrido, HoraOcorrido, DanosPessoais, DanosMateriaisAmbientais, Codigo, RepresentanteSesmt, Gravidade, NaturezaLesao, ParteCorpo, EspecificacaoRisco, NumeroCat, Tipo, TB_Local_idTB_Local, TB_Funcionario_Matricula, TB_LocalidadePolo_idTB_LocalidadePolo, TB_Processo_idTB_Processo, TB_AnaliseRisco_idTB_AnaliseRisco, Tarefa) VALUES (:horaCriacao, :dataCriacao, :tema, :descricao, :dataOcorrido, :horaOcorrido, :danosPessoais, :danosMateriaisAmbientais, :codigo, :representanteSesmt, :gravidade, :naturezaLesao, :parteCorpo, :especificacaoRisco, :numeroCat, :tipo, :local, :funcionario, :localidadePolo, :processo, :analiseRisco, :tarefa)";
    $bd->query($sql);
    $bd->bind(':horaCriacao', $dados["HoraCriacao"]);
    $bd->bind(':dataCriacao', $dados["DataCriacao"]);
    $bd->bind(':tema', $dados["Tema"]);
    $bd->bind(':descricao', $dados["Descricao"]);
    $bd->bind(':dataOcorrido', $dados["DataOcorrido"]);
    $bd->bind(':horaOcorrido', $dados["HoraOcorrido"]);
    $bd->bind(':danosPessoais', $dados["DanosPessoais"]);
    $bd->bind(':danosMateriaisAmbientais', $dados["DanosMateriaisAmbientais"]);
    $bd->bind(':codigo', $dados["Codigo"]);
    $bd->bind(':representanteSesmt', $dados["RepresentanteSesmt"]);
    $bd->bind(':gravidade', $dados["Gravidade"]);
    $bd->bind(':naturezaLesao', $dados["NaturezaLesao"]);
    $bd->bind(':parteCorpo', $dados["ParteCorpo"]);
    $bd->bind(':especificacaoRisco', $dados["EspecificacaoRisco"]);
    $bd->bind(':numeroCat', $dados["NumeroCat"]);
    $bd->bind(':tipo', $dados["Tipo"]);
    $bd->bind(':local', $dados["TB_Local_idTB_Local"]);
    $bd->bind(':funcionario', $dados["TB_Funcionario_Matricula"]);
    $bd->bind(':localidadePolo', $dados["TB_LocalidadePolo_idTB_LocalidadePolo"]);
    if ($dados["TB_Processo_idTB_Processo"] == "NULL") {
      $bd->bind(':processo', NULL);
    } else {
      $bd->bind(':processo', $dados["TB_Processo_idTB_Processo"]);
    }
    if ($dados["TB_AnaliseRisco_idTB_AnaliseRisco"] == "NULL") {
      $bd->bind(':analiseRisco', NULL);
    } else {
      $bd->bind(':analiseRisco', $dados["TB_AnaliseRisco_idTB_AnaliseRisco"]);
    }
    $bd->bind(':tarefa', $dados["Tarefa"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putComunicado($idComunicado, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_Comunicado SET HoraCriacao=:horaCriacao, DataCriacao=:dataCriacao, Tema=:tema, Descricao=:descricao, DataOcorrido=:dataOcorrido, HoraOcorrido=:horaOcorrido, DanosPessoais=:danosPessoais, DanosMateriaisAmbientais=:danosMateriaisAmbientais, Codigo=:codigo, RepresentanteSesmt=:representanteSesmt, Gravidade=:gravidade, NaturezaLesao=:naturezaLesao, ParteCorpo=:parteCorpo, EspecificacaoRisco=:especificacaoRisco, NumeroCat=:numeroCat, TB_Local_idTB_Local=:local, TB_Funcionario_Matricula=:funcionario, TB_Processo_idTB_Processo=:processo, Tipo=:tipo, TB_AnaliseRisco_idTB_AnaliseRisco=:analiseRisco, TB_LocalidadePolo_idTB_LocalidadePolo=:localidadePolo, Tarefa=:tarefa WHERE idTB_Comunicado = :idComunicado";
    $bd->query($sql);
    $bd->bind(':idComunicado', $idComunicado);
    $bd->bind(':horaCriacao', $dados["HoraCriacao"]);
    $bd->bind(':dataCriacao', $dados["DataCriacao"]);
    $bd->bind(':tema', $dados["Tema"]);
    $bd->bind(':descricao', $dados["Descricao"]);
    $bd->bind(':dataOcorrido', $dados["DataOcorrido"]);
    $bd->bind(':horaOcorrido', $dados["HoraOcorrido"]);
    $bd->bind(':danosPessoais', $dados["DanosPessoais"]);
    $bd->bind(':danosMateriaisAmbientais', $dados["DanosMateriaisAmbientais"]);
    $bd->bind(':codigo', $dados["Codigo"]);
    $bd->bind(':representanteSesmt', $dados["RepresentanteSesmt"]);
    $bd->bind(':gravidade', $dados["Gravidade"]);
    $bd->bind(':naturezaLesao', $dados["NaturezaLesao"]);
    $bd->bind(':parteCorpo', $dados["ParteCorpo"]);
    $bd->bind(':especificacaoRisco', $dados["EspecificacaoRisco"]);
    $bd->bind(':numeroCat', $dados["NumeroCat"]);
    $bd->bind(':local', $dados["TB_Local_idTB_Local"]);
    if ($dados["TB_Processo_idTB_Processo"] == "NULL") {
      $bd->bind(':processo', NULL);
    } else {
      $bd->bind(':processo', $dados["TB_Processo_idTB_Processo"]);
    }
    $bd->bind(':tipo', $dados["Tipo"]);
    if ($dados["TB_AnaliseRisco_idTB_AnaliseRisco"] == "NULL") {
      $bd->bind(':analiseRisco', NULL);
    } else {
      $bd->bind(':analiseRisco', $dados["TB_AnaliseRisco_idTB_AnaliseRisco"]);
    }
    $bd->bind(':funcionario', $dados["TB_Funcionario_Matricula"]);
    $bd->bind(':localidadePolo', $dados["TB_LocalidadePolo_idTB_LocalidadePolo"]);
    $bd->bind(':tarefa', $dados["Tarefa"]);
    $bd->execute();
    $bd->close();
  }

}
