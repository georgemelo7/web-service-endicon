<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerAtendimentoMedico
 *
 * @author ADM
 */
use App\Modulos\AtendimentoMedico;
use App\Modulos\BD;

class ControllerAtendimentoMedico {

  public function getAtendimentoMedico($idAtendimentoMedico) {
    if ($idAtendimentoMedico == NULL) {
      $ate = new AtendimentoMedico();
      return $ate->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_AtendimentoMedico WHERE idTB_AtendimentoMedico = :idAtendimentoMedico";
      $bd->query($sql);
      $bd->bind(':idAtendimentoMedico', $idAtendimentoMedico);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $ate = new AtendimentoMedico($row['idTB_AtendimentoMedico'], $row['DataAtendimento'], $row['HoraAtendimento'], $row['NomeLocal'], $row['HouveInternacao'], $row['ParteAtingida'], $row['TB_Atestado_idTB_Atestado']);
        $atendimentoMedico = $ate->toArray();
      } else {
        $atendimentoMedico = null;
      }
      $bd->close();
      return $atendimentoMedico;
    }
  }

  public function deleteAtendimentoMedico($idAtendimentoMedico) {
    $bd = new BD();
    $sql = "DELETE FROM TB_AtendimentoMedico WHERE idTB_AtendimentoMedico = :idAtendimentoMedico";
    $bd->query($sql);
    $bd->bind(':idAtendimentoMedico', $idAtendimentoMedico);
    $bd->execute();
    $bd->close();
  }

  public function postAtendimentoMedico($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_AtendimentoMedico (DataAtendimento, HoraAtendimento, NomeLocal, HouveInternacao, ParteAtingida, TB_Atestado_idTB_Atestado) VALUES (:dataAtendimento, :horaAtendimento, :nomeLocal, :houveInternacao, :parteAtingida, :atestado)";
    $bd->query($sql);
    $bd->bind(':dataAtendimento', $dados["DataAtendimento"]);
    $bd->bind(':horaAtendimento', $dados["HoraAtendimento"]);
    $bd->bind(':nomeLocal', $dados["NomeLocal"]);
    $bd->bind(':houveInternacao', $dados["HouveInternacao"]);
    $bd->bind(':parteAtingida', $dados["ParteAtingida"]);
    $bd->bind(':atestado', $dados["TB_Atestado_idTB_Atestado"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putAtendimentoMedico($idAtendimentoMedico, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_AtendimentoMedico SET DataAtendimento=:dataAtendimento, HoraAtendimento=:horaAtendimento, NomeLocal=:nomeLocal, HouveInternacao=:houveInternacao, ParteAtingida=:parteAtingida, TB_Atestado_idTB_Atestado=:atestado WHERE idTB_AtendimentoMedico = :idAtendimentoMedico";
    $bd->query($sql);
    $bd->bind(':idAtendimentoMedico', $idAtendimentoMedico);
    $bd->bind(':dataAtendimento', $dados["DataAtendimento"]);
    $bd->bind(':horaAtendimento', $dados["HoraAtendimento"]);
    $bd->bind(':nomeLocal', $dados["NomeLocal"]);
    $bd->bind(':houveInternacao', $dados["HouveInternacao"]);
    $bd->bind(':parteAtingida', $dados["ParteAtingida"]);
    $bd->bind(':atestado', $dados["TB_Atestado_idTB_Atestado"]);
    $bd->execute();
    $bd->close();
  }

}
