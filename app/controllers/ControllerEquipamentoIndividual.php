<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerEquipamento
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\Equipamento;

class ControllerEquipamentoIndividual {

  public function getEquipamento($idEquipamento) {
    if ($idEquipamento == NULL) {
      $equ = new Equipamento();
      return $equ->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_EquipamentoIndividual WHERE idTB_EquipamentoIndividual = :idEquipamento";
      $bd->query($sql);
      $bd->bind(':idEquipamento', $idEquipamento);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $equ = new Equipamento($row["idTB_EquipamentoIndividual"], $row["Nome"]);
        $equipamento = $equ->toArray();
      } else {
        $equipamento = null;
      }
      $bd->close();
      return $equipamento;
    }
  }

  public function getEquipamentos() {
    $bd = new BD();
    $sql = "SELECT * FROM TB_EquipamentoIndividual";
    $bd->query($sql);
    if ($bd->execute()) {
      $equipamentos = array();
      while ($row = $bd->single()) {
        $equ = new Equipamento($row["idTB_EquipamentoIndividual"], $row["Nome"]);
        $equipamentos[] = $equ->toArray();
      }
    } else {
      $equipamentos = null;
    }
    $bd->close();
    return $equipamentos;
  }

  public function deleteEquipamento($idEquipamento) {
    $bd = new BD();
    $sql = "DELETE FROM TB_EquipamentoIndividual WHERE idTB_EquipamentoIndividual = :idEquipamento";
    $bd->query($sql);
    $bd->bind(':idEquipamento', $idEquipamento);
    $bd->execute();
    $bd->close();
  }

  public function postEquipamento($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_EquipamentoIndividual (Nome) VALUES (:nome)";
    $bd->query($sql);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putProcesso($idEquipamento, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_EquipamentoIndividual SET Nome=:nome WHERE idTB_EquipamentoIndividual = :idEquipamento";
    $bd->query($sql);
    $bd->bind(':idEquipamento', $idEquipamento);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->execute();
    $bd->close();
  }

}
