<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerMembrosAnaliseRisco
 *
 * @author ADM
 */
use App\Modulos\BD;

class ControllerMembroAnaliseRisco {

  public function deleteMembrosAnaliseRisco($referencia) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Funcionario_has_TB_AnaliseRisco WHERE TB_AnaliseRisco_idTB_AnaliseRisco = :idAnaliseRisco";
    $bd->query($sql);
    $bd->bind(':idAnaliseRisco', $referencia);
    $bd->execute();
    $bd->close();
  }

  public function postMembroAnaliseRisco($referencia, $membro) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Funcionario_has_TB_AnaliseRisco (TB_AnaliseRisco_idTB_AnaliseRisco, TB_Funcionario_Matricula) VALUES (:idAnaliseRisco, :idFuncionario)";
    $bd->query($sql);
    $bd->bind(':idAnaliseRisco', $referencia);
    $bd->bind(':idFuncionario', $membro);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

}
