<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerEnvolvido
 *
 * @author ADM
 */
use App\Modulos\Envolvido;
use App\Modulos\BD;

class ControllerEnvolvidoComunicado {

  public function getEnvolvidoComunicado($idEnvolvido) {
    if ($idEnvolvido == NULL) {
      $env = new Envolvido();
      return $env->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_Envolvido WHERE idTB_Envolvido = :idEnvolvido";
      $bd->query($sql);
      $bd->bind(':idEnvolvido', $idEnvolvido);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $env = new Envolvido($row["idTB_Envolvido"], $row["Nome"], $row["Envolvimento"], $row["Relacao"], $row["TB_ComunicadoAcidente_idTB_ComunicadoAcidente"]);
        $envolvido = $env->toArray();
      } else {
        $envolvido = null;
      }
      $bd->close();
      return $envolvido;
    }
  }

  public function getEnvolvidosComunicado($idReferencia) {
    $bd = new BD();
    $sql = "SELECT * FROM TB_Envolvido WHERE TB_ComunicadoAcidente_idTB_ComunicadoAcidente = :idReferencia";
    $bd->query($sql);
    $bd->bind(':idReferencia', $idReferencia);
    if ($bd->execute()) {
      $envolvidos = array();
      while ($row = $bd->single()) {
        $env = new Envolvido($row["idTB_Envolvido"], $row["Nome"], $row["Envolvimento"], $row["Relacao"], $row["TB_ComunicadoAcidente_idTB_ComunicadoAcidente"]);
        $envolvidos[] = $env->toArray();
      }
    } else {
      $envolvidos = null;
    }
    $bd->close();
    return $envolvidos;
  }

  public function deleteEnvolvidosComunicado($idComunicado) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Envolvido WHERE TB_ComunicadoAcidente_idTB_ComunicadoAcidente = :idComunicado";
    $bd->query($sql);
    $bd->bind(':idComunicado', $idComunicado);
    $bd->execute();
    $bd->close();
  }

  public function putEnvolvidoComunicado($idEnvolvido, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_Envolvido SET Nome=:nome, Envolvimento=:envolvimento, Relacao=:relacao, TB_ComunicadoAcidente_idTB_ComunicadoAcidente=:comunicado WHERE idTB_Envolvido = :idEnvolvido";
    $bd->query($sql);
    $bd->bind(':idEnvolvido', $idEnvolvido);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->bind(':envolvimento', $dados["Envolvimento"]);
    $bd->bind(':relacao', $dados["Relacao"]);
    $bd->bind(':comunicado', $dados["TB_ComunicadoAcidente_idTB_ComunicadoAcidente"]);
    $bd->execute();
    $bd->close();
  }

  public function postEnvolvidoComunicado($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Envolvido (Nome, Envolvimento, Relacao, TB_ComunicadoAcidente_idTB_ComunicadoAcidente) VALUES (:nome, :envolvimento, :relacao, :comunicado)";
    $bd->query($sql);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->bind(':envolvimento', $dados["Envolvimento"]);
    $bd->bind(':relacao', $dados["Relacao"]);
    $bd->bind(':comunicado', $dados["TB_ComunicadoAcidente_idTB_ComunicadoAcidente"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

}
