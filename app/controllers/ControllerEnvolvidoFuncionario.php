<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerEnvolvidoFuncionario
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\EnvolvidoFuncionario;

class ControllerEnvolvidoFuncionario {

  public function getEnvolvido($idFuncionario, $idComunicado) {
    $bd = new BD();
    $sql = "SELECT * FROM TB_Funcionario_has_TB_ComunicadoAcidente WHERE TB_Funcionario_Matricula = :idFuncionario AND TB_ComunicadoAcidente_idTB_ComunicadoAcidente = :idComunicado";
    $bd->query($sql);
    $bd->bind(':idFuncionario', $idFuncionario);
    $bd->bind(':idComunicado', $idComunicado);
    $bd->execute();
    $row = $bd->single();
    if (!empty($row)) {
      $env = new EnvolvidoFuncionario($row["TB_Funcionario_Matricula"], $row["TB_ComunicadoAcidente_idTB_ComunicadoAcidente"], $row["Envolvimento"], $row["Relacao"]);
      //recupera funcionario
      $controlFuncionario = new ControllerFuncionario();
      $env->setIdFuncionario($controlFuncionario->getFuncionario($env->getIdFuncionario()));

      $envolvido = $env->toArray();
    } else {
      $envolvido = null;
    }
    $bd->close();
    return $envolvido;
  }

  public function getEnvolvidos($idComunicado) {
    $bd = new BD();
    $sql = "SELECT * FROM TB_Funcionario_has_TB_ComunicadoAcidente WHERE TB_ComunicadoAcidente_idTB_ComunicadoAcidente = :idComunicado";
    $bd->query($sql);
    $bd->bind(':idComunicado', $idComunicado);
    if ($bd->execute()) {
      $envolvidos = array();
      while ($row = $bd->single()) {
        $env = new EnvolvidoFuncionario($row["TB_Funcionario_Matricula"], $row["TB_ComunicadoAcidente_idTB_ComunicadoAcidente"], $row["Envolvimento"], $row["Relacao"]);
        //recupera funcionario
        $controlFuncionario = new ControllerFuncionario();
        $env->setIdFuncionario($controlFuncionario->getFuncionario($env->getIdFuncionario()));

        $envolvidos[] = $env->toArray();
      }
    } else {
      $envolvidos = null;
    }
    $bd->close();
    return $envolvidos;
  }

  public function deleteEnvolvidos($idComunicado) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Funcionario_has_TB_ComunicadoAcidente WHERE TB_ComunicadoAcidente_idTB_ComunicadoAcidente = :idComunicado";
    $bd->query($sql);
    $bd->bind(':idComunicado', $idComunicado);
    $bd->execute();
    $bd->close();
  }

  public function postEnvolvido($idFuncionario, $idComunicado, $dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Funcionario_has_TB_ComunicadoAcidente (TB_Funcionario_Matricula, TB_ComunicadoAcidente_idTB_ComunicadoAcidente, Envolvimento, Relacao) VALUES (:idFuncionario, :idComunicado, :envolvimento, :relacao)";
    $bd->query($sql);
    $bd->bind(':idFuncionario', $idFuncionario);
    $bd->bind(':idComunicado', $idComunicado);
    $bd->bind(':envolvimento', $dados["Envolvimento"]);
    $bd->bind(':relacao', $dados["Relacao"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putEnvolvido($idFuncionario, $idComunicado, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_Funcionario_has_ComunicadoAcidente SET Envolvimento=:envolvimento, Relacao=:relacao WHERE TB_Funcionario_Matricula = :idFuncionario AND TB_ComunicadoAcidente_idTB_ComunicadoAcidente = :idComunicado";
    $bd->query($sql);
    $bd->bind(':idFuncionario', $idFuncionario);
    $bd->bind(':idComunicado', $idComunicado);
     $bd->bind(':envolvimento', $dados["Envolvimento"]);
    $bd->bind(':relacao', $dados["Relacao"]);
    $bd->execute();
    $bd->close();
  }

}
