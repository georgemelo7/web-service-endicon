<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerEnvolvido
 *
 * @author ADM
 */

use App\Modulos\BD;

class ControllerAnaliseAcidenteRelatorioInvestigacao {

  public function deleteAnaliseAcidente($idReferencia, $idAnaliseAcidente) {
    $bd = new BD();
    $sql = "DELETE FROM TB_RelatorioInvestigacao_has_TB_AnaliseAcidente WHERE TB_AnaliseAcidente_idTB_AnaliseAcidente = :idAnaliseAcidente AND TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao = :idReferencia";
    $bd->query($sql);
    $bd->bind(':idReferencia', $idReferencia);
    $bd->bind(':idAnaliseAcidente', $idAnaliseAcidente);
    $bd->execute();
    $bd->close();
  }

  public function postAnaliseAcidente($idReferencia, $idAnaliseAcidente) {
    $bd = new BD();
    $sql = "INSERT INTO TB_RelatorioInvestigacao_has_TB_AnaliseAcidente (TB_AnaliseAcidente_idTB_AnaliseAcidente, TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao) VALUES (:idAnaliseAcidente, :idReferencia)";
    $bd->query($sql);
    $bd->bind(':idReferencia', $idReferencia);
    $bd->bind(':idAnaliseAcidente', $idAnaliseAcidente);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

}
