<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerTarefa
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\Tarefa;

class ControllerTarefa {

    public function getTarefa($idTarefa) {
        if ($idTarefa == NULL) {
            $taf = new Tarefa();
            //recupera o tipoProcesso
            $controlProcesso = new ControllerProcesso();
            $taf->setProcesso($controlProcesso->getProcesso($taf->getProcesso()));
            return $taf->toArray();
        } else {
            $bd = new BD();
            $sql = "SELECT * FROM TB_Tarefa WHERE idTB_Tarefa = :idTarefa";
            $bd->query($sql);
            $bd->bind(':idTarefa', $idTarefa);
            $bd->execute();
            $row = $bd->single();
            if (!empty($row)) {
                $taf = new Tarefa($row["idTB_Tarefa"], $row["Nome"], $row["TB_Processo_idTB_Processo"]);
                //recupera o tipoProcesso
                $controlProcesso = new ControllerProcesso();
                $taf->setProcesso($controlProcesso->getProcesso($taf->getProcesso()));


                $tarefa = $taf->toArray();
            } else {
                $tarefa = null;
            }

            $bd->close();
            return $tarefa;
        }
    }

    public function getTarefas() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_Tarefa";
        $bd->query($sql);
        if ($bd->execute()) {
            $tarefas = array();
            while ($row = $bd->single()) {
                $taf = new Tarefa($row["idTB_Tarefa"], $row["Nome"], $row["TB_Processo_idTB_Processo"]);
                //recupera o tipoProcesso
                $controlProcesso = new ControllerProcesso();
                $taf->setProcesso($controlProcesso->getProcesso($taf->getProcesso()));

                $tarefas[] = $taf->toArray();
            }
        } else {
            $tarefas = null;
        }
        $bd->close();
        return $tarefas;
    }

    public function deleteTarefa($idTarefa) {
        $bd = new BD();
        $sql = "DELETE FROM TB_Tarefa WHERE idTB_Tarefa = :idTarefa";
        $bd->query($sql);
        $bd->bind(':idTarefa', $idTarefa);
        $bd->execute();
        $bd->close();
    }

    public function postTarefa($dados) {
        $bd = new BD();
        $sql = "INSERT INTO TB_Tarefa (Nome,TB_Processo_idTB_Processo) VALUES (:nome,:processo)";
        $bd->query($sql);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':processo', $dados["TB_Processo_idTB_Processo"]);
        $bd->execute();
        $json = array(
            'id' => (int) $bd->lastInput()
        );
        $bd->close();
        return $json;
    }

    public function putTarefa($idTarefa, $dados) {
        $bd = new BD();
        $sql = "UPDATE TB_Tarefa SET Nome=:nome, TB_Processo_idTB_Processo=:processo WHERE idTB_Tarefa = :idTarefa";
        $bd->query($sql);
        $bd->bind(':idTarefa', $idTarefa);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':processo', $dados["TB_Processo_idTB_Processo"]);
        $bd->execute();
        $bd->close();
    }

}
