<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerPergunta
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\Pergunta;

class ControllerPerguntaRelatoInterno {

  public function getPergunta($idPergunta) {
    if ($idPergunta == NULL) {
      $per = new Pergunta();
      return $per->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_PerguntaRelato WHERE idTB_PerguntaRelato = :idPergunta";
      $bd->query($sql);
      $bd->bind(':idPergunta', $idPergunta);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $per = new Pergunta($row["idTB_PerguntaRelato"], $row["Pergunta"]);
        $pergunta = $per->toArray();
      } else {
        $pergunta = null;
      }
      $bd->close();
      return $pergunta;
    }
  }

  public function getPerguntas() {
    $bd = new BD();
    $sql = "SELECT * FROM TB_PerguntaRelato";
    $bd->query($sql);
    if ($bd->execute()) {
      $perguntas = array();
      while ($row = $bd->single()) {
        $per = new Pergunta($row["idTB_PerguntaRelato"], $row["Pergunta"]);
        $perguntas[] = $per->toArray();
      }
    } else {
      $perguntas = null;
    }
    $bd->close();
    return $perguntas;
  }

  public function deletePergunta($idPergunta) {
    $bd = new BD();
    $sql = "DELETE FROM TB_PerguntaRelato WHERE idTB_PerguntaRelato = :idPergunta";
    $bd->query($sql);
    $bd->bind(':idPergunta', $idPergunta);
    $bd->execute();
    $bd->close();
  }

  public function postPergunta($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_PerguntaRelato (Pergunta) VALUES (:pergunta)";
    $bd->query($sql);
    $bd->bind(':pergunta', $dados["Pergunta"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putPergunta($idPergunta, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_PerguntaRelato SET Pergunta=:pergunta WHERE idTB_PerguntaRelato = :idPergunta";
    $bd->query($sql);
    $bd->bind(':idPergunta', $idPergunta);
    $bd->bind(':pergunta', $dados["Pergunta"]);
    $bd->execute();
    $bd->close();
  }

}
