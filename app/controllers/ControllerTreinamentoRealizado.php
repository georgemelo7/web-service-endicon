<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerTreinamentoRealizado
 *
 * @author George Tassiano
 */
use App\Modulos\BD;
use App\Modulos\TreinamentoRealizado;

class ControllerTreinamentoRealizado {

  public function getTreinamentoRealizado($idFuncionario, $idTreinamento) {

    $bd = new BD();
    $sql = "SELECT * FROM TB_Funcionario_has_TB_Treinamento WHERE TB_Funcionario_Matricula = :idFuncionario and TB_Treinamento_idTB_Treinamento= :idTreinamento";
    $bd->query($sql);
    $bd->bind(':idFuncionario', $idFuncionario);
    $bd->bind(':idTreinamento', $idTreinamento);
    $bd->execute();
    $row = $bd->single();
    if (!empty($row)) {
      $tre = new TreinamentoRealizado($row["TB_Funcionario_Matricula"], $row["TB_Treinamento_idTB_Treinamento"], $row["Data"], $row["Instrutor"], $row["CargaHoraria"]);
      //busca o treinamento
      $controlTreinamento = new ControllerTreinamento();
      $tre->setIdTreinamento($controlTreinamento->getTreinamento($tre->getIdTreinamento()));
      $treinamentoRealizado = $tre->toArray();
    } else {
      $treinamentoRealizado = null;
    }
    $bd->close();
    return $treinamentoRealizado;
  }

  public function getTreinamentosRealizados() {
    $bd = new BD();
    $sql = "SELECT * FROM TB_Funcionario_has_TB_Treinamento";
    $bd->query($sql);
    if ($bd->execute()) {
      $treinamentosRealizados = array();
      while ($row = $bd->single()) {
        $tre = new TreinamentoRealizado($row["TB_Funcionario_Matricula"], $row["TB_Treinamento_idTB_Treinamento"], $row["Data"], $row["Instrutor"], $row["CargaHoraria"]);
        //busca o treinamento
        $controlTreinamento = new ControllerTreinamento();
        $tre->setIdTreinamento($controlTreinamento->getTreinamento($tre->getIdTreinamento()));

        //busca o funcionario
        $controlFuncionario = new ControllerFuncionario();
        $tre->setIdFuncionario($controlFuncionario->getFuncionarioLite($tre->getIdFuncionario()));

        $treinamentosRealizados[] = $tre->toArray();
      }
    } else {
      $treinamentosRealizados = null;
    }
    $bd->close();
    return $treinamentosRealizados;
  }

  public function deleteTreinamentoRealizado($idFuncionario, $idTreinamento) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Funcionario_has_TB_Treinamento WHERE TB_Funcionario_Matricula = :idFuncionario and TB_Treinamento_idTB_Treinamento= :idTreinamento";
    $bd->query($sql);
    $bd->bind(':idFuncionario', $idFuncionario);
    $bd->bind(':idTreinamento', $idTreinamento);
    $bd->execute();
    $bd->close();
  }

  public function postTreinamentoRealizado($idFuncionario, $idTreinamento, $dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Funcionario_has_TB_Treinamento (TB_Funcionario_Matricula, TB_Treinamento_idTB_Treinamento, Data, Instrutor, CargaHoraria) VALUES (:funcionario , :treinamento, :data, :instrutor, :cargaHoraria)";
    $bd->query($sql);
    $bd->bind(':funcionario', $idFuncionario);
    $bd->bind(':treinamento', $idTreinamento);
    $bd->bind(':data', $dados["Data"]);
    $bd->bind(':instrutor', $dados["Instrutor"]);
    $bd->bind(':cargaHoraria', $dados["CargaHoraria"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putTreinamentoRealizado($idFuncionario, $idTreinamento, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_Funcionario_has_TB_Treinamento SET Data=:data, Instrutor=:instrutor, CargaHoraria=:cargaHoraria WHERE TB_Funcionario_Matricula = :idFuncionario and TB_Treinamento_idTB_Treinamento= :idTreinamento";
    $bd->query($sql);
    $bd->bind(':idFuncionario', $idFuncionario);
    $bd->bind(':idTreinamento', $idTreinamento);
    $bd->bind(':data', $dados["Data"]);
    $bd->bind(':instrutor', $dados["Instrutor"]);
    $bd->bind(':cargaHoraria', $dados["CargaHoraria"]);
    $bd->execute();
    $bd->close();
  }

}
