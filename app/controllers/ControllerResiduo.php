<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use App\Modulos\Residuo;
use App\Modulos\BD;

class ControllerResiduo {

  public function getResiduo($idResiduo) {
    if ($idResiduo == NULL) {
      $res = new Residuo();
      return $res->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_Residuo WHERE idTB_Residuo = :idResiduo";
      $bd->query($sql);
      $bd->bind(':idResiduo', $idResiduo);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $res = new Residuo($row["idTB_Residuo"], $row["Identificacao"], $row["Quantidade"], $row["DestinacaoDada"], $row["TB_ComunicadoAcidente_idTB_ComunicadoAcidente"]);
        $residuo = $res->toArray();
      } else {
        $residuo = null;
      }
      $bd->close();
      return $residuo;
    }
  }

  public function getResiduos($idReferencia) {
    $bd = new BD();
    $sql = "SELECT * FROM TB_Residuo WHERE TB_ComunicadoAcidente_idTB_ComunicadoAcidente =:referencia";
    $bd->query($sql);
    $bd->bind(':referencia', $idReferencia);
    if ($bd->execute()) {
      $residuos = array();
      while ($row = $bd->single()) {
        $res = new Residuo($row["idTB_Residuo"], $row["Identificacao"], $row["Quantidade"], $row["DestinacaoDada"], $row["TB_ComunicadoAcidente_idTB_ComunicadoAcidente"]);
        $residuos[] = $res->toArray();
      }
    } else {
      $residuos = null;
    }
    $bd->close();
    return $residuos;
  }
  
  public function deleteResiduos($idComunicado) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Residuo WHERE TB_ComunicadoAcidente_idTB_ComunicadoAcidente = :idComunicado";
    $bd->query($sql);
    $bd->bind(':idComunicado', $idComunicado);
    $bd->execute();
    $bd->close();
  }

  public function postResiduo($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Residuo (TB_ComunicadoAcidente_idTb_ComunicadoAcidente, Identificacao, Quantidade, DestinacaoDada) VALUES (:comunicado, :identificacao, :quantidade, :destinacaoDada)";
    $bd->query($sql);
    $bd->bind(':comunicado', $dados["TB_ComunicadoAcidente_idTB_ComunicadoAcidente"]);
    $bd->bind(':identificacao', $dados["Identificacao"]);
    $bd->bind(':quantidade', $dados["Quantidade"]);
    $bd->bind(':destinacaoDada', $dados["DestinacaoDada"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putResiduo($idResiduo, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_Residuo SET TB_ComunicadoAcidente_idTb_ComunicadoAcidente=:comunicado, Identifiacao=:identificacao, Quantidade=:quantidade, DestinacaoDada=:destinacaoDada WHERE idTB_Residuo = :idResiduo";
    $bd->query($sql);
    $bd->bind(':idResiduo', $idResiduo);
    $bd->bind(':comunicado', $dados["TB_ComunicadoAcidente_idTB_ComunicadoAcidente"]);
    $bd->bind(':identificacao', $dados["Identificacao"]);
    $bd->bind(':quantidade', $dados["Quantidade"]);
    $bd->bind(':destinacaoDada', $dados["DestinacaoDada"]);
    $bd->execute();
    $bd->close();
  }

}
