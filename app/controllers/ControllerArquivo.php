<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerArquivo
 *
 * @author ADM
 */
use App\Modulos\Arquivo;
use App\Modulos\BD;

class ControllerArquivo {

  public function getArquivo($idArquivo) {
    if ($idArquivo == NULL) {
      $arq = new Arquivo();
      return $arq->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_Arquivo WHERE idTB_Arquivo = :idArquivo";
      $bd->query($sql);
      $bd->bind(':idArquivo', $idArquivo);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $arq = new Arquivo($row['idTB_Arquivo'], $row['Nome'], $row['Tipo'], $row['Descricao'], $row['TamanhoArquivo'], $row['TB_Pasta_idTB_Pasta'], $row['TB_Usuario_idTB_Usuario']);
        $arquivo = $arq->toArray();
      } else {
        $arquivo = null;
      }
      $bd->close();
      return $arquivo;
    }
  }

  public function getArquivos($pasta) {
    $bd = new BD();
    $sql = "SELECT * FROM TB_Arquivo WHERE Tb_Pasta_idTB_Pasta =:pasta";
    $bd->query($sql);
    $bd->bind(':pasta', $pasta);
    if ($bd->execute()) {
      $arquivos = array();
      while ($row = $bd->single()) {
        $arq = new Arquivo($row['idTB_Arquivo'], $row['Nome'], $row['Tipo'], $row['Descricao'], $row['TamanhoArquivo'], $row['TB_Pasta_idTB_Pasta'], $row['TB_Usuario_idTB_Usuario']);
        $arquivos[] = $arq->toArray();
      }
    } else {
      $arquivos = null;
    }
    $bd->close();
    return $arquivos;
  }

  public function deleteArquivo($idArquivo) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Arquivo WHERE idTB_Arquivo = :idArquivo";
    $bd->query($sql);
    $bd->bind(':idArquivo', $idArquivo);
    $bd->execute();
    $bd->close();
  }

  public function postArquivo($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Arquivo (Nome, Tipo, Descricao, TamanhoArquivo, TB_Pasta_idTB_Pasta, TB_Usuario_idTB_Usuario) VALUES (:nome, :tipo, :descricao, :tamanhoArquivo, :pasta, :usuario)";
    $bd->query($sql);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->bind(':tipo', $dados["Tipo"]);
    $bd->bind(':descricao', $dados["Descricao"]);
    $bd->bind(':tamanhoArquivo', $dados["TamanhoArquivo"]);
    $bd->bind(':pasta', $dados["TB_Pasta_idTB_Pasta"]);
    $bd->bind(':usuario', $dados["TB_Usuario_idTB_Usuario"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putArquivo($idArquivo, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_Arquivo SET Nome=:nome, Tipo=:tipo, Descricao=:descricao, TamanhoArquivo=:tamanhoArquivo, TB_Pasta_idTB_Pasta=:pasta, TB_Usuario_idTB_Usuario=:usuario WHERE idTB_Arquivo = :idArquivo";
    $bd->query($sql);
    $bd->bind(':idArquivo', $idArquivo);
    $bd->bind(':nome', $dados["Nome"]);
    $bd->bind(':tipo', $dados["Tipo"]);
    $bd->bind(':descricao', $dados["Descricao"]);
    $bd->bind(':tamanhoArquivo', $dados["TamanhoArquivo"]);
    $bd->bind(':pasta', $dados["TB_Pasta_idTB_Pasta"]);
    $bd->bind(':usuario', $dados["TB_Usuario_idTB_Usuario"]);
    $bd->execute();
    $bd->close();
  }

}
