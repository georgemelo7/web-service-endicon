<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerFoto
 *
 * @author ADM
 */
use App\Modulos\Foto;
use App\Modulos\BD;

class ControllerFoto {

  public function getFoto($idFoto) {
    if ($idFoto == NULL) {
      $fot = new Foto();
      return $fot->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_Foto WHERE idTB_Foto = :idFoto";
      $bd->query($sql);
      $bd->bind(':idFoto', $idFoto);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $fot = new Foto($row['idTB_Foto'], $row['Foto'], $row['TamanhoImagem'], $row['Descricao'], $row['TipoImagem'], $row['NomeImagem'], $row['TB_Desvio_idTB_Desvio']);
        //recupera desvio
        $controlDesvio = new ControllerDesvio;
        $fot->setDesvio($controlDesvio->getDesvio($fot->getDesvio()));

        $foto = $foto->toArray();
      } else {
        $foto = null;
      }
      $bd->close();
      return $foto;
    }
  }

  public function getFotos() {
    $bd = new BD();
    $sql = "SELECT * FROM TB_Foto";
    $bd->query($sql);
    if ($bd->execute()) {
      $fotos = array();
      while ($row = $bd->single()) {
        $fot = new Foto($row['idTB_Foto'], $row['Foto'], $row['TamanhoImagem'], $row['Descricao'], $row['TipoImagem'], $row['NomeImagem'], $row['TB_Desvio_idTB_Desvio']);

        $fotos[] = $fot->toArray();
      }
    } else {
      $fotos = null;
    }
    $bd->close();
    return $fotos;
  }

  public function deleteFoto($idFoto) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Foto WHERE idTB_Foto = :idFoto";
    $bd->query($sql);
    $bd->bind(':idFoto', $idFoto);
    $bd->execute();
    $bd->close();
  }

  public function postFuncao($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Foto (Foto, TamanhoImagem, Descricao, TipoImagem, NomeImagem, TB_Desvio_idTB_Desvio) VALUES (:foto, :tamanhoImagem, :descricao, :tipoImagem, :nomeImagem, :desvio)";
    $bd->query($sql);
    $bd->bind(':foto', $dados["Foto"]);
    $bd->bind(':tamanhoImagem', $dados["TamanhoImagem"]);
    $bd->bind(':descricao', $dados["Descricao"]);
    $bd->bind(':tipoImagem', $dados["TipoImagem"]);
    $bd->bind(':nomeImagem', $dados["NomeImagem"]);
    $bd->bind(':desvio', $dados["TB_Desvio_idTB_Desvio"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putFoto($idFoto, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_Foto SET Foto=:foto, TamanhoImagem=:tamanhoImagem, Descricao=:descricao, TipoImagem=:tipoImagem, NomeImagem=:nomeImagem, TB_Desvio_idTB_Desvio=:desvio WHERE idTB_Foto = :idFoto";
    $bd->query($sql);
    $bd->bind(':idFoto', $idFoto);
    $bd->bind(':foto', $dados["Foto"]);
    $bd->bind(':tamanhoImagem', $dados["TamanhoImagem"]);
    $bd->bind(':descricao', $dados["Descricao"]);
    $bd->bind(':tipoImagem', $dados["TipoImagem"]);
    $bd->bind(':nomeImagem', $dados["NomeImagem"]);
    $bd->bind(':desvio', $dados["TB_Desvio_idTB_Desvio"]);
    $bd->execute();
    $bd->close();
  }

}
