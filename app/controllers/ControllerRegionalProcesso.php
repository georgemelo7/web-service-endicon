<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerRegionalProcesso
 *
 * @author ADM
 */

use App\Modulos\BD;

class ControllerRegionalProcesso {
  
  public function deleteProcessos($referencia) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Regional_has_TB_Processo WHERE TB_Regional_idTB_Regional = :idRegional";
    $bd->query($sql);
    $bd->bind(':idRegional', $referencia);
    $bd->execute();
    $bd->close();
  }

  public function postProcesso($referencia, $processo) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Regional_has_TB_Processo (TB_Regional_idTB_Regional, TB_Processo_idTB_Processo) VALUES (:regional, :processo)";
    $bd->query($sql);
    $bd->bind(':regional', $referencia);
    $bd->bind(':processo', $processo);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }
  
}
