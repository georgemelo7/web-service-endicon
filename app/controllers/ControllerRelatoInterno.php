<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of FormularioInterno
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\RelatoInterno;

class ControllerRelatoInterno {

  public function getRelatoInterno($idRelatoInterno) {
    if ($idRelatoInterno == NULL) {
      $rel = new RelatoInterno();
      return $rel->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_RelatoInterno WHERE idTB_RelatoInterno = :idRelatoInterno";
      $bd->query($sql);
      $bd->bind(':idRelatoInterno', $idRelatoInterno);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $rel = new RelatoInterno($row['idTB_RelatoInterno'], $row['DataCriacao'], $row['HoraCriacao'], $row['HorarioInicioTrabalho'], $row['DataAtendimentoMedico'], $row['HorarioAtendimentoMedico'], $row['ApresentouCopiaAtestado'], $row['Equipe'], $row['PlacaVeiculo'], $row['TB_Funcionario_Matricula'], $row['TB_Testemunha_Matricula1'], $row['TB_ComunicadoAcidente_idTB_ComunicadoAcidente']);
        //recupera o funcionario
        $controlFuncionario = new ControllerFuncionario();
        $rel->setFuncionario($controlFuncionario->getFuncionarioLite($rel->getFuncionario()));
        //recupera o funcionario
        $rel->setTestemunha($controlFuncionario->getFuncionarioLite($rel->getTestemunha()));
        //recupera o comunicado
        $controlComunicado = new ControllerComunicado();
        $rel->setComunicado($controlComunicado->getComunicado($rel->getComunicado()));
        //perguntasRespondidas
        $controlResposta = new ControllerRespostaRelatoInterno();
        $bd1 = new BD();
        $sql = "SELECT * FROM TB_RelatoInterno_has_TB_PerguntaRisco WHERE TB_RelatoInterno_idTB_RelatoInterno=:idRelatoInterno";
        $bd1->query($sql);
        $bd1->bind(':idRelatoInterno', $rel->getId());
        if ($bd1->execute()) {
          $perguntasRespondidas = array();
          while ($row = $bd1->single()) {
            $perguntasRespondidas[] = $controlResposta->getResposta($rel->getId(), $row["TB_RelatoInterno_idTB_RelatoInterno"]);
          }
        } else {
          $perguntasRespondidas = null;
        }
        $rel->setPerguntasRespondidas($perguntasRespondidas);
        $bd1->close();

        $relatoInterno = $rel->toArray();
      } else {
        $relatoInterno = null;
      }
      $bd->close();
      return $relatoInterno;
    }
  }

  public function getRelatosInternos() {
    $bd = new BD();
    $sql = "SELECT * FROM TB_RelatoInterno";
    $bd->query($sql);
    if ($bd->execute()) {
      $relatosInternos = array();
      while ($row = $bd->single()) {
        $rel = new RelatoInterno($row['idTB_RelatoInterno'], $row['DataCriacao'], $row['HoraCriacao'], $row['HorarioInicioTrabalho'], $row['DataAtendimentoMedico'], $row['HorarioAtendimentoMedico'], $row['ApresentouCopiaAtestado'], $row['Equipe'], $row['PlacaVeiculo'], $row['TB_Funcionario_Matricula'], $row['TB_Funcionario_Matricula1'], $row['TB_ComunicadoAcidente_idTB_ComunicadoAcidente']);
        //recupera o funcionario
        $controlFuncionario = new ControllerFuncionario();
        $rel->setFuncionario($controlFuncionario->getFuncionarioLite($rel->getFuncionario()));
        //recupera o funcionario
        $rel->setTestemunha($controlFuncionario->getFuncionarioLite($rel->getTestemunha()));
        //recupera o comunicado
        $controlComunicado = new ControllerComunicado();
        $rel->setComunicado($controlComunicado->getComunicado($rel->getComunicado()));
        //perguntas respondidas
        $controlResposta = new ControllerRespostaRelatoInterno();
        $bd1 = new BD();
        $sql = "SELECT * FROM TB_RelatoInterno_has_TB_PerguntaRisco WHERE TB_RelatoInterno_idTB_RelatoInterno=:idRelatoInterno";
        $bd1->query($sql);
        $bd1->bind(':idRelatoInterno', $rel->getId());
        if ($bd1->execute()) {
          $perguntasRespondidas = array();
          while ($row = $bd1->single()) {
            $perguntasRespondidas[] = $controlResposta->getResposta($rel->getId(), $row["TB_RelatoInterno_idTB_RelatoInterno"]);
          }
        } else {
          $perguntasRespondidas = null;
        }
        $rel->setPerguntasRespondidas($perguntasRespondidas);
        $bd1->close();

        $relatosInternos[] = $rel->toArray();
      }
    } else {
      $relatosInternos = null;
    }
    $bd->close();
    return $relatosInternos;
  }

  public function deleteRelatoInterno($idRelatoInterno) {
    $bd = new BD();
    $sql = "DELETE FROM TB_RelatoInterno WHERE idTB_RelatoInterno = :idRelatoInterno";
    $bd->query($sql);
    $bd->bind(':idRelatoInterno', $idRelatoInterno);
    $bd->execute();
    $bd->close();
  }

  public function postRelatoInterno($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_RelatoInterno (DataCriacao, HoraCriacao, HorarioInicioTrabalho, DataAtendimentoMedico, HorarioAtendimentoMedico, ApresentouCopiaAtestado, Equipe, PlacaVeiculo, DanosMateriais, TB_Funcionario_Matricula, TB_Funcionario_Matricula1, TB_ComunicadoAcidente_idTB_ComunicadoAcidente) VALUES (:dataCriacao, :horaCriacao, :horarioInicioTrabalho, :dataAtendimentoMedico, :horarioAtendimentoMedico, :apresentouCopiaAtestado, :equipe, :placaVeiculo, :funcionario, :testemunha, :comunicado)";
    $bd->query($sql);
    $bd->bind(':dataCriacao', $dados["DataCriacao"]);
    $bd->bind(':horaCriacao', $dados["HoraCriacao"]);
    $bd->bind(':horarioInicioTrabalho', $dados["HorarioInicioTrabalho"]);
    $bd->bind(':dataAtendimentoMedico', $dados["DataAtendimentoMedico"]);
    $bd->bind(':horarioAtendimentoMedico', $dados["HorarioAtendimentoMedico"]);
    $bd->bind(':apresentouCopiaAtestado', $dados["ApresentouCopiaAtestado"]);
    $bd->bind(':equipe', $dados["Equipe"]);
    $bd->bind(':placaVeiculo', $dados["PlacaVeiculo"]);
    $bd->bind(':funcionario', $dados["TB_Funcionario_Matricula"]);
    $bd->bind(':testemunha', $dados["TB_Funcionario_Matricula1"]);
    $bd->bind(':comunicado', $dados["TB_ComunicadoAcidente_idTB_ComunicadoAcidente"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putRelatoInterno($idRelatoInterno, $dados) {
    $bd = new BD();
    $sql = "UPDATE TB_RelatoInterno SET DataCriacao=:dataCriacao, HoraCriacao=:horaCriacao, HorarioInicioTrabalho=:horarioInicioTrabalho, DataAtendimentoMedico=:dataAtendimentoMedico, HorarioAtendimentoMedico=:horarioAtendimentoMedico, ApresentouCopiaAtestado=:apresentouCopiaAtestado, Equipe=:equipe, PlacaVeiculo=:placaVeiculo, TB_Funcionario_Matricula=:funcionario, TB_Funcionario_Matricula1=:testemunha, TB_ComunicadoAcidente_idTB_ComunicadoAcidente=:comunicado  WHERE idTB_RelatoInterno = :idRelatoInterno";
    $bd->query($sql);
    $bd->bind(':idRelatoInterno', $idRelatoInterno);
    $bd->bind(':dataCriacao', $dados["DataCriacao"]);
    $bd->bind(':horaCriacao', $dados["HoraCriacao"]);
    $bd->bind(':horarioInicioTrabalho', $dados["HorarioInicioTrabalho"]);
    $bd->bind(':dataAtendimentoMedico', $dados["DataAtendimentoMedico"]);
    $bd->bind(':horarioAtendimentoMedico', $dados["HorarioAtendimentoMedico"]);
    $bd->bind(':apresentouCopiaAtestado', $dados["ApresentouCopiaAtestado"]);
    $bd->bind(':equipe', $dados["Equipe"]);
    $bd->bind(':placaVeiculo', $dados["PlacaVeiculo"]);
    $bd->bind(':funcionario', $dados["TB_Funcionario_Matricula"]);
    $bd->bind(':testemunha', $dados["TB_Funcionario_Matricula1"]);
    $bd->bind(':comunicado', $dados["TB_ComunicadoAcidente_idTB_ComunicadoAcidente"]);
    $bd->execute();
    $bd->close();
  }

}
