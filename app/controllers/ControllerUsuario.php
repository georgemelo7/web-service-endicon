<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerUsuario
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\Usuario;

class ControllerUsuario {

  public function getUsuario($login) {
    if ($login == NULL) {
      $usu = new Usuario();
      return $usu->toArray();
    } else {
      $bd = new BD();
      $sql = "SELECT * FROM TB_Usuario WHERE Login = :login";
      $bd->query($sql);
      $bd->bind(':login', $login);
      $bd->execute();
      $row = $bd->single();
      if (!empty($row)) {
        $usu = new Usuario($row["Login"], $row["UltimoAcesso"], $row["Permissao"]);
        $usuario = $usu->toArray();
      } else {
        $usuario = null;
      }
      $bd->close();
      return $usuario;
    }
  }

  public function getUsuarios() {
    $bd = new BD();
    $sql = "SELECT * FROM TB_Usuario";
    $bd->query($sql);
    if ($bd->execute()) {
      $usuarios = array();
      while ($row = $bd->single()) {
        $usu = new Usuario($row["Login"], $row["UltimoAcesso"], $row["Permissao"]);
        $usuarios[] = $usu->toArray();
      }
    } else {
      $usuarios = null;
    }
    $bd->close();
    return $usuarios;
  }

  public function deleteUsuario($dados) {
    $bd = new BD();
    $sql = "DELETE FROM TB_Usuario WHERE Login = :login";
    $bd->query($sql);
    $bd->bind(':login', $dados["Login"]);
    $bd->execute();
    $bd->close();
  }

  public function postUsuario($dados) {
    $bd = new BD();
    $sql = "INSERT INTO TB_Usuario (Login, Senha, UltimoAcesso, Permissao) VALUES (:login, :senha, :ultimoAcesso, :permissao)";
    $bd->query($sql);
    $bd->bind(':login', $dados["Login"]);
    $bd->bind(':senha', password_hash($dados["Senha"], PASSWORD_DEFAULT));
    $bd->bind(':ultimoAcesso', $dados["UltimoAcesso"]);
    $bd->bind(':permissao', $dados["Permissao"]);
    $bd->execute();
    $json = array(
        'id' => (int) $bd->lastInput()
    );
    $bd->close();
    return $json;
  }

  public function putUsuario($dados) {
    $bd = new BD();
    if ($dados["Senha"] == NULL) {
      $sql = "UPDATE TB_Usuario SET Login=:login, UltimoAcesso=:ultimoAcesso, Permissao=:permissao WHERE Login = :login";
      $bd->query($sql);
      $bd->bind(':login', $dados["Login"]);
      $bd->bind(':ultimoAcesso', $dados["UltimoAcesso"]);
      $bd->bind(':permissao', $dados["Permissao"]);
      $bd->execute();
      $bd->close();
    } else {
      $sql = "UPDATE TB_Usuario SET Login=:login, Senha=:senha, UltimoAcesso=:ultimoAcesso, Permissao=:permissao WHERE Login = :login";
      $bd->query($sql);
      $bd->bind(':login', $dados["Login"]);
      $bd->bind(':senha', password_hash($dados["Senha"], PASSWORD_DEFAULT));
      $bd->bind(':ultimoAcesso', $dados["UltimoAcesso"]);
      $bd->bind(':permissao', $dados["Permissao"]);
      $bd->execute();
      $bd->close();
    }
  }

}
