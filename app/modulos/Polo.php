<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Polo
 *
 * @author George Tassiano
 */
class Polo {

  private $id;
  private $nome;
  private $regional;
  private $localidadesPolo;

  public function __construct($id = -1, $nome = '', $regional = NULL, $localidadesPolo=array()) {
    $this->id = $id;
    $this->nome = $nome;
    $this->regional = $regional;
    $this->localidadesPolo = $localidadesPolo;
  }

  public function getid() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getNome() {
    return $this->nome;
  }

  public function setNome($nome) {
    $this->nome = $nome;
  }

  public function getRegional() {
    return $this->regional;
  }

  public function setRegional($regional) {
    $this->regional = $regional;
  }
  public function getLocalidadesPolo() {
    return $this->localidadesPolo;
  }

  public function setLocalidadesPolo($localidadesPolo) {
    $this->localidadesPolo = $localidadesPolo;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'nome' => $this->nome,
      'regional' => $this->regional,
        'localidadesPolo'=>  $this->localidadesPolo
    );
    return $json;
  }

}
