<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

class FormularioInterno {

  private $id;
  private $dataCriacao;
  private $horaCriacao;
  private $horarioInicioTrabalho;
  private $dataOcorrido;
  private $horaOcorrido;
  private $dataAtendimentoMedico;
  private $horarioAtendimentoMedico;
  private $apresentouCopiaAtestado;
  private $parceiro;
  private $perguntasRelato;

  public function __construct($id = -1, $dataCriacao = '', $horaCriacao = '', $horarioInicioTrabalho = '', $dataOcorrido = '', $horaOcorrido = '', $dataAtendimentoMedico = '', $horarioAtendimentoMedico = '', $apresentouCopiaAtestado = '', $parceiro = NULL, $perguntasRelato = array()) {
    $this->id = $id;
    $this->dataCriacao = $dataCriacao;
    $this->horaCriacao = $horaCriacao;
    $this->horarioInicioTrabalho = $horarioInicioTrabalho;
    $this->dataOcorrido = $dataOcorrido;
    $this->horaOcorrido = $horaOcorrido;
    $this->dataAtendimentoMedico = $dataAtendimentoMedico;
    $this->horarioAtendimentoMedico = $horarioAtendimentoMedico;
    $this->apresentouCopiaAtestado = $apresentouCopiaAtestado;
    $this->parceiro = $parceiro;
    $this->perguntasRelato = $perguntasRelato;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getDataCriacao() {
    return $this->dataCriacao;
  }

  public function setDataCriacao($dataCriacao) {
    $this->dataCriacao = $dataCriacao;
  }

  public function getHoraCriacao() {
    return $this->horaCriacao;
  }

  public function setHoraCriacao($horaCriacao) {
    $this->horaCriacao = $horaCriacao;
  }

  public function getHorarioInicioTrabalho() {
    return $this->horarioInicioTrabalho;
  }

  public function setHorarioInicioTrabalho($horarioInicioTrabalho) {
    $this->horarioInicioTrabalho = $horarioInicioTrabalho;
  }

  public function getDataOcorrido() {
    return $this->dataOcorrido;
  }

  public function setDataOcorrido($dataOcorrido) {
    $this->dataOcorrido = $dataOcorrido;
  }

  public function getHoraOcorrido() {
    return $this->horaOcorrido;
  }

  public function setHoraOcorrido($horaOcorrido) {
    $this->horaOcorrido = $horaOcorrido;
  }

  public function getDataAtendimentoMedico() {
    return $this->dataAtendimentoMedico;
  }

  public function setDataAtendimentoMedico($dataAtendimentoMedico) {
    $this->dataAtendimentoMedico = $dataAtendimentoMedico;
  }

  public function getHorarioAtendimentoMedico() {
    return $this->horarioAtendimentoMedico;
  }

  public function setHorarioAtendimentoMedico($horaAtendimentoMedico) {
    $this->horarioAtendimentoMedico = $horaAtendimentoMedico;
  }

  public function getApresentouCopiaAtestado() {
    return $this->apresentouCopiaAtestado;
  }

  public function setApresentouCopiaAtestado($apresentouCopiaAtestado) {
    $this->apresentouCopiaAtestado = $apresentouCopiaAtestado;
  }

  public function getParceiro() {
    return $this->parceiro;
  }

  public function setParceiro($parceiro) {
    $this->parceiro = $parceiro;
  }

  public function getPerguntasRelato() {
    return $this->perguntasRelato;
  }

  public function setPerguntasRelato($perguntasRelato) {
    $this->perguntasRelato = $perguntasRelato;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'dataCriacao' => $this->dataCriacao,
      'horaCriacao' => $this->horaCriacao,
      'horarioInicioTrabalho' => $this->horarioInicioTrabalho,
      'dataOcorrido' => $this->dataOcorrido,
      'horaOcorrido' => $this->horaOcorrido,
      'dataAtendimentoMedico' => $this->dataAtendimentoMedico,
      'horarioAtendimentoMedico' => $this->horarioAtendimentoMedico,
      'apresentouCopiaAtestado' => $this->apresentouCopiaAtestado,
      'parceiro' => $this->parceiro,
      'perguntasRelato' => $this->perguntasRelato
    );
    return $json;
  }

}
