<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Processo
 *
 * @author George Tassiano
 */

class Acompanhamento {

  private $idFuncionario;
  private $id;
  private $data;
  private $novosDados;
  private $dados;

  public function __construct($id = -1, $idFuncionario = NULL, $data = '', $novosDados = '', $dados = '') {
    $this->id = $id;
    $this->idFuncionario = $idFuncionario;
    $this->data = $data;
    $this->novosDados = $novosDados;
    $this->dados = $dados;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getIdFuncionario() {
    return $this->idFuncionario;
  }

  public function setIdFuncionario($idFuncionario) {
    $this->idFuncionario = $idFuncionario;
  }

  public function getData() {
    return $this->data;
  }

  public function setData($data) {
    $this->data = $data;
  }

  public function getNovosDados() {
    return $this->novosDados;
  }

  public function setNovosDados($novosDados) {
    $this->novosDados = $novosDados;
  }

  public function getDado() {
    return $this->dado;
  }

  public function setDado($dado) {
    $this->dado = $dado;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'idFuncionario' => $this->idFuncionario,
      'data' => $this->data,
      'novosDados' => $this->novosDados,
      'dados' => $this->dados
    );
    return $json;
  }

}
