<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of pergunta
 *
 * @author George Tassiano
 */

class Pergunta {

  private $id;
  private $pergunta;

  public function __construct($id = -1, $pergunta = '') {
    $this->id = $id;
    $this->pergunta = $pergunta;
  }

  public function getid() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getPergunta() {
    return $this->pergunta;
  }

  public function setPergunta($pergunta) {
    $this->pergunta = $pergunta;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'pergunta' => $this->pergunta
    );
    return $json;
  }

}
