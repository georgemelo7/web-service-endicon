<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Envolvido
 *
 * @author ADM
 */

class Envolvido {

  private $id;
  private $nome;
  private $envolvimento;
  private $relacao;
  private $comunicado;

  public function __construct($id = -1, $nome = '', $envolvimento = '', $relacao = '', $comunicado = NULL) {
    $this->id = $id;
    $this->nome = $nome;
    $this->envolvimento = $envolvimento;
    $this->relacao = $relacao;
    $this->comunicado = $comunicado;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getNome() {
    return $this->nome;
  }

  public function setNome($nome) {
    $this->nome = $nome;
  }

  public function getEnvolvimento() {
    return $this->envolvimento;
  }

  public function setEnvolvimento($envolvimento) {
    $this->envolvimento = $envolvimento;
  }

  public function getRelacao() {
    return $this->relacao;
  }

  public function setRelacao($relacao) {
    $this->relacao = $relacao;
  }

  public function getComunicado() {
    return $this->comunicado;
  }

  public function setComunicado($comunicado) {
    $this->comunicado = $comunicado;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'nome' => $this->nome,
      'envolvimento' => $this->envolvimento,
      'relacao' => $this->relacao,
      'comunicado' => $this->comunicado
    );
    return $json;
  }

}
