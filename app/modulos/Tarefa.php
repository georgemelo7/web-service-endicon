<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Tarefa
 *
 * @author ADM
 */
class Tarefa {

  private $id;
  private $nome;
  private $processo;

  public function __construct($id = -1, $nome = '', $processo = NULL) {
    $this->id = $id;
    $this->nome = $nome;
    $this->processo = $processo;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getNome() {
    return $this->nome;
  }

  public function setNome($nome) {
    $this->nome = $nome;
  }

  public function getProcesso() {
    return $this->processo;
  }

  public function setProcesso($processo) {
    $this->processo = $processo;
  }

  public function toArray() {
    $json = array(
        'id' => $this->id,
        'nome' => $this->nome,
        'processo' => $this->processo
    );
    return $json;
  }

}
