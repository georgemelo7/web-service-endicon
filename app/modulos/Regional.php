<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Regional
 *
 * @author George Tassiano
 */

class Regional {

  private $id;
  private $nome;
  private $estado;
  private $polos;
  private $processos;

  public function __construct($id = -1, $nome = '', $estado = '', $polos = array(), $processos = array()) {
    $this->id = $id;
    $this->nome = $nome;
    $this->estado = $estado;
    $this->polos = $polos;
    $this->processos = $processos;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getNome() {
    return $this->nome;
  }

  public function setNome($nome) {
    $this->nome = $nome;
  }

  public function getEstado() {
    return $this->estado;
  }

  public function setEstado($estado) {
    $this->estado = $estado;
  }
  public function getPolos() {
    return $this->polos;
  }

  public function setPolos($polos) {
    $this->polos = $polos;
  }
  public function getProcessos() {
    return $this->estado;
  }

  public function setProcessos($processos) {
    $this->processos = $processos;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'nome' => $this->nome,
      'estado' => $this->estado,
        'polos' => $this->polos,
        'processos'=>  $this->processos
    );
    return $json;
  }

}
