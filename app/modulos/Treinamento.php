<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Treinamento
 *
 * @author George Tassiano
 */

class Treinamento {

  private $id;
  private $nome;
  private $codigo;
  private $conteudoProgramatico;

  public function __construct($id = -1, $nome = '', $codigo = '', $conteudoProgramatico = '') {
    $this->id = $id;
    $this->nome = $nome;
    $this->codigo = $codigo;
    $this->conteudoProgramatico = $conteudoProgramatico;
  }

  public function getid() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getNome() {
    return $this->nome;
  }

  public function setNome($nome) {
    $this->nome = $nome;
  }

  public function getCodigo() {
    return $this->codigo;
  }

  public function setCodigo($codigo) {
    $this->codigo = $codigo;
  }

  public function getConteudoProgramatico() {
    return $this->conteudoProgramatico;
  }

  public function setConteudoProgramatico($conteudoProgramatico) {
    $this->conteudoProgramatico = $conteudoProgramatico;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'nome' => $this->nome,
      'codigo' => $this->codigo,
      'conteudoProgramatico' => $this->conteudoProgramatico
    );
    return $json;
  }

}
