<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of classe_residuos
 *
 * @author George Tassiano
 */

namespace App\Modulos;

class Residuo {

  private $id;
  private $idComunicado;
  private $identificacao;
  private $quantidade;
  private $destinacaoDada;

  public function __construct($id = -1, $identificacao = '', $quantidade = '', $destinacaoDada = '', $idComunicado = NULL) {
    $this->id = $id;
    $this->idComunicado = $idComunicado;
    $this->identificacao = $identificacao;
    $this->quantidade = $quantidade;
    $this->destinacaoDada = $destinacaoDada;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getIdComunicado() {
    return $this->idComunicado;
  }

  public function setIdComunicado($idComunicado) {
    $this->idComunicado = $idComunicado;
  }

  public function getIdentificacao() {
    return $this->identificacao;
  }

  public function setIdentificacao($identificacao) {
    $this->identificacao = $identificacao;
  }

  public function getQuantidade() {
    return $this->quantidade;
  }

  public function setQuantidade($quantidade) {
    $this->quantidade = $quantidade;
  }

  public function getDestinacaoDada() {
    return $this->destinacaoDada;
  }

  public function setDestinacaoDada($destinacaoDada) {
    $this->destinacaoDada = $destinacaoDada;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'identificacao' => $this->identificacao,
      'quantidade' => $this->quantidade,
      'destinacaoDada' => $this->destinacaoDada,
      'idComunicado' => $this->idComunicado
    );
    return $json;
  }

}
