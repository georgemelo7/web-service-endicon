<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Pasta
 *
 * @author George Tassiano
 */
class Pasta {

  private $id;
  private $nome;
  private $pastaPai;

  public function __construct($id = -1, $nome = '', $pastaPai = NULL) {
    $this->id = $id;
    $this->nome = $nome;
    $this->pastaPai = $pastaPai;
  }

  public function getid() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getNome() {
    return $this->nome;
  }

  public function setNome($nome) {
    $this->nome = $nome;
  }

  public function getPastaPai() {
    return $this->pastaPai;
  }

  public function setPastaPai($pastaPai) {
    $this->pastaPai = $pastaPai;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'nome' => $this->nome,
      'pastaPai' => $this->pastaPai
    );
    return $json;
  }

}
