<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of DanoMaterial
 *
 * @author George Tassiano
 */
class DanoMaterial {

  private $id;
  private $nome;
  private $custo;
  private $relatorio;

  public function __construct($id = -1, $nome = '', $custo = '', $relatorio = '') {
    $this->id = $id;
    $this->nome = $nome;
    $this->custo = $custo;
  }

  public function getid() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getNome() {
    return $this->nome;
  }

  public function setNome($nome) {
    $this->nome = $nome;
  }

  public function getCusto() {
    return $this->custo;
  }

  public function setCusto($custo) {
    $this->custo = $custo;
  }

  public function getRelatorio() {
    return $this->relatorio;
  }

  public function setRelatorio($relatorio) {
    $this->relatorio = $relatorio;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'nome' => $this->nome,
      'custo' => $this->custo,
      'relatorio' => $this->relatorio
    );
    return $json;
  }

}
