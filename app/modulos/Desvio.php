<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

class Desvio {

  private $id;
  private $funcionario;
  private $dataOcorrido;
  private $horaOcorrido;
  private $registro;
  private $dataCriacao;
  private $horaCriacao;
  private $tipo;
  private $local;
  private $localidadePolo;

  public function __construct($id = -1, $dataCriacao = '', $horaCriacao = '', $tipo = '', $dataOcorrido = '', $horaOcorrido = '', $registro = '', $funcionario = NULL, $local = NULL, $localidadePolo = NULL) {
    $this->id = $id;
    $this->funcionario = $funcionario;
    $this->tipo = $tipo;
    $this->dataOcorrido = $dataOcorrido;
    $this->horaOcorrido = $horaOcorrido;
    $this->registro = $registro;
    $this->local = $local;
    $this->dataCriacao = $dataCriacao;
    $this->horaCriacao = $horaCriacao;
    $this->localidadePolo = $localidadePolo;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getFuncionario() {
    return $this->funcionario;
  }

  public function setFuncionario($funcionario) {
    $this->funcionario = $funcionario;
  }

  public function getTipo() {
    return $this->tipo;
  }

  public function setTipo($tipo) {
    $this->tipo = $tipo;
  }

  public function getDataOcorrido() {
    return $this->dataOcorrido;
  }

  public function setDataOcorrido($dataOcorrido) {
    $this->dataOcorrido = $dataOcorrido;
  }

  public function getHoraOcorrido() {
    return $this->horaOcorrido;
  }

  public function setHoraOcorrido($horaOcorrido) {
    $this->horaOcorrido = $horaOcorrido;
  }

  public function getRegistro() {
    return $this->registro;
  }

  public function setRegistro($registro) {
    $this->registro = $registro;
  }

  public function getLocal() {
    return $this->local;
  }

  public function setLocal($local) {
    $this->local = $local;
  }

  public function getDataCriacao() {
    return $this->dataCriacao;
  }

  public function setDataCriacao($dataCriacao) {
    $this->dataCriacao = $dataCriacao;
  }

  public function getHoraCriacao() {
    return $this->horaCriacao;
  }

  public function setHoraCriacao($horaCriacao) {
    $this->horaCriacao = $horaCriacao;
  }

  public function getLocalidadePolo() {
    return $this->localidadePolo;
  }

  public function setLocalidadePolo($localidadePolo) {
    $this->localidadePolo = $localidadePolo;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'dataCriacao' => $this->dataCriacao,
      'horaCriacao' => $this->horaCriacao,
      'tipo' => $this->tipo,
      'dataOcorrido' => $this->dataOcorrido,
      'horaOcorrido' => $this->horaOcorrido,
      'registro' => $this->registro,
      'funcionario' => $this->funcionario,
      'local' => $this->local,
      'localidadePolo' => $this->localidadePolo
    );
    return $json;
  }

}
