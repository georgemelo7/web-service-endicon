<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of relatorioInvestigacao
 *
 * @author George Tassiano
 */

class relatorioInvestigacao {

  private $id;
  private $dataCriacao;
  private $horaCriacao;
  private $descricaoAcidente;
  private $porqueOcorreu;
  private $informacaoComplementar;
  private $atividadeMomentoAcidente;
  private $acidenteTrabalho;
  private $experienciaAnterior;
  private $faltouEquipamento;
  private $eraAdequado;
  private $justificativaEquipamento;
  private $horaExtra;
  private $classificacao;
  private $atendimento;
  private $relato;
  private $funcionario;
  private $preenchidoPor;

  public function __construct($id = -1, $dataCriacao = '', $horaCriacao = '', $descricaoAcidente = '', $porqueOcorreu = '', $informacaoComplementar = '', $atividadeMomentoAcidente = '', $acidenteTrabalho = '', $experienciaAnterior = '', $faltouEquipamento = '', $eraAdequado = '', $justificativaEquipamento = '', $horaExtra = '', $classificacao = '', $atendimento = '', $relato = NULL, $funcionario = NULL, $preenchidoPor = NULL) {
    $this->id = $id;
    $this->dataCriacao = $dataCriacao;
    $this->horaCriacao = $horaCriacao;
    $this->descricaoAcidente = $descricaoAcidente;
    $this->porqueOcorreu = $porqueOcorreu;
    $this->informacaoComplementar = $informacaoComplementar;
    $this->atividadeMomentoAcidente = $atividadeMomentoAcidente;
    $this->acidenteTrabalho = $acidenteTrabalho;
    $this->experienciaAnterior = $experienciaAnterior;
    $this->faltouEquipamento = $faltouEquipamento;
    $this->eraAdequado = $eraAdequado;
    $this->justificativaEquipamento = $justificativaEquipamento;
    $this->horaExtra = $horaExtra;
    $this->classificacao = $classificacao;
    $this->atendimento = $atendimento;
    $this->relato = $relato;
    $this->funcionario = $funcionario;
    $this->preenchidoPor = $preenchidoPor;
  }

  public function getid() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getDataCriacao() {
    return $this->dataCriacao;
  }

  public function setDataCriacao($dataCriacao) {
    $this->dataCriacao = $dataCriacao;
  }

  public function getHoraCriacao() {
    return $this->horaCriacao;
  }

  public function setHoraCriacao($horaCriacao) {
    $this->horaCriacao = $horaCriacao;
  }

  public function getDescricaoAcidente() {
    return $this->descricaoAcidente;
  }

  public function setDescricaoAcidente($descricaoAcidente) {
    $this->descricaoAcidente = $descricaoAcidente;
  }

  public function getPorqueOcorreu() {
    return $this->porqueOcorreu;
  }

  public function setPorqueOcorreu($porqueOcorreu) {
    $this->porqueOcorreu = $porqueOcorreu;
  }

  public function getInformacaoComplementar() {
    return $this->informacaoComplementar;
  }

  public function setInformacaoComplementar($informacaoComplementar) {
    $this->informacaoComplementar = $informacaoComplementar;
  }

  public function getAtividadeMomentoAcidente() {
    return $this->atividadeMomentoAcidente;
  }

  public function setAtividadeMomentoAcidente($atividadeMomentoAcidente) {
    $this->atividadeMomentoAcidente = $atividadeMomentoAcidente;
  }

  public function getAcidenteTrabalho() {
    return $this->acidenteTrabalho;
  }

  public function setAcidenteTrabalho($acidenteTrabalho) {
    $this->acidenteTrabalho = $acidenteTrabalho;
  }

  public function getExperienciaAnterior() {
    return $this->experienciaAnterior;
  }

  public function setExperienciaAnterior($experienciaAnterior) {
    $this->experienciaAnterior = $experienciaAnterior;
  }

  public function getFaltouEquipamento() {
    return $this->faltouEquipamento;
  }

  public function setFaltouEquipamento($faltouEquipamento) {
    $this->faltouEquipamento = $faltouEquipamento;
  }

  public function getEraAdequado() {
    return $this->eraAdequado;
  }

  public function setEraAdequado($eraAdequado) {
    $this->eraAdequado = $eraAdequado;
  }

  public function getJustificativaEquipamento() {
    return $this->justificativaEquipamento;
  }

  public function setJustificativaEquipamento($justificativaEquipamento) {
    $this->justificativaEquipamento = $justificativaEquipamento;
  }

  public function getHoraExtra() {
    return $this->horaExtra;
  }

  public function setHoraExtra($horaExtra) {
    $this->horaExtra = $horaExtra;
  }

  public function getClassificacao() {
    return $this->classificacao;
  }

  public function setClassificacao($classificacao) {
    $this->classificacao = $classificacao;
  }

  public function getAtendimento() {
    return $this->atendimento;
  }

  public function setAtendimento($atendimento) {
    $this->atendimento = $atendimento;
  }

  public function getRelato() {
    return $this->relato;
  }

  public function setRelato($relato) {
    $this->relato = $relato;
  }

  public function getFuncionario() {
    return $this->funcionario;
  }

  public function setFuncionario($funcionario) {
    $this->funcionario = $funcionario;
  }

  public function getPreenchidoPor() {
    return $this->preenchidoPor;
  }

  public function setPreenchidoPor($preenchidoPor) {
    $this->preenchidoPor = $preenchidoPor;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'dataCriacao' => $this->dataCriacao,
      'horaCriacao' => $this->horaCriacao,
      'descricaoAcidente' => $this->descricaoAcidente,
      'porqueOcorreu' => $this->porqueOcorreu,
      'informacaoComplementar' => $this->informacaoComplementar,
      'atividadeMomentoAcidente' => $this->atividadeMomentoAcidente,
      'acidenteTrabalho' => $this->acidenteTrabalho,
      'experienciaAnterior' => $this->experienciaAnterior,
      'faltouEquipamento' => $this->faltouEquipamento,
      'eraAdequado' => $this->eraAdequado,
      'justificativaEquipamento' => $this->justificativaEquipamento,
      'horaExtra' => $this->horaExtra,
      'classificacao' => $this->classificacao,
      'atendimento' => $this->atendimento,
      'relato' => $this->relato,
      'funcionario' => $this->funcionario,
      'preenchidoPor' => $this->preenchidoPor
    );
    return $json;
  }

}
