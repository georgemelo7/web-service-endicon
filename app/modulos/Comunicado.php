<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

class Comunicado {

  private $id;
  private $tipo;
  private $dataCriacao;
  private $horaCriacao;
  private $tema;
  private $descricao;
  private $dataOcorrido;
  private $horaOcorrido;
  private $danosPessoais;
  private $danosMateriaisAmbientais;
  private $codigo;
  private $representanteSesmt;
  private $gravidade;
  private $naturezaLesao;
  private $parteCorpo;
  private $especificacaoRisco;
  private $numeroCat;
  private $local;
  private $funcionario;
  private $residuos;
  private $envolvidos;
  private $envolvidosFuncionarios;
  private $processo;
  private $analiseRisco;
  private $localidadePolo;
  private $tarefa;

  public function __construct($id = -1, $tipo = '', $dataCriacao = '', $horaCriacao = '', $tema = '', $descricao = '', $dataOcorrido = '', $horaOcorrido = '', $danosPessoais = '', $danosMateriaisAmbientais = '', $codigo='', $representanteSesmt='', $gravidade='', $naturezaLesao='', $parteCorpo='', $especificacaoRisco='', $numeroCat='', $tarefa='', $local = NULL, $funcionario = NULL, $processo = NULL, $localidadePolo = NULL, $analiseRisco = NULL, $residuos = array(), $envolvidos = array(), $envolvidosFuncionarios = array()) {
    $this->id = $id;
    $this->tipo = $tipo;
    $this->dataCriacao = $dataCriacao;
    $this->horaCriacao = $horaCriacao;
    $this->tema = $tema;
    $this->descricao = $descricao;
    $this->dataOcorrido = $dataOcorrido;
    $this->horaOcorrido = $horaOcorrido;
    $this->danosPessoais = $danosPessoais;
    $this->danosMateriaisAmbientais = $danosMateriaisAmbientais;
    $this->codigo=$codigo;
    $this->representanteSesmt=$representanteSesmt;
    $this->gravidade=$gravidade;
    $this->naturezaLesao=$naturezaLesao;
    $this->parteCorpo=$parteCorpo;
    $this->especificacaoRisco=$especificacaoRisco;
    $this->numeroCat=$numeroCat;
    $this->local = $local;
    $this->funcionario = $funcionario;
    $this->processo = $processo;
    $this->residuos = $residuos;
    $this->envolvidos = $envolvidos;
    $this->analiseRisco = $analiseRisco;
    $this->localidadePolo = $localidadePolo;
    $this->tarefa=$tarefa;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getTipo() {
    return $this->tipo;
  }

  public function setTipo($tipo) {
    $this->tipo = $tipo;
  }

  public function getDataCriacao() {
    return $this->dataCriacao;
  }

  public function setDataCriacao($dataCriacao) {
    $this->dataCriacao = $dataCriacao;
  }

  public function getHoraCriacao() {
    return $this->horaCriacao;
  }

  public function setHoraCriacao($horaCriacao) {
    $this->horaCriacao = $horaCriacao;
  }

  public function getTema() {
    return $this->tema;
  }

  public function setTema($tema) {
    $this->tema = $tema;
  }

  public function getDescricao() {
    return $this->descricao;
  }

  public function setDescricao($descricao) {
    $this->descricao = $descricao;
  }

  public function getDataOcorrido() {
    return $this->dataOcorrido;
  }

  public function setDataOcorrido($dataOcorrido) {
    $this->dataOcorrido = $dataOcorrido;
  }

  public function getHoraOcorrido() {
    return $this->horaOcorrido;
  }

  public function setHoraOcorrido($horaOcorrido) {
    $this->horaOcorrido = $horaOcorrido;
  }

  public function getDanosPessoais() {
    return $this->danosPessoais;
  }

  public function setDanosPessoais($danosPessoais) {
    $this->danosPessoais = $danosPessoais;
  }

  public function getDanosMateriaisAmbientais() {
    return $this->danosMateriaisAmbientais;
  }

  public function setDanosMateriaisAmbientais($danosMateriaisAmbientais) {
    $this->danosMateriaisAmbientais = $danosMateriaisAmbientais;
  }
  
  public function getCodigo() {
    return $this->codigo;
  }

  public function setCodigo($codigo) {
    $this->codigo = $codigo;
  }
  
  public function getRepresentanteSesmt() {
    return $this->representanteSesmt;
  }

  public function setRepresentanteSesmt($representanteSesmt) {
    $this->representanteSesmt = $representanteSesmt;
  }
  
  public function getGravidade() {
    return $this->gravidade;
  }

  public function setGravidade($gravidade) {
    $this->gravidade = $gravidade;
  }

  public function getNaturezaLesao() {
    return $this->naturezaLesao;
  }

  public function setNaturezaLesao($naturezaLesao) {
    $this->naturezaLesao = $naturezaLesao;
  }
  
  public function getParteCorpo() {
    return $this->parteCorpo;
  }

  public function setParteCorpo($parteCorpo) {
    $this->parteCorpo = $parteCorpo;
  }
  
  public function getEspecificacaoRisco() {
    return $this->especificacaoRisco;
  }

  public function setEspecificacaoRisco($especificacaoRisco) {
    $this->especificacaoRisco = $especificacaoRisco;
  }
  
  public function getNumeroCat() {
    return $this->numeroCat;
  }

  public function setNumeroCat($numeroCat) {
    $this->numeroCat = $numeroCat;
  }
  
  public function getLocal() {
    return $this->local;
  }

  public function setLocal($local) {
    $this->local = $local;
  }

  public function getFuncionario() {
    return $this->funcionario;
  }

  public function setFuncionario($funcionario) {
    $this->funcionario = $funcionario;
  }

  public function getResiduos() {
    return $this->residuos;
  }

  public function setResiduos($residuos) {
    $this->residuos = $residuos;
  }

  public function getEnvolvidos() {
    return $this->envolvidos;
  }

  public function setEnvolvidos($envolvidos) {
    $this->envolvidos = $envolvidos;
  }

  public function getEnvolvidosFuncionarios() {
    return $this->envolvidosFuncionarios;
  }

  public function setEnvolvidosFuncionarios($envolvidosFuncionarios) {
    $this->envolvidosFuncionarios = $envolvidosFuncionarios;
  }

  public function getProcesso() {
    return $this->processo;
  }

  public function setProcesso($processo) {
    $this->processo = $processo;
  }

  public function getAnaliseRisco() {
    return $this->analiseRisco;
  }

  public function setAnaliseRisco($analiseRisco) {
    $this->analiseRisco = $analiseRisco;
  }

  public function getLocalidadePolo() {
    return $this->localidadePolo;
  }

  public function setLocalidadePolo($localidadePolo) {
    $this->localidadePolo = $localidadePolo;
  }
  
  public function getTarefa() {
    return $this->tarefa;
  }

  public function setTarefa($tarefa) {
    $this->tarefa = $tarefa;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'tipo' => $this->tipo,
      'dataCriacao' => $this->dataCriacao,
      'horaCriacao' => $this->horaCriacao,
      'tema' => $this->tema,
      'descricao' => $this->descricao,
      'dataOcorrido' => $this->dataOcorrido,
      'horaOcorrido' => $this->horaOcorrido,
      'danosPessoais' => $this->danosPessoais,
      'danosMateriaisPessoais' => $this->danosMateriaisAmbientais,
        'codigo'=>  $this->codigo,
        'representanteSesmt'=>  $this->representanteSesmt,
        'gravidade'=>  $this->gravidade,
        'naturezaLesao'=>  $this->naturezaLesao,
        'parteCorpo'=>  $this->parteCorpo,
        'especificacaoRisco'=>  $this->especificacaoRisco,
        'numeroCat'=>  $this->numeroCat,
      'local' => $this->local,
      'funcionario' => $this->funcionario,
      'localidadePolo' => $this->localidadePolo,
      'residuos' => $this->residuos,
      'envolvidos' => $this->envolvidos,
      'envolvidosFuncionarios' => $this->envolvidosFuncionarios,
      'processo' => $this->processo,
      'analiseRisco' => $this->analiseRisco,
        'tarefa'=>  $this->tarefa
    );
    return $json;
  }

}
