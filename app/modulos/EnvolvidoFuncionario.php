<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of EnvolvidoFuncionario
 *
 * @author ADM
 */

class EnvolvidoFuncionario {

  private $idFuncionario;
  private $idComunicado;
  private $envolvimento;
  private $relacao;

  public function __construct($idFuncionario = NULL, $idComunicado = NULL, $envolvimento = '', $relacao = '') {
    $this->idFuncionario = $idFuncionario;
    $this->idComunicado = $idComunicado;
    $this->envolvimento = $envolvimento;
    $this->relacao = $relacao;
  }

  public function getIdFuncionario() {
    return $this->idFuncionario;
  }

  public function setIdFuncionario($idFuncionario) {
    $this->idFuncionario = $idFuncionario;
  }

  public function getIdComunicado() {
    return $this->idComunicado;
  }

  public function setIdComunicado($idComunicado) {
    $this->idComunicado = $idComunicado;
  }

  public function getEnvolvimento() {
    return $this->envolvimento;
  }

  public function setEnvolvimento($envolvimento) {
    $this->envolvimento = $envolvimento;
  }

  public function getRelacao() {
    return $this->relacao;
  }

  public function setRelacao($relacao) {
    $this->relacao = $relacao;
  }

  public function toArray() {
    $json = array(
      'idFuncionario' => $this->idFuncionario,
      'idComunicado' => $this->idComunicado,
      'envolvimento' => $this->envolvimento,
      'relacao' => $this->relacao
    );
    return $json;
  }

}
