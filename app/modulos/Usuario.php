<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Usuario
 *
 * @author George Tassiano
 */

class Usuario {
  private $login;
  private $ultimoAcesso;
  private $permissao;

  public function __construct($login = '', $ultimoAcesso = '', $permissao = '') {
    $this->login = $login;
    $this->ultimoAcesso = $ultimoAcesso;
    $this->permissao = $permissao;
  }

  public function getLogin() {
    return $this->login;
  }

  public function setLogin($login) {
    $this->login = $login;
  }

  public function getUltimoAcesso() {
    return $this->ultimoAcesso;
  }

  public function setUltimoAcesso($ultimoAcesso) {
    $this->ultimoAcesso = $ultimoAcesso;
  }

  public function getPermissao() {
    return $this->permissao;
  }

  public function setPermissao($permissao) {
    $this->permissao = $permissao;
  }

  public function toArray() {
    $json = array(
      'login' => $this->login,
      'ultimoAcesso' => $this->ultimoAcesso,
      'permissao' => $this->permissao
    );
    return $json;
  }

}
