<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Equipamento
 *
 * @author George Tassiano
 */

class Equipamento {

  private $id;
  private $nome;

  public function __construct($id = -1, $nome = '') {
    $this->id = $id;
    $this->nome = $nome;
  }

  public function getid() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getNome() {
    return $this->nome;
  }

  public function setNome($nome) {
    $this->nome = $nome;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'nome' => $this->nome
    );
    return $json;
  }

}
