<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of AtendimentoMedico
 *
 * @author George Tassiano
 */

class AtendimentoMedico {

  private $id;
  private $dataAtendimento;
  private $horaAtendimento;
  private $nomeLocal;
  private $houveInternacao;
  private $parteAtingida;
  private $atestado;

  public function __construct($id = -1, $dataAtendimento = '', $horaAtendimento = '', $nomeLocal = '', $houveInternacao = '', $parteAtingida = '', $atestado = '') {
    $this->id = $id;
    $this->dataAtendimento = $dataAtendimento;
    $this->horaAtendimento = $horaAtendimento;
    $this->nomeLocal = $nomeLocal;
    $this->houveInternacao = $houveInternacao;
    $this->parteAtingida = $parteAtingida;
    $this->atestado = $atestado;
  }

  public function getid() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getDataAtendimento() {
    return $this->dataAtendimento;
  }

  public function setDataAtendimento($dataAtendimento) {
    $this->dataAtendimento = $dataAtendimento;
  }

  public function getHoraAtendimento() {
    return $this->horaAtendimento;
  }

  public function setHoraAtendimento($horaAtendimento) {
    $this->horaAtendimento = $horaAtendimento;
  }

  public function getNomeLocal() {
    return $this->nomeLocal;
  }

  public function setNomeLocal($nomeLocal) {
    $this->nomeLocal = $nomeLocal;
  }

  public function getHouveInternacao() {
    return $this->houveInternacao;
  }

  public function setHouveInternacao($houveInternacao) {
    $this->houveInternacao = $houveInternacao;
  }

  public function getParteAtingida() {
    return $this->parteAtingida;
  }

  public function setParteAtingida($parteAtingida) {
    $this->parteAtingida = $parteAtingida;
  }

  public function getAtestado() {
    return $this->atestado;
  }

  public function setAtestado($atestado) {
    $this->atestado = $atestado;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'dataAtendimento' => $this->dataAtendimento,
      'horaAtendimento' => $this->horaAtendimento,
      'nomeLocal' => $this->nomeLocal,
      'houveInternacao' => $this->houveInternacao,
      'parteAtingida' => $this->parteAtingida,
      'atestado' => $this->atestado
    );
    return $json;
  }

}
