<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of TipoProcesso
 *
 * @author George Tassiano
 */

class TipoProcesso {

  private $id;
  private $nome;
  private $processo;
  private $regional;

  public function __construct($id = -1, $nome = '', $processo = NULL, $regional = NULL) {
    $this->id = $id;
    $this->nome = $nome;
    $this->processo = $processo;
    $this->regional = $regional;
  }

  public function getid() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getNome() {
    return $this->nome;
  }

  public function setNome($nome) {
    $this->nome = $nome;
  }

  public function getProcesso() {
    return $this->processo;
  }

  public function setProcesso($processo) {
    $this->processo = $processo;
  }

  public function getRegional() {
    return $this->regional;
  }

  public function setRegional($regional) {
    $this->regional = $regional;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'nome' => $this->nome,
      'processo' => $this->processo,
      'regional' => $this->regional
    );
    return $json;
  }

}
