<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

class AnaliseRisco {

  private $id;
  private $dataCriacao;
  private $horaCriacao;
  private $descricaoAtividade;
  private $dataOcorrido;
  private $atividadeSegura;
  private $justificativa;
  private $observacao;
  private $codigo;
  private $local;
  private $funcionario;
  private $processo;
  private $localidadePolo;
  private $tarefa;
  private $perguntasRespondidas;
  private $equipamentosIndividuais;
  private $equipamentosColetivos;
  private $riscosIdentificados;
  private $membros;

  public function __construct($id = -1, $dataCriacao = '', $horaCriacao = '', $dataOcorrido = '', $descricaoAtividade = '', $atividadeSegura = '', $justificativa = '', $observacao = '', $codigo = '', $local = NULL, $funcionario = NULL, $processo = NULL, $localidadePolo = NULL, $tarefa = NULL, $perguntasRespondidas = NULL, $equipamentosIndividuais = NULL, $equipamentosColetivos = NULL, $riscosIdentificados = NULL, $membros = NULL) {
    $this->id = $id;
    $this->dataCriacao = $dataCriacao;
    $this->horaCriacao = $horaCriacao;
    $this->dataOcorrido = $dataOcorrido;
    $this->descricaoAtividade = $descricaoAtividade;
    $this->atividadeSegura = $atividadeSegura;
    $this->justificativa = $justificativa;
    $this->observacao = $observacao;
    $this->codigo=$codigo;
    $this->local = $local;
    $this->funcionario = $funcionario;
    $this->processo = $processo;
    $this->localidadePolo = $localidadePolo;
    $this->tarefa = $tarefa;
    $this->perguntasRespondidas = $perguntasRespondidas;
    $this->equipamentosIndividuais = $equipamentosIndividuais;
    $this->equipamentosColetivos = $equipamentosColetivos;
    $this->riscosIdentificados = $riscosIdentificados;
    $this->membros = $membros;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getDataCriacao() {
    return $this->dataCriacao;
  }

  public function setDataCriacao($dataCriacao) {
    $this->dataCriacao = $dataCriacao;
  }

  public function getHoraCriacao() {
    return $this->horaCriacao;
  }

  public function setHoraCriacao($horaCriacao) {
    $this->horaCriacao = $horaCriacao;
  }

  public function getDataOcorrido() {
    return $this->DataOcorrido;
  }

  public function setDataOcorrido($dataOcorrido) {
    $this->DataOcorrido = $dataOcorrido;
  }

  public function getDescricaoAtividade() {
    return $this->descricaoAtividade;
  }

  public function setDescricaoAtividade($descricaoAtividade) {
    $this->descricaoAtividade = $descricaoAtividade;
  }

  public function getAtividadeSegura() {
    return $this->atividadeSegura;
  }

  public function setAtividadeSegura($atividadeSegura) {
    $this->atividadeSegura = $atividadeSegura;
  }

  public function getJustificativa() {
    return $this->justificativa;
  }

  public function setJustificativa($justificativa) {
    $this->justificativa = $justificativa;
  }

  public function getObservacao() {
    return $this->observacao;
  }

  public function setObservacao($observacao) {
    $this->observacao = $observacao;
  }
  public function getCodigo() {
    return $this->codigo;
  }

  public function setCodigo($codigo) {
    $this->codigo = $codigo;
  }

  public function getLocal() {
    return $this->local;
  }

  public function setLocal($local) {
    $this->local = $local;
  }

  public function getFuncionario() {
    return $this->funcionario;
  }

  public function setFuncionario($funcionario) {
    $this->funcionario = $funcionario;
  }

  public function getProcesso() {
    return $this->processo;
  }

  public function setProcesso($processo) {
    $this->processo = $processo;
  }

  public function getLocalidadePolo() {
    return $this->localidadePolo;
  }

  public function setLocalidadePolo($localidadePolo) {
    $this->localidadePolo = $localidadePolo;
  }
  
  public function getTarefa() {
    return $this->tarefa;
  }

  public function setTarefa($tarefa) {
    $this->tarefa = $tarefa;
  }

  public function getPerguntasRespondidas() {
    return $this->perguntasRespondidas;
  }

  public function setPerguntasRespondidas($perguntasRespondidas) {
    $this->perguntasRespondidas = $perguntasRespondidas;
  }

  public function getEquipamentosIndividuais() {
    return $this->equipamentosIndividuais;
  }

  public function setEquipamentosIndividuais($equipamentosIndividuais) {
    $this->equipamentosIndividuais = $equipamentosIndividuais;
  }

  public function getEquipamentosColetivos() {
    return $this->equipamentosColetivos;
  }

  public function setEquipamentosColetivos($equipamentosColetivos) {
    $this->equipamentosColetivos = $equipamentosColetivos;
  }

  public function getRiscosIdentificados() {
    return $this->riscosIdentificados;
  }

  public function setRiscosIdentificados($riscosIdentificados) {
    $this->riscosIdentificados = $riscosIdentificados;
  }

  public function getMembros() {
    return $this->membros;
  }

  public function setMembros($membros) {
    $this->membros = $membros;
  }

  public function toArray() {
    $json = array(
      'id' => $this->id,
      'dataCriacao' => $this->dataCriacao,
      'horaCriacao' => $this->horaCriacao,
      'dataOcorrido' => $this->dataOcorrido,
      'descricaoAtividade' => $this->descricaoAtividade,
      'atividadeSegura' => $this->atividadeSegura,
      'justificativa' => $this->justificativa,
      'observacao' => $this->observacao,
        'codigo'=> $this->codigo,
      'local' => $this->local,
      'funcionario' => $this->funcionario,
      'processo' => $this->processo,
      'localidadePolo' => $this->localidadePolo,
        'tarefa'=>  $this->tarefa,
      'perguntasRespondidas' => $this->perguntasRespondidas,
      'equipamentosIndividuais' => $this->equipamentosIndividuais,
      'equipamentosColetivos' => $this->equipamentosColetivos,
      'riscosIdentificados' => $this->riscosIdentificados,
      'membros' => $this->membros
    );
    return $json;
  }

}
