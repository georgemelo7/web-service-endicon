<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Resposta
 *
 * @author ADM
 */

class Resposta {

  private $idReferencia;
  private $idPergunta;
  private $resposta;

  public function __construct($idReferencia = NULL, $idPergunta = NULL, $resposta = '') {
    $this->idReferencia = $idReferencia;
    $this->idPergunta = $idPergunta;
    $this->resposta = $resposta;
  }

  public function getIdReferencia() {
    return $this->idReferencia;
  }

  public function setIdReferencia($idReferencia) {
    $this->idReferencia = $idReferencia;
  }

  public function getIdPergunta() {
    return $this->idPergunta;
  }

  public function setIdPergunta($idPergunta) {
    $this->idPergunta = $idPergunta;
  }

  public function getResposta() {
    return $this->resposta;
  }

  public function setResposta($resposta) {
    $this->resposta = $resposta;
  }

  public function toArray() {
    $json = array(
      'idReferencia' => $this->idReferencia,
      'idPergunta' => $this->idPergunta,
      'resposta' => $this->resposta
    );
    return $json;
  }

}
