<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FuncionarioTest
 *
 * @author George Tassiano
 */
use App\Modulos\Funcionario;
class FuncionarioTest extends PHPUnit_Framework_TestCase{
    /**
     * @test
     */
    public function TestToArray() {
        $comunicado = new Funcionario(200, 'ola', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        $array = array(
            'id'=>  200,
            'nome'=>  'ola',
            'matricula'=>  0,
            'dataNascimento'=>  0,
            'sexo'=>  0,
            'estadoCivil'=>  0,
            'inicioFuncao'=>  0,
            'grauInstrucao'=>  0,
            'estadoInstrucao'=>  0,
            'inicioRefeicao'=>  0,
            'fimRefeicao'=>  0,
            'turnoTrabalho'=>  0,
            'nacionalidade'=>  0,
                'funcao'=>  0,
            'moradia'=>  0
        );
        
        $this->assertEquals($array, $comunicado->toArray());
    }
}
