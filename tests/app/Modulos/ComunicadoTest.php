<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//namespace Endicon\Test\Modulos;

/**
 * Description of ComunicadoTest
 *
 * @author George Tassiano
 */

use App\Modulos\Comunicado;
class ComunicadoTest extends PHPUnit_Framework_TestCase {

    /**
     * @test
     */
    public function TestToArray() {
        $comunicado = new Comunicado(200, 'ola', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        $array = array(
            'id' => 200,
            'tipo' => 'ola',
            'dataCriacao' => 0,
            'horaCriacao' => 0,
            'tema' => 0,
            'descricao' => 0,
            'dataOcorrido' => 0,
            'horaOcorrido' => 0,
            'danosMateriais' => 0,
            'danosAmbientais' => 0,
            'local' => 0,
            'funcionario' => 0,
            'residuos' => 0,
            'envolvidos' => 0,
            'tipoProcesso'=>0
        );
        
        $this->assertEquals($array, $comunicado->toArray());
    }

}
