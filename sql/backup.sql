-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27-Jan-2017 às 03:47
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbendicon`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_acaocorretiva`
--

CREATE TABLE `tb_acaocorretiva` (
  `idTB_AcaoCorretiva` int(11) NOT NULL,
  `Acao` varchar(100) DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao` int(11) UNSIGNED NOT NULL,
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_acompanhamento`
--

CREATE TABLE `tb_acompanhamento` (
  `idTB_Acompanhamento` int(11) NOT NULL,
  `NovosDados` tinyint(1) DEFAULT NULL,
  `Dados` varchar(500) DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_analiseacidente`
--

CREATE TABLE `tb_analiseacidente` (
  `idTB_AnaliseAcidente` int(11) UNSIGNED NOT NULL,
  `Codigo` int(11) UNSIGNED DEFAULT NULL,
  `Descricao` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_analisecausa`
--

CREATE TABLE `tb_analisecausa` (
  `idTB_AnaliseCausa` int(11) NOT NULL,
  `Resposta` varchar(500) DEFAULT NULL,
  `TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_analiserisco`
--

CREATE TABLE `tb_analiserisco` (
  `idTB_AnaliseRisco` int(11) UNSIGNED NOT NULL,
  `DataCriacao` date DEFAULT NULL,
  `HoraCriacao` time DEFAULT NULL,
  `DescricaoAtividade` varchar(300) DEFAULT NULL,
  `Observacao` varchar(300) DEFAULT NULL,
  `TB_Local_idTB_Local` int(11) UNSIGNED NOT NULL,
  `DataOcorrido` date DEFAULT NULL,
  `Justificativa` varchar(500) DEFAULT NULL,
  `AtividadeSegura` tinyint(1) DEFAULT NULL,
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL,
  `TB_LocalidadePolo_idTB_LocalidadePolo` int(11) UNSIGNED NOT NULL,
  `Codigo` varchar(16) DEFAULT NULL,
  `TB_Processo_idTB_Processo` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_analiserisco_has_tb_equipamentocoletivo`
--

CREATE TABLE `tb_analiserisco_has_tb_equipamentocoletivo` (
  `TB_AnaliseRisco_idTB_AnaliseRisco` int(11) UNSIGNED NOT NULL,
  `TB_EquipamentoColetivo_idTB_EquipamentoColetivo` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_analiserisco_has_tb_equipamentoindividual`
--

CREATE TABLE `tb_analiserisco_has_tb_equipamentoindividual` (
  `TB_AnaliseRisco_idTB_AnaliseRisco` int(11) UNSIGNED NOT NULL,
  `TB_EquipamentoIndividual_idTB_EquipamentoIndividual` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_analiserisco_has_tb_perguntarisco`
--

CREATE TABLE `tb_analiserisco_has_tb_perguntarisco` (
  `TB_AnaliseRisco_idTB_AnaliseRisco` int(11) UNSIGNED NOT NULL,
  `TB_PerguntaRisco_idTB_PerguntaRisco` int(11) UNSIGNED NOT NULL,
  `Resposta` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_analiserisco_has_tb_riscoidentificado`
--

CREATE TABLE `tb_analiserisco_has_tb_riscoidentificado` (
  `TB_AnaliseRisco_idTB_AnaliseRisco` int(11) UNSIGNED NOT NULL,
  `TB_RiscoIdentificado_idTB_RiscoIdentificado` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_anexorelatorioinvestigacao`
--

CREATE TABLE `tb_anexorelatorioinvestigacao` (
  `idTB_AnexoRelatorioInvestigacao` int(11) NOT NULL,
  `Nome` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_atendimentomedico`
--

CREATE TABLE `tb_atendimentomedico` (
  `idTB_AtendimentoMedico` int(11) UNSIGNED NOT NULL,
  `DataAtendimento` date DEFAULT NULL,
  `HoraAtendimento` time DEFAULT NULL,
  `NomeLocal` varchar(100) DEFAULT NULL,
  `HouveInternacao` tinyint(1) DEFAULT NULL,
  `ParteAtingida` varchar(50) DEFAULT NULL,
  `TB_Atestado_idTB_Atestado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_atestado`
--

CREATE TABLE `tb_atestado` (
  `idTB_Atestado` int(11) NOT NULL,
  `InicioAtestado` date DEFAULT NULL,
  `FimAtestado` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categoriacausa`
--

CREATE TABLE `tb_categoriacausa` (
  `idTB_CategoriaCausa` int(11) NOT NULL,
  `Nome` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comite`
--

CREATE TABLE `tb_comite` (
  `idTB_Comite` int(11) NOT NULL,
  `DataAvaliacao` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comunicadoacidente`
--

CREATE TABLE `tb_comunicadoacidente` (
  `idTB_ComunicadoAcidente` int(11) UNSIGNED NOT NULL,
  `HoraCriacao` time DEFAULT NULL,
  `DataCriacao` date DEFAULT NULL,
  `Tema` varchar(50) DEFAULT NULL,
  `Descricao` varchar(300) DEFAULT NULL,
  `DataOcorrido` date DEFAULT NULL,
  `HoraOcorrido` time DEFAULT NULL,
  `DanosMateriaisAmbientais` varchar(300) DEFAULT NULL,
  `DanosPessoais` varchar(300) DEFAULT NULL,
  `TB_Local_idTB_Local` int(11) UNSIGNED NOT NULL,
  `Tipo` varchar(50) DEFAULT NULL,
  `TB_AnaliseRisco_idTB_AnaliseRisco` int(11) UNSIGNED DEFAULT NULL,
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL,
  `TB_LocalidadePolo_idTB_LocalidadePolo` int(11) UNSIGNED NOT NULL,
  `Codigo` varchar(16) DEFAULT NULL,
  `RepresentanteSesmt` varchar(100) DEFAULT NULL,
  `Gravidade` varchar(45) DEFAULT NULL,
  `NaturezaLesao` varchar(45) DEFAULT NULL,
  `ParteCorpo` varchar(45) DEFAULT NULL,
  `EspecificacaoRisco` varchar(45) DEFAULT NULL,
  `NumeroCat` varchar(45) DEFAULT NULL,
  `TB_Processo_idTB_Processo` int(11) UNSIGNED NOT NULL,
  `Tarefa` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_danomaterial`
--

CREATE TABLE `tb_danomaterial` (
  `idTB_DanoMaterial` int(11) UNSIGNED NOT NULL,
  `TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao` int(11) UNSIGNED NOT NULL,
  `Nome` varchar(50) DEFAULT NULL,
  `Custo` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_desvio`
--

CREATE TABLE `tb_desvio` (
  `idTB_Desvio` int(11) UNSIGNED NOT NULL,
  `DataOcorrido` date DEFAULT NULL,
  `HoraOcorrido` time DEFAULT NULL,
  `Registro` varchar(300) DEFAULT NULL,
  `TB_Local_idTB_Local` int(11) UNSIGNED NOT NULL,
  `DataCriacao` date DEFAULT NULL,
  `HoraCriacao` time DEFAULT NULL,
  `Tipo` varchar(50) DEFAULT NULL,
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL,
  `TB_LocalidadePolo_idTB_LocalidadePolo` int(11) UNSIGNED NOT NULL,
  `Codigo` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_eficacia`
--

CREATE TABLE `tb_eficacia` (
  `idTB_Eficacia` int(11) NOT NULL,
  `Data` date DEFAULT NULL,
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_envolvido`
--

CREATE TABLE `tb_envolvido` (
  `idTB_Envolvido` int(11) NOT NULL,
  `Nome` varchar(200) DEFAULT NULL,
  `Relacao` varchar(100) DEFAULT NULL,
  `Envolvimento` varchar(100) DEFAULT NULL,
  `TB_ComunicadoAcidente_idTB_ComunicadoAcidente` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_equipamentocoletivo`
--

CREATE TABLE `tb_equipamentocoletivo` (
  `idTB_EquipamentoColetivo` int(11) UNSIGNED NOT NULL,
  `Nome` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Extraindo dados da tabela `tb_equipamentocoletivo`
--

INSERT INTO `tb_equipamentocoletivo` (`idTB_EquipamentoColetivo`, `Nome`) VALUES
(2, 'Vara de manobra'),
(3, 'Manta Isolante'),
(4, 'Aterramento temporário'),
(5, 'Detector de tensão'),
(6, 'Load-buster'),
(7, 'Placa de sinalização'),
(8, 'Cone de sinalização'),
(9, 'Fita de sinalização'),
(10, 'Bastão isolado'),
(11, 'Coberturas Rígidas'),
(12, 'Manta isolante'),
(13, 'Linha de vida');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_equipamentoindividual`
--

CREATE TABLE `tb_equipamentoindividual` (
  `idTB_EquipamentoIndividual` int(11) UNSIGNED NOT NULL,
  `Nome` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Extraindo dados da tabela `tb_equipamentoindividual`
--

INSERT INTO `tb_equipamentoindividual` (`idTB_EquipamentoIndividual`, `Nome`) VALUES
(2, 'Capacete/Jugular'),
(3, 'Luva isolante BT'),
(4, 'Luva de algodão'),
(5, 'Luva de raspa'),
(6, 'Luva de cobertura'),
(7, 'Luva isolante AT'),
(8, 'Bota segurança'),
(9, 'Cinto Seg./Talabarte/Trava queda'),
(10, 'Bota sete léguas PVC'),
(11, 'Óculos de Proteção'),
(12, 'Manga isolante'),
(13, 'Protetor auricular');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_equipe`
--

CREATE TABLE `tb_equipe` (
  `idTB_Equipe` int(11) UNSIGNED NOT NULL,
  `Nome` varchar(100) DEFAULT NULL,
  `Codigo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_foto`
--

CREATE TABLE `tb_foto` (
  `idTB_Foto` int(11) UNSIGNED NOT NULL,
  `TB_Desvio_id_Desvio` int(11) UNSIGNED NOT NULL,
  `Foto` longblob,
  `TamanhoImagem` int(11) UNSIGNED DEFAULT NULL,
  `Descricao` varchar(200) DEFAULT NULL,
  `TipoImagem` varchar(50) DEFAULT NULL,
  `NomeImagem` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_funcao`
--

CREATE TABLE `tb_funcao` (
  `idTB_Funcao` int(11) NOT NULL,
  `Nome` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_funcao`
--

INSERT INTO `tb_funcao` (`idTB_Funcao`, `Nome`) VALUES
(2, 'Administrador de redes'),
(3, 'Ajudante'),
(4, 'Ajudante Eletricista'),
(5, 'Analista Auditoria'),
(6, 'Analista Contabil'),
(7, 'Analista Controladoria'),
(8, 'Analista Controle Operacional'),
(9, 'Analista Controle Qualidade'),
(10, 'Analista de Processos'),
(11, 'Analista DP'),
(12, 'Analista Financeiro'),
(13, 'Analistas Folha Pagamento'),
(14, 'Analista Juridico'),
(15, 'Analista Orcamentario'),
(16, 'Analista Sistema I'),
(17, 'Analista SN I'),
(18, 'Assist. Contabil'),
(19, 'Assist. Operacional'),
(20, 'Analista Planejamento'),
(21, 'Assist. Social'),
(22, 'Assist. Tecnico IX'),
(23, 'Assist. Tecnico VII'),
(24, 'Aux. Contabil'),
(25, 'Aux. Mecanico'),
(26, 'Aux. Operacional'),
(27, 'Aux. Pedreiro'),
(28, 'Aux. Sup. Tecnico'),
(29, 'Aux. TI'),
(30, 'Auxiliar de Frota'),
(31, 'Coord. Financeiro'),
(32, 'Coord. Processos'),
(33, 'Coord. RH'),
(34, 'Coord. Suprimentos'),
(35, 'Coord. Suprimentos III'),
(36, 'Coord. TI'),
(37, 'Executivo'),
(38, 'Ger. Contrato'),
(39, 'Ger. Controladoria'),
(40, 'Ger. Executivo'),
(41, 'Ger. Geral'),
(42, 'Ger. RH'),
(43, 'Gestor Processo'),
(44, 'Instrutor'),
(45, 'Auxiliar de Poda'),
(46, 'Jovem Aprendiz'),
(47, 'Lider Baixar OS'),
(48, 'Operador de logistica'),
(49, 'Servente Habilitado'),
(50, 'Conferente'),
(51, 'Soldador'),
(52, 'Supervisor Financeiro'),
(53, 'Supervisor II'),
(54, 'Supervisor IV'),
(55, 'Supervisor Plantão'),
(56, 'Supervisor RH'),
(57, 'Coord. Tecnico'),
(58, 'ENC. Obra'),
(59, 'ENC. Poda'),
(60, 'Encarregado LE'),
(61, 'Operador Maquina'),
(62, 'Operador Motopoda'),
(63, 'Recepcionista'),
(64, 'Revisor'),
(65, 'Revisor I'),
(66, 'Supervisor Operacional'),
(67, 'Aux. Eletricista'),
(68, 'Chefe Turma'),
(69, 'Ajudante Externo'),
(70, 'Alinhador Pneu'),
(71, 'Almoxarife'),
(72, 'Analista Administrativo'),
(73, 'Analista RH'),
(74, 'Arquivista'),
(75, 'Assist. Administrativo'),
(76, 'Assist. Logistica'),
(77, 'Assist. RH'),
(78, 'Atendente'),
(79, 'Aux. Administrativo'),
(80, 'Eletricista'),
(81, 'Aux. Almoxarife'),
(82, 'Aux. RH'),
(83, 'Aux. Serv. Gerais'),
(84, 'Borracheiro'),
(85, 'Cadista'),
(86, 'Chefe de Contas a Pagar'),
(87, 'Comprador'),
(88, 'Fiscal de Campo'),
(89, 'Frentista'),
(90, 'Contador'),
(91, 'Coord. Administrativo'),
(92, 'Coord. Almoxarifado'),
(93, 'Coord. Frota'),
(94, 'Coord. Manutenção'),
(95, 'Coord. Operacional'),
(96, 'Coord. Seg. Trabalho'),
(97, 'Coordenador'),
(98, 'Coordenador de Contrato'),
(99, 'Despachante'),
(100, 'Dir. Administrativo'),
(101, 'Eletricista Autos'),
(102, 'Eletrotecnico'),
(103, 'ENC. TI'),
(104, 'Encarregado'),
(105, 'Eng. Eletrico'),
(106, 'Eng. Seg. Trabalho'),
(107, 'Ger. Administrativo'),
(108, 'Ger. Auditoria'),
(109, 'Ger. Financeiro'),
(110, 'Ger. Operações'),
(111, 'Ger. Pessoal'),
(112, 'Ger. Produção'),
(113, 'Ger. Regional'),
(114, 'Ger. Segurança'),
(115, 'Gerente'),
(116, 'Gerente Juridico'),
(117, 'Operador Motoserra'),
(118, 'Lanterneiro'),
(119, 'Leiturista'),
(120, 'Podador Arvore'),
(121, 'Mecânico'),
(122, 'Médido Trabalho'),
(123, 'Mot. OP. Guin. Mov. Munck'),
(124, 'Motorista'),
(125, 'Motorista Munck'),
(126, 'Pedreiro'),
(127, 'Pintor'),
(128, 'Supervisor'),
(129, 'Porteiro'),
(130, 'Secretaria'),
(131, 'Serv. Gerais'),
(132, 'Supervisor Campo'),
(133, 'Supervisor Construcao'),
(134, 'Servente'),
(135, 'Superintendente Operações'),
(136, 'Supervisor Administrativo'),
(137, 'Supervisor Almoxarifado'),
(138, 'Supervisor Call Center'),
(139, 'Supervisor Frota'),
(140, 'Supervisor Manutenção'),
(141, 'Supervisor III'),
(142, 'Supervisor Logistica'),
(143, 'Supervisor Obras'),
(144, 'Tec. Eletrotecnico'),
(145, 'Tec. Enfermagem'),
(146, 'Tec. Informatica'),
(147, 'Tec. Operacional'),
(148, 'Tec. Seg. Trabalho'),
(149, 'Torneiro Mecânico'),
(150, 'Vigia'),
(151, 'Zelador');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_funcionario`
--

CREATE TABLE `tb_funcionario` (
  `Matricula` int(11) UNSIGNED NOT NULL,
  `Nome` varchar(100) DEFAULT NULL,
  `DataNascimento` date DEFAULT NULL,
  `Sexo` varchar(20) DEFAULT NULL,
  `EstadoCivil` varchar(20) DEFAULT NULL,
  `InicioFuncao` date DEFAULT NULL,
  `GrauInstrucao` varchar(30) DEFAULT NULL,
  `EstadoInstrucao` varchar(20) DEFAULT NULL,
  `InicioRefeicao` time DEFAULT NULL,
  `FinalRefeicao` time DEFAULT NULL,
  `TurnoTrabalho` varchar(30) DEFAULT NULL,
  `Nacionalidade` varchar(30) DEFAULT NULL,
  `TB_Local_idTB_Local` int(11) UNSIGNED DEFAULT NULL,
  `TB_Funcao_idTB_Funcao` int(11) DEFAULT NULL,
  `TB_LocalidadePolo_idTB_LocalidadePolo` int(11) UNSIGNED DEFAULT NULL,
  `InicioTrabalho` time DEFAULT NULL,
  `HoraExtra` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_funcionario_has_tb_analiserisco`
--

CREATE TABLE `tb_funcionario_has_tb_analiserisco` (
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL,
  `TB_AnaliseRisco_idTB_AnaliseRisco` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_funcionario_has_tb_comite`
--

CREATE TABLE `tb_funcionario_has_tb_comite` (
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL,
  `TB_Comite_idTB_Comite` int(11) NOT NULL,
  `Sigla` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_funcionario_has_tb_comunicadoacidente`
--

CREATE TABLE `tb_funcionario_has_tb_comunicadoacidente` (
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL,
  `TB_ComunicadoAcidente_idTB_ComunicadoAcidente` int(11) UNSIGNED NOT NULL,
  `Relacao` varchar(100) DEFAULT NULL,
  `Envolvimento` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_funcionario_has_tb_relatorioinvestigacao`
--

CREATE TABLE `tb_funcionario_has_tb_relatorioinvestigacao` (
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL,
  `TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_funcionario_has_tb_relatorioinvestigacao1`
--

CREATE TABLE `tb_funcionario_has_tb_relatorioinvestigacao1` (
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL,
  `TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao` int(11) UNSIGNED NOT NULL,
  `Data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_funcionario_has_tb_treinamento`
--

CREATE TABLE `tb_funcionario_has_tb_treinamento` (
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL,
  `TB_Treinamento_idTB_Treinamento` int(11) NOT NULL,
  `Data` date DEFAULT NULL,
  `Instrutor` varchar(100) DEFAULT NULL,
  `CargaHoraria` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_implantacao`
--

CREATE TABLE `tb_implantacao` (
  `idTB_Implantacao` int(11) NOT NULL,
  `Data` date DEFAULT NULL,
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_local`
--

CREATE TABLE `tb_local` (
  `idTB_Local` int(11) UNSIGNED NOT NULL,
  `Endereco` varchar(100) DEFAULT NULL,
  `CEP` int(11) UNSIGNED DEFAULT NULL,
  `PosicaoGeografica` varchar(150) DEFAULT NULL,
  `Estado` varchar(50) DEFAULT NULL,
  `Cidade` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_localidadepolo`
--

CREATE TABLE `tb_localidadepolo` (
  `idTB_LocalidadePolo` int(11) UNSIGNED NOT NULL,
  `TB_Polo_idTB_Polo` int(11) UNSIGNED NOT NULL,
  `Nome` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Extraindo dados da tabela `tb_localidadepolo`
--

INSERT INTO `tb_localidadepolo` (`idTB_LocalidadePolo`, `TB_Polo_idTB_Polo`, `Nome`) VALUES
(2, 3, 'Sobral'),
(3, 3, 'Camocim'),
(4, 3, 'São Benedito'),
(5, 3, 'Quixadá'),
(6, 3, 'Canindé'),
(7, 3, 'Nova Russas'),
(8, 3, 'Crateús'),
(9, 4, 'Juazeiro do Norte'),
(10, 4, 'Milagres'),
(11, 4, 'Campos Sales'),
(12, 5, 'Iguatú'),
(13, 5, 'Icó'),
(14, 5, 'Senador Pompeu'),
(15, 5, 'Tauá'),
(16, 6, 'Limoeiro do Norte'),
(17, 6, 'Aracati'),
(18, 6, 'Jaguaribe'),
(19, 7, 'Itapajé'),
(20, 7, 'Aracaú'),
(21, 7, 'Itapipoca'),
(22, 8, 'Cabo Frio'),
(23, 8, 'Petrópolis'),
(24, 8, 'Teresópolis'),
(25, 8, 'Niterói'),
(26, 8, 'Magé'),
(27, 8, 'Angra'),
(28, 8, 'Resende'),
(29, 8, 'Macaé'),
(30, 8, 'Campos'),
(31, 8, 'Cantagalo'),
(32, 8, 'Pádua'),
(33, 8, 'Itaperuna'),
(34, 8, 'São Gonçalo'),
(35, 8, 'Itaboraí'),
(36, 8, 'Maricá'),
(37, 9, 'Formosa'),
(38, 9, 'Porangatu'),
(39, 10, 'Metro. Belém'),
(40, 11, 'Abaetetuba'),
(41, 12, 'Castanhal'),
(42, 12, 'Capanema'),
(43, 12, 'Paragominas'),
(44, 13, 'Santarem'),
(45, 13, 'Itaituba'),
(46, 14, 'Altamira'),
(47, 15, 'manaus'),
(48, 16, 'IP Belém'),
(49, 17, 'IP Santarem'),
(50, 18, 'Salobo'),
(51, 19, 'Sossego'),
(52, 20, 'Unça Puma'),
(53, 21, 'Tomé Açu'),
(54, 22, 'Tocantins'),
(55, 23, 'Alagoinha'),
(56, 24, 'Itabuna'),
(57, 25, 'Santa Catarina'),
(58, 26, 'Manaus');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_medicao`
--

CREATE TABLE `tb_medicao` (
  `idTB_Medicao` int(11) NOT NULL,
  `TB_Regional_idTB_Regional` int(11) UNSIGNED NOT NULL,
  `TB_Equipe_idTB_Equipe` int(11) UNSIGNED NOT NULL,
  `TB_TipoProcesso_idTB_TipoProcesso` int(11) NOT NULL,
  `Mes` int(11) DEFAULT NULL,
  `Ano` year(4) DEFAULT NULL,
  `CentroDeCusto` double DEFAULT NULL,
  `MetaFaturamento` double DEFAULT NULL,
  `Produtividade` double DEFAULT NULL,
  `FaturamentoRealizado` double DEFAULT NULL,
  `HoraExtra` double DEFAULT NULL,
  `SobreAviso` double DEFAULT NULL,
  `KMExcedente` double DEFAULT NULL,
  `Reembolso` double DEFAULT NULL,
  `Total1` double DEFAULT NULL,
  `Indisponivel` double DEFAULT NULL,
  `Glosa` double DEFAULT NULL,
  `Multa` double DEFAULT NULL,
  `FaturamentoTotal` double DEFAULT NULL,
  `Porcentagem` double DEFAULT NULL,
  `Observacao` varchar(300) DEFAULT NULL,
  `Total2` double DEFAULT NULL,
  `TB_Funcionario_idTB_Funcionario` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_perda`
--

CREATE TABLE `tb_perda` (
  `idTB_Perda` int(11) NOT NULL,
  `DataCriacao` date DEFAULT NULL,
  `HoraCriacao` date DEFAULT NULL,
  `DataOcorrido` date DEFAULT NULL,
  `H_H` int(11) DEFAULT NULL,
  `PerdaFaturamento` double DEFAULT NULL,
  `PrevisaoNormalizacao` date DEFAULT NULL,
  `Observacao` varchar(300) DEFAULT NULL,
  `TB_Regional_idTB_Regional` int(11) UNSIGNED NOT NULL,
  `TB_Equipe_idTB_Equipe` int(11) UNSIGNED NOT NULL,
  `CentroDeCusto` double DEFAULT NULL,
  `TB_SubCategoriaCausa_idTB_SubCategoriaCausa` int(11) NOT NULL,
  `TB_Veiculo_idTB_Veiculo` int(11) UNSIGNED NOT NULL,
  `TB_Funcionario_idTB_Funcionario` int(11) UNSIGNED NOT NULL,
  `TB_Funcionario_idTB_Funcionario1` int(11) UNSIGNED NOT NULL,
  `TB_Funcionario_idTB_Funcionario2` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_perguntarelato`
--

CREATE TABLE `tb_perguntarelato` (
  `idTB_PerguntaRelato` int(11) UNSIGNED NOT NULL,
  `Pergunta` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Extraindo dados da tabela `tb_perguntarelato`
--

INSERT INTO `tb_perguntarelato` (`idTB_PerguntaRelato`, `Pergunta`) VALUES
(1, 'Qual tarefa era executada?'),
(2, 'Como era feito? Máquinas, Equipamentos, Ferramentas e EPI''s Utilizadas'),
(3, 'O que ocorreu? (Descrição do acidente)'),
(4, 'Quais foram as consequências e a(s) parte(s) do corpo atingida(s)?'),
(5, 'Você foi treinado e capacitado para execução deste serviço?'),
(6, 'Causas que contribuíram diretamente para ocorrência do acidente?'),
(7, 'Quais medidas podem ser tomadas para que este tipo de acidente seja evitado?');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_perguntarisco`
--

CREATE TABLE `tb_perguntarisco` (
  `idTB_PerguntaRisco` int(11) UNSIGNED NOT NULL,
  `Pergunta` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Extraindo dados da tabela `tb_perguntarisco`
--

INSERT INTO `tb_perguntarisco` (`idTB_PerguntaRisco`, `Pergunta`) VALUES
(1, 'O veículo está  estacionado em local seguro, calçado e sinalizado?'),
(2, 'Área de trabalho está isolada e sinalizada?'),
(3, 'A tarefa procede como programada na Ordem de Serviço?'),
(4, 'A tarefa foi planejada?'),
(5, 'O COD está ciente da manobra?'),
(6, 'A Rede está com religamento bloqueado?'),
(7, 'Foi analizado risco de energização acidental?'),
(8, 'Foram Instaladas placas indicativas?'),
(9, 'Foi realizado teste de ausência de tensão?'),
(10, 'Foram instalados os aterramentos temporários isolando o trecho de trabalho?'),
(11, 'O estado das estruturas a serem trabalhadas oferecem segurança?'),
(12, 'Os eletricistas foram orientados quanto as partes energizadas e desenergizadas?'),
(13, 'Equipamentos, ferramentas, EPI''s e EPC''s necessários para atividade, estão disponíveis?'),
(14, 'Equipamentos, ferramentas, EPI''s e EPC''s, para atividade, estão em condições de uso?'),
(15, 'O local de trabalho está desobstruído e com boa visibilidade para execução?'),
(16, 'Os trabalhadores de solo estão fora da área de risco de queda de materiais?'),
(17, 'Foram analisadas as condições das estruturas em função dos esforços?'),
(18, 'Foi verificado e existência de insetos e outros animais nocivos?'),
(19, 'As condições climáticas estão favoráveis para execução dos serviços?'),
(20, 'As escadas têm sapata e estão devidamente amarradas e ancoradas?'),
(21, 'O sistema de linha de vida está devidamente instalado?'),
(22, 'Os componentes da equipe, estão com condições física e psicológica para realização da atividade?');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_polo`
--

CREATE TABLE `tb_polo` (
  `idTB_Polo` int(11) UNSIGNED NOT NULL,
  `TB_Regional_idTB_Regional` int(11) UNSIGNED NOT NULL,
  `Nome` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Extraindo dados da tabela `tb_polo`
--

INSERT INTO `tb_polo` (`idTB_Polo`, `TB_Regional_idTB_Regional`, `Nome`) VALUES
(3, 3, 'Polo Norte/Centro Norte'),
(4, 3, 'Polo Sul'),
(5, 3, 'Polo Centro Sul'),
(6, 3, 'Polo Leste'),
(7, 3, 'Atlântico'),
(8, 4, 'Ampla'),
(9, 5, 'CELG'),
(10, 6, 'Metro. Belém'),
(11, 6, 'Baixo Tocantins'),
(12, 6, 'Nordeste'),
(13, 6, 'Oeste'),
(14, 6, 'Centro-Oeste'),
(15, 7, 'Manaus'),
(16, 8, 'IP Belém'),
(17, 9, 'IP Santarem'),
(18, 10, 'Salobo'),
(19, 10, 'Sossego'),
(20, 10, 'Unça Puma'),
(21, 11, 'Tomé Açu'),
(22, 13, 'Tocantins'),
(23, 14, 'Alagoinha'),
(24, 14, 'Itabuna'),
(25, 15, 'Santa Catarina'),
(26, 16, 'Manaus');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_processo`
--

CREATE TABLE `tb_processo` (
  `idTB_Processo` int(11) UNSIGNED NOT NULL,
  `Nome` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Extraindo dados da tabela `tb_processo`
--

INSERT INTO `tb_processo` (`idTB_Processo`, `Nome`) VALUES
(2, 'Linha Viva Mid Sky'),
(3, 'Linha Viva Sky'),
(4, 'Pesado'),
(5, 'Plantão'),
(6, 'Comercial'),
(7, 'Multifuncional'),
(8, 'Poda'),
(9, 'Fiscalização'),
(10, 'Medição Retroativa'),
(11, 'Serviços Operacional'),
(12, 'Serviços Administrativos'),
(13, 'Limpeza de Faixa'),
(14, 'Processo Leitura'),
(15, 'Linha Viva Lavagem'),
(16, 'Transmissão');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_regional`
--

CREATE TABLE `tb_regional` (
  `idTB_Regional` int(11) UNSIGNED NOT NULL,
  `Nome` varchar(100) DEFAULT NULL,
  `Estado` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Extraindo dados da tabela `tb_regional`
--

INSERT INTO `tb_regional` (`idTB_Regional`, `Nome`, `Estado`) VALUES
(3, 'COELCE', 'Ceará'),
(4, 'AMPLA', 'Rio de janeiro'),
(5, 'CELG', 'Goiás'),
(6, 'Celpa', 'Pará'),
(7, 'Eletrobras', 'Amazonas'),
(8, 'IP Belém', 'Pará'),
(9, 'IP Santarem', 'Pará'),
(10, 'Vale', 'Pará'),
(11, 'Hydro', 'Pará'),
(13, 'Celtins', 'Tocantins'),
(14, 'Coelba', 'Bahia'),
(15, 'Celesc', 'Santa Catarina'),
(16, 'Petrobrás', 'Amazonas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_regional_has_tb_processo`
--

CREATE TABLE `tb_regional_has_tb_processo` (
  `TB_Regional_idTB_Regional` int(11) UNSIGNED NOT NULL,
  `TB_Processo_idTB_Processo` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_relatointerno`
--

CREATE TABLE `tb_relatointerno` (
  `idTB_RelatoInterno` int(11) UNSIGNED NOT NULL,
  `DataCriacao` date DEFAULT NULL,
  `HoraCriacao` time DEFAULT NULL,
  `HorarioInicioTrabalho` time DEFAULT NULL,
  `DataAtendimentoMedico` date DEFAULT NULL,
  `HorarioAtendimentoMedico` time DEFAULT NULL,
  `ApresentouCopiaAtestado` tinyint(1) DEFAULT NULL,
  `TB_ComunicadoAcidente_idTB_ComunicadoAcidente` int(11) UNSIGNED NOT NULL,
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL,
  `TB_Funcionario_Matricula1` int(11) UNSIGNED NOT NULL,
  `Equipe` varchar(45) DEFAULT NULL,
  `PlacaVeiculo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_relatointerno_has_tb_perguntarelato`
--

CREATE TABLE `tb_relatointerno_has_tb_perguntarelato` (
  `TB_RelatoInterno_idTB_RelatoInterno` int(11) UNSIGNED NOT NULL,
  `TB_PerguntaRelato_idTB_PerguntaRelato` int(11) UNSIGNED NOT NULL,
  `Resposta` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_relatorioinvestigacao`
--

CREATE TABLE `tb_relatorioinvestigacao` (
  `idTB_RelatorioInvestigacao` int(11) UNSIGNED NOT NULL,
  `TB_AtendimentoMedico_idTB_AtendimentoMedico` int(11) UNSIGNED NOT NULL,
  `DescricaoAcidente` varchar(300) DEFAULT NULL,
  `PorqueOcorreu` varchar(300) DEFAULT NULL,
  `InformacaoComplementar` varchar(300) DEFAULT NULL,
  `AtividadeMomentoAcidente` varchar(300) DEFAULT NULL,
  `AcidenteTrabalho` tinyint(1) DEFAULT NULL,
  `ExperienciaAnterior` tinyint(1) DEFAULT NULL,
  `FaltouEquipamento` tinyint(1) DEFAULT NULL,
  `EraAdequado` tinyint(1) DEFAULT NULL,
  `JustificativaEquipamento` varchar(300) DEFAULT NULL,
  `HoraExtra` time DEFAULT NULL,
  `DataCriacao` date DEFAULT NULL,
  `HoraCriacao` time DEFAULT NULL,
  `TB_RelatoInterno_idTB_RelatoInterno` int(11) UNSIGNED NOT NULL,
  `Classificacao` varchar(50) DEFAULT NULL,
  `TB_Comite_idTB_Comite` int(11) NOT NULL,
  `AnaliseComplementar` varchar(500) DEFAULT NULL,
  `ObservacaoImportante` varchar(500) DEFAULT NULL,
  `TB_Eficacia_idTB_Eficacia` int(11) NOT NULL,
  `TB_Funcionario_Matricula` int(11) UNSIGNED NOT NULL,
  `TB_Funcionario_Matricula1` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_relatorioinvestigacao_has_tb_analiseacidente`
--

CREATE TABLE `tb_relatorioinvestigacao_has_tb_analiseacidente` (
  `TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao` int(11) UNSIGNED NOT NULL,
  `TB_AnaliseAcidente_idTB_AnaliseAcidente` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_relatorioinvestigacao_has_tb_anexorelatorioinvestigacao`
--

CREATE TABLE `tb_relatorioinvestigacao_has_tb_anexorelatorioinvestigacao` (
  `TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao` int(11) UNSIGNED NOT NULL,
  `TB_AnexoRelatorioInvestigacao_idTB_AnexoRelatorioInvestigacao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_residuo`
--

CREATE TABLE `tb_residuo` (
  `idTB_Residuo` int(11) UNSIGNED NOT NULL,
  `TB_ComunicadoAcidente_idTB_ComunicadoAcidente` int(11) UNSIGNED NOT NULL,
  `Identificacao` varchar(100) DEFAULT NULL,
  `Quantidade` int(11) DEFAULT NULL,
  `DestinacaoDada` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_riscoidentificado`
--

CREATE TABLE `tb_riscoidentificado` (
  `idTB_RiscoIdentificado` int(11) UNSIGNED NOT NULL,
  `Nome` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Extraindo dados da tabela `tb_riscoidentificado`
--

INSERT INTO `tb_riscoidentificado` (`idTB_RiscoIdentificado`, `Nome`) VALUES
(1, 'Choque elétrico'),
(2, 'Atropelamento'),
(3, 'Ferimentos, cortes, etc.'),
(4, 'Arco voltaico'),
(5, 'Postura inadequada'),
(6, 'Anim. Peçonhentos(abelha, cobra, etc)'),
(7, 'Queda de objetos'),
(8, 'Transp. manual de peso'),
(9, 'Ruídos'),
(10, 'Trabalho em altura'),
(11, 'Risco Químico(óleo, graxa)'),
(12, 'Movimentos repetitivos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_subcategoriacausa`
--

CREATE TABLE `tb_subcategoriacausa` (
  `idTB_SubCategoriaCausa` int(11) NOT NULL,
  `Nome` varchar(45) DEFAULT NULL,
  `TB_CategoriaCausa_idTB_CategoriaCausa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_tarefa`
--

CREATE TABLE `tb_tarefa` (
  `idTB_Tarefa` int(11) NOT NULL,
  `Nome` varchar(45) DEFAULT NULL,
  `TB_Processo_idTB_Processo` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_testemunha`
--

CREATE TABLE `tb_testemunha` (
  `idTB_Testemunha` int(11) UNSIGNED NOT NULL,
  `TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao` int(11) UNSIGNED NOT NULL,
  `CPF` int(11) UNSIGNED DEFAULT NULL,
  `Nome` varchar(50) DEFAULT NULL,
  `TB_Local_idTB_Local` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_treinamento`
--

CREATE TABLE `tb_treinamento` (
  `idTB_Treinamento` int(11) NOT NULL,
  `Nome` varchar(45) DEFAULT NULL,
  `Codigo` varchar(45) DEFAULT NULL,
  `ConteudoProgramatico` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_usuario`
--

CREATE TABLE `tb_usuario` (
  `Login` varchar(300) NOT NULL,
  `Senha` varchar(300) DEFAULT NULL,
  `UltimoAcesso` date DEFAULT NULL,
  `Permissao` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_usuario`
--

INSERT INTO `tb_usuario` (`Login`, `Senha`, `UltimoAcesso`, `Permissao`) VALUES
('amanda.silva@endicon.com.br', '$2y$10$v2C1WCUDEkGOYeL6sZ56DOdF/fidCgMp1tLRSQI.E4QQBbiXKqVV6', '2017-01-16', 'admin'),
('fabio.custodio@endicon.com.br', '$2y$10$7WsN12sDIz8EGp5qdLNVieIh1vhVXkLd/kY4E/IBatmjrspa63dCu', '2017-01-26', 'admin'),
('george.melo7@gmail.com', '$2y$10$ScTDbGdW426fZ61jLPbA/.AEXgqyKL2DmLW2mH4gKe8ri7kfBFTMG', '2017-01-11', 'admin'),
('gleyson.correa@endicon.com.br', '$2y$10$3XqUnaMMoHtdVPpsekdgfecHNpNjmhQnwIRM6PqIxM7M75FyssB/a', '2017-01-25', 'admin'),
('kildren.sales@endicon.com.br', '$2y$10$4.IFjB3R.LC6ThkrdkUDg.FIBM/JnmrJ28O2ZdyOGUjXw1jvYoiyy', '2017-01-25', 'admin'),
('mayara.sampaio@endicon.com.br', '$2y$10$u4tA/UKrHCpLzN5ygmI7N.OllBuYR7GPoWD7N8UiiDHShk7SnHVt.', '2017-01-26', 'admin'),
('renata.fernandes@endicon.com.br', '$2y$10$9tLlXX4TzJomVwPQLC9Gxe4UfZESb2hpP3r5QlBWntGY.tJG7Exxa', '2017-01-16', 'admin');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_veiculo`
--

CREATE TABLE `tb_veiculo` (
  `idTB_Veiculo` int(11) UNSIGNED NOT NULL,
  `Placa` varchar(50) DEFAULT NULL,
  `Tipo` int(11) UNSIGNED DEFAULT NULL,
  `Fabricante` varchar(45) DEFAULT NULL,
  `Modelo` varchar(100) DEFAULT NULL,
  `Chassi` varchar(50) DEFAULT NULL,
  `Renavam` int(11) DEFAULT NULL,
  `Ano` year(4) DEFAULT NULL,
  `Idade` int(11) DEFAULT NULL,
  `Familia` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 PACK_KEYS=0;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_acaocorretiva`
--
ALTER TABLE `tb_acaocorretiva`
  ADD PRIMARY KEY (`idTB_AcaoCorretiva`),
  ADD KEY `fk_TB_AcoesCorretivas_TB_RelatorioInvestigacao1_idx` (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`),
  ADD KEY `fk_TB_AcaoCorretiva_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`);

--
-- Indexes for table `tb_acompanhamento`
--
ALTER TABLE `tb_acompanhamento`
  ADD PRIMARY KEY (`idTB_Acompanhamento`),
  ADD KEY `fk_TB_Acompanhamento_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`);

--
-- Indexes for table `tb_analiseacidente`
--
ALTER TABLE `tb_analiseacidente`
  ADD PRIMARY KEY (`idTB_AnaliseAcidente`);

--
-- Indexes for table `tb_analisecausa`
--
ALTER TABLE `tb_analisecausa`
  ADD PRIMARY KEY (`idTB_AnaliseCausa`),
  ADD KEY `fk_TB_AnaliseCausa_TB_RelatorioInvestigacao1_idx` (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`);

--
-- Indexes for table `tb_analiserisco`
--
ALTER TABLE `tb_analiserisco`
  ADD PRIMARY KEY (`idTB_AnaliseRisco`),
  ADD KEY `fk_TB_AnaliseRisco_TB_Local1_idx` (`TB_Local_idTB_Local`),
  ADD KEY `fk_TB_AnaliseRisco_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`),
  ADD KEY `fk_TB_AnaliseRisco_TB_LocalidadePolo1_idx` (`TB_LocalidadePolo_idTB_LocalidadePolo`),
  ADD KEY `fk_TB_AnaliseRisco_TB_Processo1_idx` (`TB_Processo_idTB_Processo`);

--
-- Indexes for table `tb_analiserisco_has_tb_equipamentocoletivo`
--
ALTER TABLE `tb_analiserisco_has_tb_equipamentocoletivo`
  ADD PRIMARY KEY (`TB_AnaliseRisco_idTB_AnaliseRisco`,`TB_EquipamentoColetivo_idTB_EquipamentoColetivo`),
  ADD KEY `fk_TB_AnaliseRisco_has_TB_EquipamentosColetivos_TB_Equipame_idx` (`TB_EquipamentoColetivo_idTB_EquipamentoColetivo`),
  ADD KEY `fk_TB_AnaliseRisco_has_TB_EquipamentosColetivos_TB_AnaliseR_idx` (`TB_AnaliseRisco_idTB_AnaliseRisco`);

--
-- Indexes for table `tb_analiserisco_has_tb_equipamentoindividual`
--
ALTER TABLE `tb_analiserisco_has_tb_equipamentoindividual`
  ADD PRIMARY KEY (`TB_AnaliseRisco_idTB_AnaliseRisco`,`TB_EquipamentoIndividual_idTB_EquipamentoIndividual`),
  ADD KEY `fk_TB_AnaliseRisco_has_TB_EquipamentosIndividuais_TB_Equipa_idx` (`TB_EquipamentoIndividual_idTB_EquipamentoIndividual`),
  ADD KEY `fk_TB_AnaliseRisco_has_TB_EquipamentosIndividuais_TB_Analis_idx` (`TB_AnaliseRisco_idTB_AnaliseRisco`);

--
-- Indexes for table `tb_analiserisco_has_tb_perguntarisco`
--
ALTER TABLE `tb_analiserisco_has_tb_perguntarisco`
  ADD PRIMARY KEY (`TB_AnaliseRisco_idTB_AnaliseRisco`,`TB_PerguntaRisco_idTB_PerguntaRisco`),
  ADD KEY `TB_AnaliseRisco_has_TB_Perguntas_FKIndex1` (`TB_AnaliseRisco_idTB_AnaliseRisco`),
  ADD KEY `TB_AnaliseRisco_has_TB_Perguntas_FKIndex2` (`TB_PerguntaRisco_idTB_PerguntaRisco`);

--
-- Indexes for table `tb_analiserisco_has_tb_riscoidentificado`
--
ALTER TABLE `tb_analiserisco_has_tb_riscoidentificado`
  ADD PRIMARY KEY (`TB_AnaliseRisco_idTB_AnaliseRisco`,`TB_RiscoIdentificado_idTB_RiscoIdentificado`),
  ADD KEY `fk_TB_AnaliseRisco_has_TB_IdentificacaoRiscos_TB_Identifica_idx` (`TB_RiscoIdentificado_idTB_RiscoIdentificado`),
  ADD KEY `fk_TB_AnaliseRisco_has_TB_IdentificacaoRiscos_TB_AnaliseRis_idx` (`TB_AnaliseRisco_idTB_AnaliseRisco`);

--
-- Indexes for table `tb_anexorelatorioinvestigacao`
--
ALTER TABLE `tb_anexorelatorioinvestigacao`
  ADD PRIMARY KEY (`idTB_AnexoRelatorioInvestigacao`);

--
-- Indexes for table `tb_atendimentomedico`
--
ALTER TABLE `tb_atendimentomedico`
  ADD PRIMARY KEY (`idTB_AtendimentoMedico`),
  ADD UNIQUE KEY `HoraAtendimento_UNIQUE` (`HoraAtendimento`),
  ADD KEY `fk_TB_AtendimentoMedico_TB_DiaAtestado1_idx` (`TB_Atestado_idTB_Atestado`);

--
-- Indexes for table `tb_atestado`
--
ALTER TABLE `tb_atestado`
  ADD PRIMARY KEY (`idTB_Atestado`);

--
-- Indexes for table `tb_categoriacausa`
--
ALTER TABLE `tb_categoriacausa`
  ADD PRIMARY KEY (`idTB_CategoriaCausa`);

--
-- Indexes for table `tb_comite`
--
ALTER TABLE `tb_comite`
  ADD PRIMARY KEY (`idTB_Comite`);

--
-- Indexes for table `tb_comunicadoacidente`
--
ALTER TABLE `tb_comunicadoacidente`
  ADD PRIMARY KEY (`idTB_ComunicadoAcidente`),
  ADD KEY `fk_Table_ComunicadoAcidente_TB_Local1_idx` (`TB_Local_idTB_Local`),
  ADD KEY `fk_TB_ComunicadoAcidente_TB_AnaliseRisco1_idx` (`TB_AnaliseRisco_idTB_AnaliseRisco`),
  ADD KEY `fk_TB_ComunicadoAcidente_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`),
  ADD KEY `fk_TB_ComunicadoAcidente_TB_LocalidadePolo1_idx` (`TB_LocalidadePolo_idTB_LocalidadePolo`),
  ADD KEY `fk_TB_ComunicadoAcidente_TB_Processo1_idx` (`TB_Processo_idTB_Processo`);

--
-- Indexes for table `tb_danomaterial`
--
ALTER TABLE `tb_danomaterial`
  ADD PRIMARY KEY (`idTB_DanoMaterial`),
  ADD KEY `TB_DanoMaterial_FKIndex1` (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`);

--
-- Indexes for table `tb_desvio`
--
ALTER TABLE `tb_desvio`
  ADD PRIMARY KEY (`idTB_Desvio`),
  ADD KEY `fk_TB_Desvio_TB_Local1_idx` (`TB_Local_idTB_Local`),
  ADD KEY `fk_TB_Desvio_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`),
  ADD KEY `fk_TB_Desvio_TB_LocalidadePolo1_idx` (`TB_LocalidadePolo_idTB_LocalidadePolo`);

--
-- Indexes for table `tb_eficacia`
--
ALTER TABLE `tb_eficacia`
  ADD PRIMARY KEY (`idTB_Eficacia`),
  ADD KEY `fk_TB_Eficacia_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`);

--
-- Indexes for table `tb_envolvido`
--
ALTER TABLE `tb_envolvido`
  ADD PRIMARY KEY (`idTB_Envolvido`),
  ADD KEY `fk_TB_Envolvido_TB_ComunicadoAcidente1_idx` (`TB_ComunicadoAcidente_idTB_ComunicadoAcidente`);

--
-- Indexes for table `tb_equipamentocoletivo`
--
ALTER TABLE `tb_equipamentocoletivo`
  ADD PRIMARY KEY (`idTB_EquipamentoColetivo`);

--
-- Indexes for table `tb_equipamentoindividual`
--
ALTER TABLE `tb_equipamentoindividual`
  ADD PRIMARY KEY (`idTB_EquipamentoIndividual`);

--
-- Indexes for table `tb_equipe`
--
ALTER TABLE `tb_equipe`
  ADD PRIMARY KEY (`idTB_Equipe`);

--
-- Indexes for table `tb_foto`
--
ALTER TABLE `tb_foto`
  ADD PRIMARY KEY (`idTB_Foto`),
  ADD KEY `TB_Fotos_FKIndex1` (`TB_Desvio_id_Desvio`);

--
-- Indexes for table `tb_funcao`
--
ALTER TABLE `tb_funcao`
  ADD PRIMARY KEY (`idTB_Funcao`);

--
-- Indexes for table `tb_funcionario`
--
ALTER TABLE `tb_funcionario`
  ADD PRIMARY KEY (`Matricula`),
  ADD KEY `fk_TB_Funcionario_TB_Local1_idx` (`TB_Local_idTB_Local`),
  ADD KEY `fk_TB_Funcionario_TB_Funcao1_idx` (`TB_Funcao_idTB_Funcao`),
  ADD KEY `fk_TB_Funcionario_TB_LocalidadePolo1_idx` (`TB_LocalidadePolo_idTB_LocalidadePolo`);

--
-- Indexes for table `tb_funcionario_has_tb_analiserisco`
--
ALTER TABLE `tb_funcionario_has_tb_analiserisco`
  ADD PRIMARY KEY (`TB_Funcionario_Matricula`,`TB_AnaliseRisco_idTB_AnaliseRisco`),
  ADD KEY `fk_TB_Funcionario_has_TB_AnaliseRisco_TB_AnaliseRisco1_idx` (`TB_AnaliseRisco_idTB_AnaliseRisco`),
  ADD KEY `fk_TB_Funcionario_has_TB_AnaliseRisco_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`);

--
-- Indexes for table `tb_funcionario_has_tb_comite`
--
ALTER TABLE `tb_funcionario_has_tb_comite`
  ADD PRIMARY KEY (`TB_Funcionario_Matricula`,`TB_Comite_idTB_Comite`),
  ADD KEY `fk_TB_Funcionario_has_TB_Comite_TB_Comite1_idx` (`TB_Comite_idTB_Comite`),
  ADD KEY `fk_TB_Funcionario_has_TB_Comite_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`);

--
-- Indexes for table `tb_funcionario_has_tb_comunicadoacidente`
--
ALTER TABLE `tb_funcionario_has_tb_comunicadoacidente`
  ADD PRIMARY KEY (`TB_Funcionario_Matricula`,`TB_ComunicadoAcidente_idTB_ComunicadoAcidente`),
  ADD KEY `fk_TB_Funcionario_has_TB_ComunicadoAcidente_TB_ComunicadoAc_idx` (`TB_ComunicadoAcidente_idTB_ComunicadoAcidente`),
  ADD KEY `fk_TB_Funcionario_has_TB_ComunicadoAcidente_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`);

--
-- Indexes for table `tb_funcionario_has_tb_relatorioinvestigacao`
--
ALTER TABLE `tb_funcionario_has_tb_relatorioinvestigacao`
  ADD PRIMARY KEY (`TB_Funcionario_Matricula`,`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`),
  ADD KEY `fk_TB_Funcionario_has_TB_RelatorioInvestigacao_TB_Relatorio_idx` (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`),
  ADD KEY `fk_TB_Funcionario_has_TB_RelatorioInvestigacao_TB_Funcionar_idx` (`TB_Funcionario_Matricula`);

--
-- Indexes for table `tb_funcionario_has_tb_relatorioinvestigacao1`
--
ALTER TABLE `tb_funcionario_has_tb_relatorioinvestigacao1`
  ADD PRIMARY KEY (`TB_Funcionario_Matricula`,`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`),
  ADD KEY `fk_TB_Funcionario_has_TB_RelatorioInvestigacao1_TB_Relatori_idx` (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`),
  ADD KEY `fk_TB_Funcionario_has_TB_RelatorioInvestigacao1_TB_Funciona_idx` (`TB_Funcionario_Matricula`);

--
-- Indexes for table `tb_funcionario_has_tb_treinamento`
--
ALTER TABLE `tb_funcionario_has_tb_treinamento`
  ADD PRIMARY KEY (`TB_Funcionario_Matricula`,`TB_Treinamento_idTB_Treinamento`),
  ADD KEY `fk_TB_Funcionario_has_TB_Treinamento_TB_Treinamento1_idx` (`TB_Treinamento_idTB_Treinamento`),
  ADD KEY `fk_TB_Funcionario_has_TB_Treinamento_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`);

--
-- Indexes for table `tb_implantacao`
--
ALTER TABLE `tb_implantacao`
  ADD PRIMARY KEY (`idTB_Implantacao`),
  ADD KEY `fk_TB_Implantacao_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`);

--
-- Indexes for table `tb_local`
--
ALTER TABLE `tb_local`
  ADD PRIMARY KEY (`idTB_Local`);

--
-- Indexes for table `tb_localidadepolo`
--
ALTER TABLE `tb_localidadepolo`
  ADD PRIMARY KEY (`idTB_LocalidadePolo`),
  ADD KEY `TB_LocalidadePolo_FKIndex1` (`TB_Polo_idTB_Polo`);

--
-- Indexes for table `tb_medicao`
--
ALTER TABLE `tb_medicao`
  ADD PRIMARY KEY (`idTB_Medicao`),
  ADD KEY `fk_TB_Medicao_TB_Regional1_idx` (`TB_Regional_idTB_Regional`),
  ADD KEY `fk_TB_Medicao_TB_Equipe1_idx` (`TB_Equipe_idTB_Equipe`);

--
-- Indexes for table `tb_perda`
--
ALTER TABLE `tb_perda`
  ADD PRIMARY KEY (`idTB_Perda`),
  ADD KEY `fk_TB_Perdas_TB_Regional1_idx` (`TB_Regional_idTB_Regional`),
  ADD KEY `fk_TB_Perdas_TB_Equipe1_idx` (`TB_Equipe_idTB_Equipe`),
  ADD KEY `fk_TB_Perdas_TB_SubCategoriaCausa1_idx` (`TB_SubCategoriaCausa_idTB_SubCategoriaCausa`),
  ADD KEY `fk_TB_Perdas_TB_Veiculo1_idx` (`TB_Veiculo_idTB_Veiculo`);

--
-- Indexes for table `tb_perguntarelato`
--
ALTER TABLE `tb_perguntarelato`
  ADD PRIMARY KEY (`idTB_PerguntaRelato`);

--
-- Indexes for table `tb_perguntarisco`
--
ALTER TABLE `tb_perguntarisco`
  ADD PRIMARY KEY (`idTB_PerguntaRisco`);

--
-- Indexes for table `tb_polo`
--
ALTER TABLE `tb_polo`
  ADD PRIMARY KEY (`idTB_Polo`),
  ADD KEY `TB_Polo_FKIndex1` (`TB_Regional_idTB_Regional`);

--
-- Indexes for table `tb_processo`
--
ALTER TABLE `tb_processo`
  ADD PRIMARY KEY (`idTB_Processo`);

--
-- Indexes for table `tb_regional`
--
ALTER TABLE `tb_regional`
  ADD PRIMARY KEY (`idTB_Regional`);

--
-- Indexes for table `tb_regional_has_tb_processo`
--
ALTER TABLE `tb_regional_has_tb_processo`
  ADD PRIMARY KEY (`TB_Regional_idTB_Regional`,`TB_Processo_idTB_Processo`),
  ADD KEY `fk_TB_Regional_has_TB_Processo_TB_Processo1_idx` (`TB_Processo_idTB_Processo`),
  ADD KEY `fk_TB_Regional_has_TB_Processo_TB_Regional1_idx` (`TB_Regional_idTB_Regional`);

--
-- Indexes for table `tb_relatointerno`
--
ALTER TABLE `tb_relatointerno`
  ADD PRIMARY KEY (`idTB_RelatoInterno`),
  ADD KEY `fk_TB_RelatoInterno_TB_ComunicadoAcidente1_idx` (`TB_ComunicadoAcidente_idTB_ComunicadoAcidente`),
  ADD KEY `fk_TB_RelatoInterno_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`),
  ADD KEY `fk_TB_RelatoInterno_TB_Funcionario2_idx` (`TB_Funcionario_Matricula1`);

--
-- Indexes for table `tb_relatointerno_has_tb_perguntarelato`
--
ALTER TABLE `tb_relatointerno_has_tb_perguntarelato`
  ADD PRIMARY KEY (`TB_RelatoInterno_idTB_RelatoInterno`,`TB_PerguntaRelato_idTB_PerguntaRelato`),
  ADD KEY `TB_RelatoInterno_has_TB_Perguntas_Relato_FKIndex1` (`TB_RelatoInterno_idTB_RelatoInterno`),
  ADD KEY `TB_RelatoInterno_has_TB_Perguntas_Relato_FKIndex2` (`TB_PerguntaRelato_idTB_PerguntaRelato`);

--
-- Indexes for table `tb_relatorioinvestigacao`
--
ALTER TABLE `tb_relatorioinvestigacao`
  ADD PRIMARY KEY (`idTB_RelatorioInvestigacao`),
  ADD KEY `TB_RelatorioInvestigacao_FKIndex6` (`TB_AtendimentoMedico_idTB_AtendimentoMedico`),
  ADD KEY `fk_TB_RelatorioInvestigacao_TB_RelatoInterno1_idx` (`TB_RelatoInterno_idTB_RelatoInterno`),
  ADD KEY `fk_TB_RelatorioInvestigacao_TB_Comite1_idx` (`TB_Comite_idTB_Comite`),
  ADD KEY `fk_TB_RelatorioInvestigacao_TB_Eficacia1_idx` (`TB_Eficacia_idTB_Eficacia`),
  ADD KEY `fk_TB_RelatorioInvestigacao_TB_Funcionario1_idx` (`TB_Funcionario_Matricula`),
  ADD KEY `fk_TB_RelatorioInvestigacao_TB_Funcionario2_idx` (`TB_Funcionario_Matricula1`);

--
-- Indexes for table `tb_relatorioinvestigacao_has_tb_analiseacidente`
--
ALTER TABLE `tb_relatorioinvestigacao_has_tb_analiseacidente`
  ADD PRIMARY KEY (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`,`TB_AnaliseAcidente_idTB_AnaliseAcidente`),
  ADD KEY `TB_RelatorioInvestigacao_has_TB_AnaliseAcidente_FKIndex1` (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`),
  ADD KEY `TB_RelatorioInvestigacao_has_TB_AnaliseAcidente_FKIndex2` (`TB_AnaliseAcidente_idTB_AnaliseAcidente`);

--
-- Indexes for table `tb_relatorioinvestigacao_has_tb_anexorelatorioinvestigacao`
--
ALTER TABLE `tb_relatorioinvestigacao_has_tb_anexorelatorioinvestigacao`
  ADD PRIMARY KEY (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`,`TB_AnexoRelatorioInvestigacao_idTB_AnexoRelatorioInvestigacao`),
  ADD KEY `fk_TB_RelatorioInvestigacao_has_TB_AnexoRelatorioInvestigac_idx` (`TB_AnexoRelatorioInvestigacao_idTB_AnexoRelatorioInvestigacao`),
  ADD KEY `fk_TB_RelatorioInvestigacao_has_TB_AnexoRelatorioInvestigac_idx1` (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`);

--
-- Indexes for table `tb_residuo`
--
ALTER TABLE `tb_residuo`
  ADD PRIMARY KEY (`idTB_Residuo`),
  ADD KEY `TB_Residuos_FKIndex1` (`TB_ComunicadoAcidente_idTB_ComunicadoAcidente`);

--
-- Indexes for table `tb_riscoidentificado`
--
ALTER TABLE `tb_riscoidentificado`
  ADD PRIMARY KEY (`idTB_RiscoIdentificado`);

--
-- Indexes for table `tb_subcategoriacausa`
--
ALTER TABLE `tb_subcategoriacausa`
  ADD PRIMARY KEY (`idTB_SubCategoriaCausa`),
  ADD KEY `fk_TB_SubCategoriaCausa_TB_CategoriaCausa1_idx` (`TB_CategoriaCausa_idTB_CategoriaCausa`);

--
-- Indexes for table `tb_tarefa`
--
ALTER TABLE `tb_tarefa`
  ADD PRIMARY KEY (`idTB_Tarefa`),
  ADD KEY `fk_Tarefa_TB_Processo1_idx` (`TB_Processo_idTB_Processo`);

--
-- Indexes for table `tb_testemunha`
--
ALTER TABLE `tb_testemunha`
  ADD PRIMARY KEY (`idTB_Testemunha`),
  ADD KEY `Testemunha_FKIndex2` (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`),
  ADD KEY `fk_Testemunha_TB_Local1_idx` (`TB_Local_idTB_Local`);

--
-- Indexes for table `tb_treinamento`
--
ALTER TABLE `tb_treinamento`
  ADD PRIMARY KEY (`idTB_Treinamento`);

--
-- Indexes for table `tb_usuario`
--
ALTER TABLE `tb_usuario`
  ADD PRIMARY KEY (`Login`);

--
-- Indexes for table `tb_veiculo`
--
ALTER TABLE `tb_veiculo`
  ADD PRIMARY KEY (`idTB_Veiculo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_analiseacidente`
--
ALTER TABLE `tb_analiseacidente`
  MODIFY `idTB_AnaliseAcidente` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_analiserisco`
--
ALTER TABLE `tb_analiserisco`
  MODIFY `idTB_AnaliseRisco` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_atendimentomedico`
--
ALTER TABLE `tb_atendimentomedico`
  MODIFY `idTB_AtendimentoMedico` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_categoriacausa`
--
ALTER TABLE `tb_categoriacausa`
  MODIFY `idTB_CategoriaCausa` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_comunicadoacidente`
--
ALTER TABLE `tb_comunicadoacidente`
  MODIFY `idTB_ComunicadoAcidente` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_danomaterial`
--
ALTER TABLE `tb_danomaterial`
  MODIFY `idTB_DanoMaterial` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_desvio`
--
ALTER TABLE `tb_desvio`
  MODIFY `idTB_Desvio` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_envolvido`
--
ALTER TABLE `tb_envolvido`
  MODIFY `idTB_Envolvido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_equipamentocoletivo`
--
ALTER TABLE `tb_equipamentocoletivo`
  MODIFY `idTB_EquipamentoColetivo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_equipamentoindividual`
--
ALTER TABLE `tb_equipamentoindividual`
  MODIFY `idTB_EquipamentoIndividual` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_equipe`
--
ALTER TABLE `tb_equipe`
  MODIFY `idTB_Equipe` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_foto`
--
ALTER TABLE `tb_foto`
  MODIFY `idTB_Foto` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_funcao`
--
ALTER TABLE `tb_funcao`
  MODIFY `idTB_Funcao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;
--
-- AUTO_INCREMENT for table `tb_local`
--
ALTER TABLE `tb_local`
  MODIFY `idTB_Local` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tb_localidadepolo`
--
ALTER TABLE `tb_localidadepolo`
  MODIFY `idTB_LocalidadePolo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `tb_medicao`
--
ALTER TABLE `tb_medicao`
  MODIFY `idTB_Medicao` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_perda`
--
ALTER TABLE `tb_perda`
  MODIFY `idTB_Perda` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_perguntarelato`
--
ALTER TABLE `tb_perguntarelato`
  MODIFY `idTB_PerguntaRelato` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_perguntarisco`
--
ALTER TABLE `tb_perguntarisco`
  MODIFY `idTB_PerguntaRisco` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tb_polo`
--
ALTER TABLE `tb_polo`
  MODIFY `idTB_Polo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `tb_processo`
--
ALTER TABLE `tb_processo`
  MODIFY `idTB_Processo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_regional`
--
ALTER TABLE `tb_regional`
  MODIFY `idTB_Regional` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_relatointerno`
--
ALTER TABLE `tb_relatointerno`
  MODIFY `idTB_RelatoInterno` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_relatorioinvestigacao`
--
ALTER TABLE `tb_relatorioinvestigacao`
  MODIFY `idTB_RelatorioInvestigacao` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_residuo`
--
ALTER TABLE `tb_residuo`
  MODIFY `idTB_Residuo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_riscoidentificado`
--
ALTER TABLE `tb_riscoidentificado`
  MODIFY `idTB_RiscoIdentificado` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_subcategoriacausa`
--
ALTER TABLE `tb_subcategoriacausa`
  MODIFY `idTB_SubCategoriaCausa` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_testemunha`
--
ALTER TABLE `tb_testemunha`
  MODIFY `idTB_Testemunha` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_treinamento`
--
ALTER TABLE `tb_treinamento`
  MODIFY `idTB_Treinamento` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_veiculo`
--
ALTER TABLE `tb_veiculo`
  MODIFY `idTB_Veiculo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_acaocorretiva`
--
ALTER TABLE `tb_acaocorretiva`
  ADD CONSTRAINT `fk_TB_AcaoCorretiva_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_AcoesCorretivas_TB_RelatorioInvestigacao1` FOREIGN KEY (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`) REFERENCES `tb_relatorioinvestigacao` (`idTB_RelatorioInvestigacao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_acompanhamento`
--
ALTER TABLE `tb_acompanhamento`
  ADD CONSTRAINT `fk_TB_Acompanhamento_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_analisecausa`
--
ALTER TABLE `tb_analisecausa`
  ADD CONSTRAINT `fk_TB_AnaliseCausa_TB_RelatorioInvestigacao1` FOREIGN KEY (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`) REFERENCES `tb_relatorioinvestigacao` (`idTB_RelatorioInvestigacao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_analiserisco`
--
ALTER TABLE `tb_analiserisco`
  ADD CONSTRAINT `fk_TB_AnaliseRisco_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_AnaliseRisco_TB_Local1` FOREIGN KEY (`TB_Local_idTB_Local`) REFERENCES `tb_local` (`idTB_Local`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_AnaliseRisco_TB_LocalidadePolo1` FOREIGN KEY (`TB_LocalidadePolo_idTB_LocalidadePolo`) REFERENCES `tb_localidadepolo` (`idTB_LocalidadePolo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_AnaliseRisco_TB_Processo1` FOREIGN KEY (`TB_Processo_idTB_Processo`) REFERENCES `tb_processo` (`idTB_Processo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_analiserisco_has_tb_equipamentocoletivo`
--
ALTER TABLE `tb_analiserisco_has_tb_equipamentocoletivo`
  ADD CONSTRAINT `fk_TB_AnaliseRisco_has_TB_EquipamentosColetivos_TB_AnaliseRis1` FOREIGN KEY (`TB_AnaliseRisco_idTB_AnaliseRisco`) REFERENCES `tb_analiserisco` (`idTB_AnaliseRisco`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_AnaliseRisco_has_TB_EquipamentosColetivos_TB_Equipament1` FOREIGN KEY (`TB_EquipamentoColetivo_idTB_EquipamentoColetivo`) REFERENCES `tb_equipamentocoletivo` (`idTB_EquipamentoColetivo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_analiserisco_has_tb_equipamentoindividual`
--
ALTER TABLE `tb_analiserisco_has_tb_equipamentoindividual`
  ADD CONSTRAINT `fk_TB_AnaliseRisco_has_TB_EquipamentosIndividuais_TB_AnaliseR1` FOREIGN KEY (`TB_AnaliseRisco_idTB_AnaliseRisco`) REFERENCES `tb_analiserisco` (`idTB_AnaliseRisco`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_AnaliseRisco_has_TB_EquipamentosIndividuais_TB_Equipame1` FOREIGN KEY (`TB_EquipamentoIndividual_idTB_EquipamentoIndividual`) REFERENCES `tb_equipamentoindividual` (`idTB_EquipamentoIndividual`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_analiserisco_has_tb_perguntarisco`
--
ALTER TABLE `tb_analiserisco_has_tb_perguntarisco`
  ADD CONSTRAINT `fk_{06099650-968B-4C97-97A0-98A435E3096C}` FOREIGN KEY (`TB_AnaliseRisco_idTB_AnaliseRisco`) REFERENCES `tb_analiserisco` (`idTB_AnaliseRisco`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_{FF7006C2-43D4-48E3-B622-0AB72C8DE9F9}` FOREIGN KEY (`TB_PerguntaRisco_idTB_PerguntaRisco`) REFERENCES `tb_perguntarisco` (`idTB_PerguntaRisco`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_analiserisco_has_tb_riscoidentificado`
--
ALTER TABLE `tb_analiserisco_has_tb_riscoidentificado`
  ADD CONSTRAINT `fk_TB_AnaliseRisco_has_TB_IdentificacaoRiscos_TB_AnaliseRisco1` FOREIGN KEY (`TB_AnaliseRisco_idTB_AnaliseRisco`) REFERENCES `tb_analiserisco` (`idTB_AnaliseRisco`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_AnaliseRisco_has_TB_IdentificacaoRiscos_TB_Identificaca1` FOREIGN KEY (`TB_RiscoIdentificado_idTB_RiscoIdentificado`) REFERENCES `tb_riscoidentificado` (`idTB_RiscoIdentificado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_atendimentomedico`
--
ALTER TABLE `tb_atendimentomedico`
  ADD CONSTRAINT `fk_TB_AtendimentoMedico_TB_DiaAtestado1` FOREIGN KEY (`TB_Atestado_idTB_Atestado`) REFERENCES `tb_atestado` (`idTB_Atestado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_comunicadoacidente`
--
ALTER TABLE `tb_comunicadoacidente`
  ADD CONSTRAINT `fk_TB_ComunicadoAcidente_TB_AnaliseRisco1` FOREIGN KEY (`TB_AnaliseRisco_idTB_AnaliseRisco`) REFERENCES `tb_analiserisco` (`idTB_AnaliseRisco`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_ComunicadoAcidente_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_ComunicadoAcidente_TB_LocalidadePolo1` FOREIGN KEY (`TB_LocalidadePolo_idTB_LocalidadePolo`) REFERENCES `tb_localidadepolo` (`idTB_LocalidadePolo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_ComunicadoAcidente_TB_Processo1` FOREIGN KEY (`TB_Processo_idTB_Processo`) REFERENCES `tb_processo` (`idTB_Processo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Table_ComunicadoAcidente_TB_Local1` FOREIGN KEY (`TB_Local_idTB_Local`) REFERENCES `tb_local` (`idTB_Local`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_danomaterial`
--
ALTER TABLE `tb_danomaterial`
  ADD CONSTRAINT `fk_{3C0128F1-CBF3-4EDC-8E6E-C3DF7989F199}` FOREIGN KEY (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`) REFERENCES `tb_relatorioinvestigacao` (`idTB_RelatorioInvestigacao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_desvio`
--
ALTER TABLE `tb_desvio`
  ADD CONSTRAINT `fk_TB_Desvio_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Desvio_TB_Local1` FOREIGN KEY (`TB_Local_idTB_Local`) REFERENCES `tb_local` (`idTB_Local`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Desvio_TB_LocalidadePolo1` FOREIGN KEY (`TB_LocalidadePolo_idTB_LocalidadePolo`) REFERENCES `tb_localidadepolo` (`idTB_LocalidadePolo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_eficacia`
--
ALTER TABLE `tb_eficacia`
  ADD CONSTRAINT `fk_TB_Eficacia_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_envolvido`
--
ALTER TABLE `tb_envolvido`
  ADD CONSTRAINT `fk_TB_Envolvido_TB_ComunicadoAcidente1` FOREIGN KEY (`TB_ComunicadoAcidente_idTB_ComunicadoAcidente`) REFERENCES `tb_comunicadoacidente` (`idTB_ComunicadoAcidente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_foto`
--
ALTER TABLE `tb_foto`
  ADD CONSTRAINT `fk_{3E0C0CDB-DCE2-4CAC-9EED-6FE749299665}` FOREIGN KEY (`TB_Desvio_id_Desvio`) REFERENCES `tb_desvio` (`idTB_Desvio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_funcionario`
--
ALTER TABLE `tb_funcionario`
  ADD CONSTRAINT `fk_TB_Funcionario_TB_Funcao1` FOREIGN KEY (`TB_Funcao_idTB_Funcao`) REFERENCES `tb_funcao` (`idTB_Funcao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Funcionario_TB_Local1` FOREIGN KEY (`TB_Local_idTB_Local`) REFERENCES `tb_local` (`idTB_Local`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Funcionario_TB_LocalidadePolo1` FOREIGN KEY (`TB_LocalidadePolo_idTB_LocalidadePolo`) REFERENCES `tb_localidadepolo` (`idTB_LocalidadePolo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_funcionario_has_tb_analiserisco`
--
ALTER TABLE `tb_funcionario_has_tb_analiserisco`
  ADD CONSTRAINT `fk_TB_Funcionario_has_TB_AnaliseRisco_TB_AnaliseRisco1` FOREIGN KEY (`TB_AnaliseRisco_idTB_AnaliseRisco`) REFERENCES `tb_analiserisco` (`idTB_AnaliseRisco`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Funcionario_has_TB_AnaliseRisco_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_funcionario_has_tb_comite`
--
ALTER TABLE `tb_funcionario_has_tb_comite`
  ADD CONSTRAINT `fk_TB_Funcionario_has_TB_Comite_TB_Comite1` FOREIGN KEY (`TB_Comite_idTB_Comite`) REFERENCES `tb_comite` (`idTB_Comite`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Funcionario_has_TB_Comite_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_funcionario_has_tb_comunicadoacidente`
--
ALTER TABLE `tb_funcionario_has_tb_comunicadoacidente`
  ADD CONSTRAINT `fk_TB_Funcionario_has_TB_ComunicadoAcidente_TB_ComunicadoAcid1` FOREIGN KEY (`TB_ComunicadoAcidente_idTB_ComunicadoAcidente`) REFERENCES `tb_comunicadoacidente` (`idTB_ComunicadoAcidente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Funcionario_has_TB_ComunicadoAcidente_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_funcionario_has_tb_relatorioinvestigacao`
--
ALTER TABLE `tb_funcionario_has_tb_relatorioinvestigacao`
  ADD CONSTRAINT `fk_TB_Funcionario_has_TB_RelatorioInvestigacao_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Funcionario_has_TB_RelatorioInvestigacao_TB_RelatorioIn1` FOREIGN KEY (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`) REFERENCES `tb_relatorioinvestigacao` (`idTB_RelatorioInvestigacao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_funcionario_has_tb_relatorioinvestigacao1`
--
ALTER TABLE `tb_funcionario_has_tb_relatorioinvestigacao1`
  ADD CONSTRAINT `fk_TB_Funcionario_has_TB_RelatorioInvestigacao1_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Funcionario_has_TB_RelatorioInvestigacao1_TB_RelatorioI1` FOREIGN KEY (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`) REFERENCES `tb_relatorioinvestigacao` (`idTB_RelatorioInvestigacao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_funcionario_has_tb_treinamento`
--
ALTER TABLE `tb_funcionario_has_tb_treinamento`
  ADD CONSTRAINT `fk_TB_Funcionario_has_TB_Treinamento_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Funcionario_has_TB_Treinamento_TB_Treinamento1` FOREIGN KEY (`TB_Treinamento_idTB_Treinamento`) REFERENCES `tb_treinamento` (`idTB_Treinamento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_implantacao`
--
ALTER TABLE `tb_implantacao`
  ADD CONSTRAINT `fk_TB_Implantacao_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_localidadepolo`
--
ALTER TABLE `tb_localidadepolo`
  ADD CONSTRAINT `fk_{A5244E8E-12CD-4DAB-8E76-4CB0842A2E7C}` FOREIGN KEY (`TB_Polo_idTB_Polo`) REFERENCES `tb_polo` (`idTB_Polo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_medicao`
--
ALTER TABLE `tb_medicao`
  ADD CONSTRAINT `fk_TB_Medicao_TB_Equipe1` FOREIGN KEY (`TB_Equipe_idTB_Equipe`) REFERENCES `tb_equipe` (`idTB_Equipe`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Medicao_TB_Regional1` FOREIGN KEY (`TB_Regional_idTB_Regional`) REFERENCES `tb_regional` (`idTB_Regional`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_perda`
--
ALTER TABLE `tb_perda`
  ADD CONSTRAINT `fk_TB_Perdas_TB_Equipe1` FOREIGN KEY (`TB_Equipe_idTB_Equipe`) REFERENCES `tb_equipe` (`idTB_Equipe`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Perdas_TB_Regional1` FOREIGN KEY (`TB_Regional_idTB_Regional`) REFERENCES `tb_regional` (`idTB_Regional`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Perdas_TB_SubCategoriaCausa1` FOREIGN KEY (`TB_SubCategoriaCausa_idTB_SubCategoriaCausa`) REFERENCES `tb_subcategoriacausa` (`idTB_SubCategoriaCausa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Perdas_TB_Veiculo1` FOREIGN KEY (`TB_Veiculo_idTB_Veiculo`) REFERENCES `tb_veiculo` (`idTB_Veiculo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_polo`
--
ALTER TABLE `tb_polo`
  ADD CONSTRAINT `fk_{4B2DC929-5E22-47B1-9FF8-78E2FD537576}` FOREIGN KEY (`TB_Regional_idTB_Regional`) REFERENCES `tb_regional` (`idTB_Regional`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_regional_has_tb_processo`
--
ALTER TABLE `tb_regional_has_tb_processo`
  ADD CONSTRAINT `fk_TB_Regional_has_TB_Processo_TB_Processo1` FOREIGN KEY (`TB_Processo_idTB_Processo`) REFERENCES `tb_processo` (`idTB_Processo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_Regional_has_TB_Processo_TB_Regional1` FOREIGN KEY (`TB_Regional_idTB_Regional`) REFERENCES `tb_regional` (`idTB_Regional`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_relatointerno`
--
ALTER TABLE `tb_relatointerno`
  ADD CONSTRAINT `fk_TB_RelatoInterno_TB_ComunicadoAcidente1` FOREIGN KEY (`TB_ComunicadoAcidente_idTB_ComunicadoAcidente`) REFERENCES `tb_comunicadoacidente` (`idTB_ComunicadoAcidente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_RelatoInterno_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_RelatoInterno_TB_Funcionario2` FOREIGN KEY (`TB_Funcionario_Matricula1`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_relatointerno_has_tb_perguntarelato`
--
ALTER TABLE `tb_relatointerno_has_tb_perguntarelato`
  ADD CONSTRAINT `fk_{13712170-99BB-4EB8-90CC-E73E092B17AC}` FOREIGN KEY (`TB_PerguntaRelato_idTB_PerguntaRelato`) REFERENCES `tb_perguntarelato` (`idTB_PerguntaRelato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_{AEB02E33-C954-456B-97CA-0CB8DE349391}` FOREIGN KEY (`TB_RelatoInterno_idTB_RelatoInterno`) REFERENCES `tb_relatointerno` (`idTB_RelatoInterno`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_relatorioinvestigacao`
--
ALTER TABLE `tb_relatorioinvestigacao`
  ADD CONSTRAINT `fk_TB_RelatorioInvestigacao_TB_Comite1` FOREIGN KEY (`TB_Comite_idTB_Comite`) REFERENCES `tb_comite` (`idTB_Comite`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_RelatorioInvestigacao_TB_Eficacia1` FOREIGN KEY (`TB_Eficacia_idTB_Eficacia`) REFERENCES `tb_eficacia` (`idTB_Eficacia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_RelatorioInvestigacao_TB_Funcionario1` FOREIGN KEY (`TB_Funcionario_Matricula`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_RelatorioInvestigacao_TB_Funcionario2` FOREIGN KEY (`TB_Funcionario_Matricula1`) REFERENCES `tb_funcionario` (`Matricula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_RelatorioInvestigacao_TB_RelatoInterno1` FOREIGN KEY (`TB_RelatoInterno_idTB_RelatoInterno`) REFERENCES `tb_relatointerno` (`idTB_RelatoInterno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_{6910453B-9D88-4DC6-AF1F-A044DE85E26D}` FOREIGN KEY (`TB_AtendimentoMedico_idTB_AtendimentoMedico`) REFERENCES `tb_atendimentomedico` (`idTB_AtendimentoMedico`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_relatorioinvestigacao_has_tb_analiseacidente`
--
ALTER TABLE `tb_relatorioinvestigacao_has_tb_analiseacidente`
  ADD CONSTRAINT `fk_{3CEC948B-5981-4474-8FB9-C0183ADBA73E}` FOREIGN KEY (`TB_AnaliseAcidente_idTB_AnaliseAcidente`) REFERENCES `tb_analiseacidente` (`idTB_AnaliseAcidente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_{B668B311-35A8-4124-AD84-DD584778AFFA}` FOREIGN KEY (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`) REFERENCES `tb_relatorioinvestigacao` (`idTB_RelatorioInvestigacao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_relatorioinvestigacao_has_tb_anexorelatorioinvestigacao`
--
ALTER TABLE `tb_relatorioinvestigacao_has_tb_anexorelatorioinvestigacao`
  ADD CONSTRAINT `fk_TB_RelatorioInvestigacao_has_TB_AnexoRelatorioInvestigacao1` FOREIGN KEY (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`) REFERENCES `tb_relatorioinvestigacao` (`idTB_RelatorioInvestigacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TB_RelatorioInvestigacao_has_TB_AnexoRelatorioInvestigacao2` FOREIGN KEY (`TB_AnexoRelatorioInvestigacao_idTB_AnexoRelatorioInvestigacao`) REFERENCES `tb_anexorelatorioinvestigacao` (`idTB_AnexoRelatorioInvestigacao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_residuo`
--
ALTER TABLE `tb_residuo`
  ADD CONSTRAINT `fk_{FBE08A93-C847-43E0-89BA-C067D798B3EF}` FOREIGN KEY (`TB_ComunicadoAcidente_idTB_ComunicadoAcidente`) REFERENCES `tb_comunicadoacidente` (`idTB_ComunicadoAcidente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_subcategoriacausa`
--
ALTER TABLE `tb_subcategoriacausa`
  ADD CONSTRAINT `fk_TB_SubCategoriaCausa_TB_CategoriaCausa1` FOREIGN KEY (`TB_CategoriaCausa_idTB_CategoriaCausa`) REFERENCES `tb_categoriacausa` (`idTB_CategoriaCausa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_tarefa`
--
ALTER TABLE `tb_tarefa`
  ADD CONSTRAINT `fk_Tarefa_TB_Processo1` FOREIGN KEY (`TB_Processo_idTB_Processo`) REFERENCES `tb_processo` (`idTB_Processo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tb_testemunha`
--
ALTER TABLE `tb_testemunha`
  ADD CONSTRAINT `fk_Testemunha_TB_Local1` FOREIGN KEY (`TB_Local_idTB_Local`) REFERENCES `tb_local` (`idTB_Local`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_{397BE970-9DB6-4B7A-9A57-3E5B45FDF2A4}` FOREIGN KEY (`TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao`) REFERENCES `tb_relatorioinvestigacao` (`idTB_RelatorioInvestigacao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
